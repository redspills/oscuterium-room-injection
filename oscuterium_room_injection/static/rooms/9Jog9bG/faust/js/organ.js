function startorgan()
{
  const isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
  let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
  SAT.Utils.faust.organ_panner = new StereoPannerNode(audio_context, {pan:0.30});
  // Create the Faust generated node
  let pluginURL = `${SAT.baseURI}/rooms/9Jog9bG/faust/wasm`;
  let plugin = new SAT.Utils.faust.classes.organ(audio_context, pluginURL);
  plugin.load().then(node => {
     console.log("Faust plugin loaded");
     SAT.Utils.faust.dsps["organ"] = node;
     console.log(SAT.Utils.faust.dsps.organ.getJSON());
    // Print paths to be used with 'setParamValue'
     console.log(SAT.Utils.faust.dsps.organ.getParams());
    // Connect it to output as a regular WebAudio node
    SAT.Utils.faust.dsps.organ.connect(SAT.Utils.faust.organ_panner).connect(audio_context.destination);
  });
SAT.Utils.io.room.on("faustMsg", (data)=> {
 if (data.dsp == "organ")
{
    if (data.dspCommand == "keyOn") {
      SAT.Utils.faust.dsps.organ.keyOn(data.dspData[0], data.dspData[1], data.dspData[2]);
    } else {
       if (data.dspCommand == "keyOff") {
         SAT.Utils.faust.dsps.organ.keyOff(data.dspData[0], data.dspData[1], data.dspData[2]);
       }
     }
  }
});
}
