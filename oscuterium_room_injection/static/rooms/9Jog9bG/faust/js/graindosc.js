function startgraindosc()
{
  const isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
  let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
  SAT.Utils.faust.graindosc_panner = new StereoPannerNode(audio_context, {pan:0.30});
  // Create the Faust generated node
  let pluginURL = `${SAT.baseURI}/rooms/9Jog9bG/faust/wasm`;
  let plugin = new SAT.Utils.faust.classes.graindosc(audio_context, pluginURL);
  plugin.load().then(node => {
     console.log("Faust plugin loaded");
     SAT.Utils.faust.dsps["graindosc"] = node;
     console.log(SAT.Utils.faust.dsps.graindosc.getJSON());
    // Print paths to be used with 'setParamValue'
     console.log(SAT.Utils.faust.dsps.graindosc.getParams());
    // Connect it to output as a regular WebAudio node
    SAT.Utils.faust.dsps.graindosc.connect(SAT.Utils.faust.graindosc_panner).connect(audio_context.destination);
  });
SAT.Utils.io.room.on("faustMsg", (data)=> {
 if (data.dsp == "graindosc")
{
    if (data.dspCommand == "keyOn") {
      SAT.Utils.faust.dsps.graindosc.keyOn(data.dspData[0], data.dspData[1], data.dspData[2]);
    } else {
       if (data.dspCommand == "keyOff") {
         SAT.Utils.faust.dsps.graindosc.keyOff(data.dspData[0], data.dspData[1], data.dspData[2]);
       }
     }
  }
});
}
