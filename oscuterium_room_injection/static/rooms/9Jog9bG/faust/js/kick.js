function startkick()
{
    const isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
    let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
    SAT.Utils.faust.kick_panner = new StereoPannerNode(audio_context, { pan: 0.30 });
    // Create the Faust generated node
    let pluginURL = `${SAT.baseURI}/rooms/9Jog9bG/faust/wasm`;
    let plugin = new SAT.Utils.faust.classes.kick(audio_context, pluginURL);
    plugin.load().then(node => {
        console.log("Faust plugin loaded");
        SAT.Utils.faust.dsps["kick"] = node;
        console.log(SAT.Utils.faust.dsps.kick.getJSON());
        // Print paths to be used with 'setParamValue'
        console.log(SAT.Utils.faust.dsps.kick.getParams());
        // Connect it to output as a regular WebAudio node
        SAT.Utils.faust.dsps.kick.connect(SAT.Utils.faust.kick_panner).connect(audio_context.destination);
    });
    SAT.Utils.io.room.on("faustMsg", (data) => {
        if (data.dsp == "kick") {
            if (data.dspCommand == "keyOn") {
                SAT.Utils.faust.dsps.kick.keyOn(data.dspData[0], data.dspData[1], data.dspData[2]);
            } else {
                if (data.dspCommand == "keyOff") {
                    SAT.Utils.faust.dsps.kick.keyOff(data.dspData[0], data.dspData[1], data.dspData[2]);
                }
            }
        }
    });
}
