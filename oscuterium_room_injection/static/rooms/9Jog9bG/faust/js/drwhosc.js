function startdrwhosc()
{
  const isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
  let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
  SAT.Utils.faust.drwhosc_panner = new StereoPannerNode(audio_context, {pan:0.30});
  // Create the Faust generated node
  let pluginURL = `${SAT.baseURI}/rooms/9Jog9bG/faust/wasm`;
  let plugin = new SAT.Utils.faust.classes.drwhosc(audio_context, pluginURL);
  plugin.load().then(node => {
     console.log("Faust plugin loaded");
     SAT.Utils.faust.dsps["drwhosc"] = node;
     console.log(SAT.Utils.faust.dsps.drwhosc.getJSON());
    // Print paths to be used with 'setParamValue'
     console.log(SAT.Utils.faust.dsps.drwhosc.getParams());
    // Connect it to output as a regular WebAudio node
    SAT.Utils.faust.dsps.drwhosc.connect(SAT.Utils.faust.drwhosc_panner).connect(audio_context.destination);
  });
SAT.Utils.io.room.on("faustMsg", (data)=> {
 if (data.dsp == "drwhosc")
{
    if (data.dspCommand == "keyOn") {
      SAT.Utils.faust.dsps.drwhosc.keyOn(data.dspData[0], data.dspData[1], data.dspData[2]);
    } else {
       if (data.dspCommand == "keyOff") {
         SAT.Utils.faust.dsps.drwhosc.keyOff(data.dspData[0], data.dspData[1], data.dspData[2]);
       }
     }
  }
});
}
