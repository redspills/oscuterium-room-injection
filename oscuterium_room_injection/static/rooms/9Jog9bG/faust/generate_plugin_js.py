#!/bin/env python3

import argparse as ap
import os
import sys


instr_name: str = ""


def make_js_string(iname: str) -> str:
    return (
        f'function start{instr_name}()\n'
        '{\n'
        '  const isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");\n'
        '  let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();\n'
        f'  SAT.Utils.faust.{instr_name}_panner = new StereoPannerNode(audio_context, '
        '{pan:0.30});\n'
        '  // Create the Faust generated node\n'
        '  let pluginURL = `${SAT.baseURI}/rooms/9Jog9bG/faust/wasm`;\n'
        f'  let plugin = new SAT.Utils.faust.classes.{instr_name}(audio_context, pluginURL);\n'
        '  plugin.load().then(node => {\n'
        '     console.log("Faust plugin loaded");\n'
        f'     SAT.Utils.faust.dsps["{instr_name}"] = node;\n'
        f'     console.log(SAT.Utils.faust.dsps.{instr_name}.getJSON());\n'
        '    // Print paths to be used with \'setParamValue\'\n'
        f'     console.log(SAT.Utils.faust.dsps.{instr_name}.getParams());\n'
        '    // Connect it to output as a regular WebAudio node\n'
        f'    SAT.Utils.faust.dsps.{instr_name}.connect(SAT.Utils.faust.{instr_name}_panner).connect(audio_context.destination);\n'
        '  });\n'
        'SAT.Utils.io.room.on("faustMsg", (data)=> {\n'
        #'  console.log("Faust msg received")\n'
        #'  console.log(data);\n'
        f' if (data.dsp == "{instr_name}")\n'
        '{\n'
        '    if (data.dspCommand == "keyOn") {\n'
        f'      SAT.Utils.faust.dsps.{instr_name}.keyOn(data.dspData[0], data.dspData[1], data.dspData[2]);\n'
        '    } else {\n'
        '       if (data.dspCommand == "keyOff") {\n'
        f'         SAT.Utils.faust.dsps.{instr_name}.keyOff(data.dspData[0], data.dspData[1], data.dspData[2]);\n'
        '       }\n'
        '     }\n'
        '  }\n'
        '});\n'
        '}\n'
        # f'start{instr_name}();\n'
    )


def get_base_file_name(fname: str) -> str:
    return strip_path(fname)


def strip_path(path: str) -> str:
    return os.path.splitext(path)[0]


def make_js_file(path: str, content: str) -> None:
    f = open(path, "w+")
    f.write(content)
    f.close()


if __name__ == '__main__':
    parser = ap.ArgumentParser()
    parser.add_argument(
        '-i', type=str, help='Faust file path (ex: osc.dsp, ../osc.dsp)')
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    args = parser.parse_args()
    if args.i:
        instr_name = get_base_file_name(args.i)
        make_js_file(f"js/{instr_name}.js", make_js_string(instr_name))
        # print(make_js_string(instr_name))
