
#!/usr/bin/bash

rm wasm/*
for f in *.dsp
do
    faust2wasm -worklet $f
    ./generate_plugin_js.py -i $f
done

mv *.js wasm
mv *.wasm wasm


for i in $(grep -l dspName wasm/*.js)
do
    filename=$(basename $i .js)
    n=$(awk '/const dspName/{print NR}' $i)
    # sed -i "s/dspName/${filename}Name/g" $i
    echo $n
    sed -i $( expr $n - 1 )q $i
    echo "SAT.Utils.faust.classes['${filename}'] = ${filename};" >> $i
done
