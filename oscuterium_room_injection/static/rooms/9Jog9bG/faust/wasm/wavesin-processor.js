
/*
Code generated with Faust version 2.40.2
Compilation options: -lang wasm-ib -cn wavesin -es 1 -mcd 16 -single -ftz 2 
*/

function getJSONwavesin() {
	return '{"name": "wavesin","filename": "wavesin.dsp","version": "2.40.2","compile_options": "-lang wasm-ib -cn wavesin -es 1 -mcd 16 -single -ftz 2","library_list": ["/usr/local/share/faust/stdfaust.lib","/usr/local/share/faust/signals.lib","/usr/local/share/faust/maths.lib","/usr/local/share/faust/platform.lib","/usr/local/share/faust/envelopes.lib","/usr/local/share/faust/basics.lib","/usr/local/share/faust/oscillators.lib","/usr/local/share/faust/misceffects.lib","/usr/local/share/faust/filters.lib","/usr/local/share/faust/phaflangers.lib","/usr/local/share/faust/delays.lib","/usr/local/share/faust/spats.lib","/usr/local/share/faust/reverbs.lib"],"include_pathnames": ["/usr/local/share/faust","/usr/local/share/faust","/usr/share/faust",".","/home/mis/src/_externs/serveur-injection/static/rooms/9Jog9bG/faust"],"size": 602840,"inputs": 0,"outputs": 2,"meta": [ { "basics.lib/name": "Faust Basic Element Library" },{ "basics.lib/version": "0.5" },{ "compile_options": "-lang wasm-ib -cn wavesin -es 1 -mcd 16 -single -ftz 2" },{ "delays.lib/name": "Faust Delay Library" },{ "delays.lib/version": "0.1" },{ "envelopes.lib/author": "GRAME" },{ "envelopes.lib/copyright": "GRAME" },{ "envelopes.lib/license": "LGPL with exception" },{ "envelopes.lib/name": "Faust Envelope Library" },{ "envelopes.lib/version": "0.1" },{ "filename": "wavesin.dsp" },{ "filters.lib/allpass_comb:author": "Julius O. Smith III" },{ "filters.lib/allpass_comb:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/allpass_comb:license": "MIT-style STK-4.3 license" },{ "filters.lib/dcblocker:author": "Julius O. Smith III" },{ "filters.lib/dcblocker:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/dcblocker:license": "MIT-style STK-4.3 license" },{ "filters.lib/lowpass0_highpass1": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/name": "Faust Filters Library" },{ "filters.lib/pole:author": "Julius O. Smith III" },{ "filters.lib/pole:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/pole:license": "MIT-style STK-4.3 license" },{ "filters.lib/version": "0.3" },{ "filters.lib/zero:author": "Julius O. Smith III" },{ "filters.lib/zero:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/zero:license": "MIT-style STK-4.3 license" },{ "maths.lib/author": "GRAME" },{ "maths.lib/copyright": "GRAME" },{ "maths.lib/license": "LGPL with exception" },{ "maths.lib/name": "Faust Math Library" },{ "maths.lib/version": "2.5" },{ "misceffects.lib/name": "Misc Effects Library" },{ "misceffects.lib/version": "2.0" },{ "name": "wavesin" },{ "oscillators.lib/name": "Faust Oscillator Library" },{ "oscillators.lib/version": "0.3" },{ "phaflangers.lib/name": "Faust Phaser and Flanger Library" },{ "phaflangers.lib/version": "0.1" },{ "platform.lib/name": "Generic Platform Library" },{ "platform.lib/version": "0.2" },{ "reverbs.lib/name": "Faust Reverb Library" },{ "reverbs.lib/version": "0.2" },{ "signals.lib/name": "Faust Signal Routing Library" },{ "signals.lib/version": "0.1" },{ "spats.lib/name": "Faust Spatialization Library" },{ "spats.lib/version": "0.0" }],"ui": [ {"type": "vgroup","label": "wavesin","items": [ {"type": "hslider","label": "a","address": "/wavesin/a","index": 16456,"meta": [{ "midi": "ctrl 73" }],"init": 0.01,"min": 0.01,"max": 4,"step": 0.01},{"type": "hslider","label": "bend","address": "/wavesin/bend","index": 16516,"meta": [{ "midi": "pitchwheel" }],"init": 0,"min": -2,"max": 2,"step": 0.01},{"type": "hslider","label": "d","address": "/wavesin/d","index": 16460,"meta": [{ "midi": "ctrl 76" }],"init": 0.6,"min": 0.01,"max": 8,"step": 0.01},{"type": "vslider","label": "damp","address": "/wavesin/damp","index": 16404,"meta": [{ "midi": "ctrl 95" }],"init": 0.5,"min": 0,"max": 1,"step": 0.025},{"type": "hslider","label": "drive","address": "/wavesin/drive","index": 16500,"meta": [{ "BELA": "ANALOG_4" }],"init": 0.3,"min": 0,"max": 1,"step": 0.001},{"type": "hslider","label": "drywetflang","address": "/wavesin/drywetflang","index": 16424,"meta": [{ "BELA": "ANALOG_5" }],"init": 0.5,"min": 0,"max": 1,"step": 0.001},{"type": "vslider","label": "drywetreverb","address": "/wavesin/drywetreverb","index": 16416,"meta": [{ "BELA": "ANALOG_6" }],"init": 0.4,"min": 0,"max": 1,"step": 0.001},{"type": "hslider","label": "flangdel","address": "/wavesin/flangdel","index": 16640,"meta": [{ "midi": "ctrl 13" }],"init": 4,"min": 0.001,"max": 10,"step": 0.001},{"type": "hslider","label": "flangfeedback","address": "/wavesin/flangfeedback","index": 16588,"meta": [{ "midi": "ctrl 94" }],"init": 0.7,"min": 0,"max": 1,"step": 0.001},{"type": "nentry","label": "freq","address": "/wavesin/freq","index": 16512,"meta": [{ "unit": "Hz" }],"init": 0,"min": 0,"max": 20000,"step": 1},{"type": "nentry","label": "gain","address": "/wavesin/gain","index": 16484,"init": 0.5,"min": 0,"max": 1,"step": 0.01},{"type": "button","label": "gate","address": "/wavesin/gate","index": 16432},{"type": "hslider","label": "lfodepth","address": "/wavesin/lfodepth","index": 16540,"meta": [{ "BELA": "ANALOG_2" }],"init": 0,"min": 0,"max": 1,"step": 0.001},{"type": "hslider","label": "lfofreq","address": "/wavesin/lfofreq","index": 16552,"meta": [{ "BELA": "ANALOG_1" }],"init": 0.1,"min": 0.01,"max": 10,"step": 0.001},{"type": "hslider","label": "pan","address": "/wavesin/pan","index": 16420,"meta": [{ "midi": "ctrl 10" }],"init": 0.5,"min": 0,"max": 1,"step": 0.001},{"type": "hslider","label": "r","address": "/wavesin/r","index": 16444,"meta": [{ "BELA": "ANALOG_3" }],"init": 0.8,"min": 0.01,"max": 8,"step": 0.01},{"type": "vslider","label": "roomsize","address": "/wavesin/roomsize","index": 16396,"meta": [{ "BELA": "ANALOG_7" }],"init": 0.7,"min": 0,"max": 1,"step": 0.025},{"type": "hslider","label": "s","address": "/wavesin/s","index": 16468,"meta": [{ "midi": "ctrl 77" }],"init": 0.2,"min": 0,"max": 1,"step": 0.01},{"type": "vslider","label": "stereo","address": "/wavesin/stereo","index": 340464,"meta": [{ "midi": "ctrl 90" }],"init": 0.6,"min": 0,"max": 1,"step": 0.01},{"type": "hslider","label": "volume","address": "/wavesin/volume","index": 16428,"meta": [{ "midi": "ctrl 7" }],"init": 1,"min": 0,"max": 1,"step": 0.001},{"type": "hslider","label": "wavetravel","address": "/wavesin/wavetravel","index": 16528,"meta": [{ "BELA": "ANALOG_0" }],"init": 0,"min": 0,"max": 1,"step": 0.01}]}]}';
}
function getBase64Codewavesin() { return "AGFzbQEAAAAB4YCAgAASYAJ/fwBgBH9/f38AYAF9AX1gAX0BfWACfX0BfWABfwF/YAF/AX9gAn9/AX1gAX8Bf2ACf38AYAF/AGACf38AYAJ/fwBgAX8AYAJ/fwF/YAJ/fwF/YAJ9fQF9YAN/f30AArKAgIAABANlbnYFX2Nvc2YAAgNlbnYFX2V4cGYAAwNlbnYGX2Ztb2RmAAQDZW52BV9wb3dmABADj4CAgAAOAAEFBgcICQoLDA0ODxEFjICAgAABAZCAgIAA+IeAgAAHuoGAgAAMB2NvbXB1dGUABQxnZXROdW1JbnB1dHMABg1nZXROdW1PdXRwdXRzAAcNZ2V0UGFyYW1WYWx1ZQAIDWdldFNhbXBsZVJhdGUACQRpbml0AAoNaW5zdGFuY2VDbGVhcgALEWluc3RhbmNlQ29uc3RhbnRzAAwMaW5zdGFuY2VJbml0AA0aaW5zdGFuY2VSZXNldFVzZXJJbnRlcmZhY2UADg1zZXRQYXJhbVZhbHVlABEGbWVtb3J5AgAKrueAgAAOooWAgAABCH9BACEDQQAhBUEAIQdBACEJQQAhAkEAIQIDQAJAQbjlJCACQQJ0akEANgIAIAJBAWohAiACQQJIBEAMAgwBCwsLQQAhAwNAAkBBAEEAKAK85SRBAWo2ArjlJCADQQJ0QwAAgDxBACgCuOUkQX9qspRDAACAP5JDAACAQBACQwAAAMCSi0MAAIC/kjgCAEEAQQAoArjlJDYCvOUkIANBAWohAyADQYAISARADAIMAQsLC0EAIQRBACEEA0ACQEHA5SQgBEECdGpBADYCACAEQQFqIQQgBEECSARADAIMAQsLC0EAIQUDQAJAQQBBACgCxOUkQQFqNgLA5SRBgCAgBUECdGpDAAAAPEEAKALA5SRBf2qylEMAAIA/kkMAAIBAEAJDAAAAwJKLQwAAgL+SOAIAQQBBACgCwOUkNgLE5SQgBUEBaiEFIAVBgAhIBEAMAgwBCwsLQQAhBkEAIQYDQAJAQcjlJCAGQQJ0akEANgIAIAZBAWohBiAGQQJIBEAMAgwBCwsLQQAhBwNAAkBBAEEAKALM5SRBAWo2AsjlJEGAwAAgB0ECdGpDAADAO0EAKALI5SRBf2qylEMAAIA/kkMAAIBAEAJDAAAAwJKLQwAAgL+SOAIAQQBBACgCyOUkNgLM5SQgB0EBaiEHIAdBgAhIBEAMAgwBCwsLQQAhCEEAIQgDQAJAQdDlJCAIQQJ0akEANgIAIAhBAWohCCAIQQJIBEAMAgwBCwsLQQAhCQNAAkBBAEEAKALU5SRBAWo2AtDlJEGA4AAgCUECdGpDAACAO0EAKALQ5SRBf2qylEMAAIA/kkMAAIBAEAJDAAAAwJKLQwAAgL+SOAIAQQBBACgC0OUkNgLU5SQgCUEBaiEJIAlBgAhIBEAMAgwBCwsLC7u5gIAAAhl/dn1BACEEQQAhBUMAAAAAIR1DAAAAACEeQwAAAAAhH0MAAAAAISBDAAAAACEhQwAAAAAhIkMAAAAAISNDAAAAACEkQwAAAAAhJUMAAAAAISZBACEGQQAhB0MAAAAAISdDAAAAACEoQQAhCEMAAAAAISlDAAAAACEqQwAAAAAhK0MAAAAAISxDAAAAACEtQwAAAAAhLkMAAAAAIS9DAAAAACEwQwAAAAAhMUMAAAAAITJDAAAAACEzQwAAAAAhNEMAAAAAITVDAAAAACE2QwAAAAAhN0EAIQlBACEKQQAhC0EAIQxBACENQQAhDkEAIQ9BACEQQwAAAAAhOEEAIRFBACESQQAhE0EAIRRDAAAAACE5QQAhFUMAAAAAITpBACEWQQAhF0MAAAAAITtDAAAAACE8QQAhGEMAAAAAIT1DAAAAACE+QwAAAAAhP0MAAAAAIUBDAAAAACFBQwAAAAAhQkMAAAAAIUNBACEZQwAAAAAhREMAAAAAIUVDAAAAACFGQwAAAAAhR0MAAAAAIUhDAAAAACFJQwAAAAAhSkMAAAAAIUtDAAAAACFMQwAAAAAhTUMAAAAAIU5DAAAAACFPQQAhGkMAAAAAIVBBACEbQwAAAAAhUUMAAAAAIVJDAAAAACFTQwAAAAAhVEMAAAAAIVVDAAAAACFWQwAAAAAhV0MAAAAAIVhDAAAAACFZQwAAAAAhWkMAAAAAIVtDAAAAACFcQwAAAAAhXUMAAAAAIV5DAAAAACFfQwAAAAAhYEMAAAAAIWFDAAAAACFiQwAAAAAhY0MAAAAAIWRDAAAAACFlQwAAAAAhZkMAAAAAIWdDAAAAACFoQwAAAAAhaUMAAAAAIWpDAAAAACFrQwAAAAAhbEMAAAAAIW1DAAAAACFuQwAAAAAhb0MAAAAAIXBDAAAAACFxQwAAAAAhckMAAAAAIXNDAAAAACF0QwAAAAAhdUMAAAAAIXZDAAAAACF3QwAAAAAheEMAAAAAIXlDAAAAACF6QwAAAAAhe0MAAAAAIXxDAAAAACF9QwAAAAAhfkMAAAAAIX9DAAAAACGAAUMAAAAAIYEBQwAAAAAhggFDAAAAACGDAUMAAAAAIYQBQwAAAAAhhQFDAAAAACGGAUMAAAAAIYcBQwAAAAAhiAFDAAAAACGJAUMAAAAAIYoBQwAAAAAhiwFDAAAAACGMAUMAAAAAIY0BQwAAAAAhjgFDAAAAACGPAUMAAAAAIZABQwAAAAAhkQFDAAAAACGSAUELIRwgA0EAaigCACEEIANBBGooAgAhBUEAKgKIgAFBACoCjIABlEMzMzM/kiEdQQAqApCAAUEAKgKUgAGUIR5DAACAPyAekyEfQQAqAqCAASEgQQAqAqSAASEhQwAAgD8gIZMhIkPNzMw9ICAgISAikpSUISNBACoCqIABQwAAgD+SISRDAACAP0MAAAA/ICSUkyElQQAqAqyAASEmQQAqArCAAUMAAAAAXiEGIAYhB0EAKgK8gAEhJ0EAKgLIgAEhKEEAKgKEgAEgKJSoIQhBACoCzIABISkgBrIhKkEAKgLUgAEgKpQhK0EAKgLggAFBACoC5IABlCEsQQAqAuCAAUEAKgL0gAGUIS1BACoC0IABQQAqAoCBAUMAAABAQ6uqqj1BACoChIEBlBADlJQhLkEAKgLggAFBACoCkIEBlCEvQQAqAuCAAUEAKgKcgQGUITBBACoC4IABQQAqAqiBAZQhMUMAAIA+ICSUITJBACoCzIEBITNBACoCgIIBITRDAACAPyAgkyE1IDUgIpQhNkEAKgLs4xRBACoC8OMUlCE3QQAqAujjFCA3kqghCUEAKgKE5BYgN5KoIQpBACoCmOQYIDeSqCELQQAqAqzkGiA3kqghDEEAKgLA5BwgN5KoIQ1BACoC1OQeIDeSqCEOQQAqAujkICA3kqghD0EAKgL85CIgN5KoIRAgN0MAAIC/kiE4QwAAgERDAAAAAEEAKgKIpSMgOJKXlqghEUMAAIBEQwAAAABBACoClOUjIDiSl5aoIRJDAACAREMAAAAAQQAqAqClJCA4kpeWqCETQwAAgERDAAAAAEEAKgKs5SQgOJKXlqghFCAhIDWUITlBACEVA0ACQCAeQQAqApyAAZQgH0EAKgKgggOUkiE6QQAgOrxBgICA/AdxBH0gOgVDAAAAAAs4ApiAAUEAIAY2ArSAAUEAIAZBACgCxIABQQFqbDYCwIABIAZBACgCuIABayEWQQAoAsCAASAISCAWIBZBAEpsciEXIBcEfSAoBSApCyE7Q+cwFD4gBwR9IDsFICcLlCE8IDxDAAAANF0hGEMAAAAAQQAqAtCAASAYBH1DAACAPwUgPAuVkxABIT0gGAR9QwAAAAAFID0LIT4gFwR9ICoFICsLIT9DAACAPyA+kyAHBH0gPwVDAAAAAAuUID5BACoC3IABlJIhQEEAIEC8QYCAgPwHcQR9IEAFQwAAAAALOALYgAEgLEEAKgLogAFBACoC8IABlJIhQUEAIEG8QYCAgPwHcQR9IEEFQwAAAAALOALsgAEgLUEAKgLogAFBACoC/IABlJIhQkEAIEK8QYCAgPwHcQR9IEIFQwAAAAALOAL4gAEgLkEAKgKMgQEgLkEAKgKMgQGSjpOSIUNBACBDvEGAgID8B3EEfSBDBUMAAAAACzgCiIEBQwAAgERBACoCiIEBlKghGSAvQQAqAuiAAUEAKgKYgQGUkiFEQQAgRLxBgICA/AdxBH0gRAVDAAAAAAs4ApSBASAwQQAqAuiAAUEAKgKkgQGUkiFFQQAgRbxBgICA/AdxBH0gRQVDAAAAAAs4AqCBASAxQQAqAuiAAUEAKgKwgQGUkiFGQQAgRrxBgICA/AdxBH0gRgVDAAAAAAs4AqyBAUEAKgK4gQFBACoC0IABQQAqAqyBAZSSIUcgRyBHjpMhSEEAIEi8QYCAgPwHcQR9IEgFQwAAAAALOAK0gQFDAAAAAEMAAIA/QQAqApSBAUEAKgKggQFDAACAP0MAAABAQQAqArSBAZRDAACAv5KLk5SSlpchSUMAAIC/QwAAgD8gJkEAKgLYgAFBACoC7IABlEMAACBBQwAAAEBBACoC+IABlBADlCAZQQJ0KgIAQwAAAABD3MuWQCBJlBAAl5RBgCAgGUECdGoqAgBDAAAAAEPcy5ZAIElDq6qqvpKUEACXlJJBgMAAIBlBAnRqKgIAQwAAAABD3MuWQCBJQ6uqKr+SlBAAl5SSQYDgACAZQQJ0aioCAEMAAAAAQ9zLlkAgSUMAAIC/kpQQAJeUkpSUQ83MzD2SlpchSiBKQwAAgD9Dq6qqPiBKQwAAAEAQA5STlCFLQQAgSzgCvIEBQ1K4fj9BACoCyIEBlCBLkkEAKgLAgQGTIUxBACBMvEGAgID8B3EEfSBMBUMAAAAACzgCxIEBIDNBACoCkIIBlEEAKgLEgQGTIU1BACBNOALQgQFBACoC0IABQQAqAoiCAUEAKgLQgAFBACoCiIIBko6TkiFOQQAgTrxBgICA/AdxBH0gTgVDAAAAAAs4AoSCAUMAACBBIDRDAAAAQEMAAABAQwAAgD9DAAAAQEEAKgKEggGUQwAAgL+Si5OUQwAAgL+SlJKWIU8gT6ghGiBPjiFQIBpBAWohG0HQgQFBACAaSAR/IBoFQQALQQJ0aioCACBQQwAAgD8gT5OSlCBPIFCTQdCBAUEAIBtIBH8gGwVBAAtBAnRqKgIAlJIhUUEAIFG8QYCAgPwHcQR9IFEFQwAAAAALOAKMggEgJUEAKgLEgQGUIDJBACoCxIEBQQAqAoyCAZKUkiFSICMgUpQhU0GYggFBACgClIIBQf8/cUECdGogHUEAKgKYgAGUIFOSOAIAQZiCAUEAKAKUggFBACgCmIIDa0H/P3FBAnRqKgIAIVRBACBUvEGAgID8B3EEfSBUBUMAAAAACzgCnIIDIB5BACoCqIIDlCAfQQAqArSCBZSSIVVBACBVvEGAgID8B3EEfSBVBUMAAAAACzgCpIIDQayCA0EAKAKUggFB/z9xQQJ0aiBTIB1BACoCpIIDlJI4AgBBrIIDQQAoApSCAUEAKAKsggVrQf8/cUECdGoqAgAhVkEAIFa8QYCAgPwHcQR9IFYFQwAAAAALOAKwggUgHkEAKgK8ggWUIB9BACoCyIIHlJIhV0EAIFe8QYCAgPwHcQR9IFcFQwAAAAALOAK4ggVBwIIFQQAoApSCAUH/P3FBAnRqIFMgHUEAKgK4ggWUkjgCAEHAggVBACgClIIBQQAoAsCCB2tB/z9xQQJ0aioCACFYQQAgWLxBgICA/AdxBH0gWAVDAAAAAAs4AsSCByAeQQAqAtCCB5QgH0EAKgLcggmUkiFZQQAgWbxBgICA/AdxBH0gWQVDAAAAAAs4AsyCB0HUggdBACgClIIBQf8/cUECdGogUyAdQQAqAsyCB5SSOAIAQdSCB0EAKAKUggFBACgC1IIJa0H/P3FBAnRqKgIAIVpBACBavEGAgID8B3EEfSBaBUMAAAAACzgC2IIJIB5BACoC5IIJlCAfQQAqAvCCC5SSIVtBACBbvEGAgID8B3EEfSBbBUMAAAAACzgC4IIJQeiCCUEAKAKUggFB/z9xQQJ0aiBTIB1BACoC4IIJlJI4AgBB6IIJQQAoApSCAUEAKALoggtrQf8/cUECdGoqAgAhXEEAIFy8QYCAgPwHcQR9IFwFQwAAAAALOALsggsgHkEAKgL4gguUIB9BACoChIMNlJIhXUEAIF28QYCAgPwHcQR9IF0FQwAAAAALOAL0ggtB/IILQQAoApSCAUH/P3FBAnRqIFMgHUEAKgL0gguUkjgCAEH8ggtBACgClIIBQQAoAvyCDWtB/z9xQQJ0aioCACFeQQAgXrxBgICA/AdxBH0gXgVDAAAAAAs4AoCDDSAeQQAqAoyDDZQgH0EAKgKYgw+UkiFfQQAgX7xBgICA/AdxBH0gXwVDAAAAAAs4AoiDDUGQgw1BACgClIIBQf8/cUECdGogUyAdQQAqAoiDDZSSOAIAQZCDDUEAKAKUggFBACgCkIMPa0H/P3FBAnRqKgIAIWBBACBgvEGAgID8B3EEfSBgBUMAAAAACzgClIMPIB5BACoCoIMPlCAfQQAqAqyDEZSSIWFBACBhvEGAgID8B3EEfSBhBUMAAAAACzgCnIMPQaSDD0EAKAKUggFB/z9xQQJ0aiBTIB1BACoCnIMPlJI4AgBBpIMPQQAoApSCAUEAKAKkgxFrQf8/cUECdGoqAgAhYkEAIGK8QYCAgPwHcQR9IGIFQwAAAAALOAKogxFBACoCnIIDQQAqArCCBZJBACoCxIIHkkEAKgLYggmSQQAqAuyCC5JBACoCgIMNkkEAKgKUgw+SQQAqAqiDEZJDAAAAP0EAKgK4wxGUkiFjQbCDEUEAKAKUggFB/w9xQQJ0aiBjOAIAQbCDEUEAKAKUggFBACgCsMMRa0H/D3FBAnRqKgIAIWRBACBkvEGAgID8B3EEfSBkBUMAAAAACzgCtMMRQwAAAABDAAAAPyBjlJMhZSBlvEGAgID8B3EEfSBlBUMAAAAACyFmQQAqArjDESBmQwAAAD9BACoCxIMSlJKSIWdBvMMRQQAoApSCAUH/D3FBAnRqIGc4AgBBvMMRQQAoApSCAUEAKAK8gxJrQf8PcUECdGoqAgAhaEEAIGi8QYCAgPwHcQR9IGgFQwAAAAALOALAgxJDAAAAAEMAAAA/IGeUkyFpIGm8QYCAgPwHcQR9IGkFQwAAAAALIWpBACoCxIMSIGpDAAAAP0EAKgLQwxKUkpIha0HIgxJBACgClIIBQf8PcUECdGogazgCAEHIgxJBACgClIIBQQAoAsjDEmtB/w9xQQJ0aioCACFsQQAgbLxBgICA/AdxBH0gbAVDAAAAAAs4AszDEkMAAAAAQwAAAD8ga5STIW0gbbxBgICA/AdxBH0gbQVDAAAAAAshbkEAKgLQwxIgbkMAAAA/QQAqAtzjEpSSkiFvQdTDEkEAKAKUggFB/wdxQQJ0aiBvOAIAQdTDEkEAKAKUggFBACgC1OMSa0H/B3FBAnRqKgIAIXBBACBwvEGAgID8B3EEfSBwBUMAAAAACzgC2OMSQwAAAABDAAAAPyBvlJMhcSBxvEGAgID8B3EEfSBxBUMAAAAACyFyIAQgFWogckEAKgLc4xKSIDYgUpSSOAIAIB5BACoC5OMSlCAfQQAqAvjjFJSSIXNBACBzvEGAgID8B3EEfSBzBUMAAAAACzgC4OMSQejjEkEAKAKUggFB/z9xQQJ0aiBTIB1BACoC4OMSlJI4AgBB6OMSQQAoApSCASAJa0H/P3FBAnRqKgIAIXRBACB0vEGAgID8B3EEfSB0BUMAAAAACzgC9OMUIB5BACoCgOQUlCAfQQAqAozkFpSSIXVBACB1vEGAgID8B3EEfSB1BUMAAAAACzgC/OMUQYTkFEEAKAKUggFB/z9xQQJ0aiBTIB1BACoC/OMUlJI4AgBBhOQUQQAoApSCASAKa0H/P3FBAnRqKgIAIXZBACB2vEGAgID8B3EEfSB2BUMAAAAACzgCiOQWIB5BACoClOQWlCAfQQAqAqDkGJSSIXdBACB3vEGAgID8B3EEfSB3BUMAAAAACzgCkOQWQZjkFkEAKAKUggFB/z9xQQJ0aiBTIB1BACoCkOQWlJI4AgBBmOQWQQAoApSCASALa0H/P3FBAnRqKgIAIXhBACB4vEGAgID8B3EEfSB4BUMAAAAACzgCnOQYIB5BACoCqOQYlCAfQQAqArTkGpSSIXlBACB5vEGAgID8B3EEfSB5BUMAAAAACzgCpOQYQazkGEEAKAKUggFB/z9xQQJ0aiBTIB1BACoCpOQYlJI4AgBBrOQYQQAoApSCASAMa0H/P3FBAnRqKgIAIXpBACB6vEGAgID8B3EEfSB6BUMAAAAACzgCsOQaIB5BACoCvOQalCAfQQAqAsjkHJSSIXtBACB7vEGAgID8B3EEfSB7BUMAAAAACzgCuOQaQcDkGkEAKAKUggFB/z9xQQJ0aiBTIB1BACoCuOQalJI4AgBBwOQaQQAoApSCASANa0H/P3FBAnRqKgIAIXxBACB8vEGAgID8B3EEfSB8BUMAAAAACzgCxOQcIB5BACoC0OQclCAfQQAqAtzkHpSSIX1BACB9vEGAgID8B3EEfSB9BUMAAAAACzgCzOQcQdTkHEEAKAKUggFB/z9xQQJ0aiBTIB1BACoCzOQclJI4AgBB1OQcQQAoApSCASAOa0H/P3FBAnRqKgIAIX5BACB+vEGAgID8B3EEfSB+BUMAAAAACzgC2OQeIB5BACoC5OQelCAfQQAqAvDkIJSSIX9BACB/vEGAgID8B3EEfSB/BUMAAAAACzgC4OQeQejkHkEAKAKUggFB/z9xQQJ0aiBTIB1BACoC4OQelJI4AgBB6OQeQQAoApSCASAPa0H/P3FBAnRqKgIAIYABQQAggAG8QYCAgPwHcQR9IIABBUMAAAAACzgC7OQgIB5BACoC+OQglCAfQQAqAoTlIpSSIYEBQQAggQG8QYCAgPwHcQR9IIEBBUMAAAAACzgC9OQgQfzkIEEAKAKUggFB/z9xQQJ0aiBTIB1BACoC9OQglJI4AgBB/OQgQQAoApSCASAQa0H/P3FBAnRqKgIAIYIBQQAgggG8QYCAgPwHcQR9IIIBBUMAAAAACzgCgOUiQQAqAvTjFEEAKgKI5BaSQQAqApzkGJJBACoCsOQakkEAKgLE5BySQQAqAtjkHpJBACoC7OQgkkEAKgKA5SKSQwAAAD9BACoCkKUjlJIhgwFBiOUiQQAoApSCAUH/D3FBAnRqIIMBOAIAQYjlIkEAKAKUggEgEWtB/w9xQQJ0aioCACGEAUEAIIQBvEGAgID8B3EEfSCEAQVDAAAAAAs4AoylI0MAAAAAQwAAAD8ggwGUkyGFASCFAbxBgICA/AdxBH0ghQEFQwAAAAALIYYBQQAqApClIyCGAUMAAAA/QQAqApzlI5SSkiGHAUGUpSNBACgClIIBQf8PcUECdGoghwE4AgBBlKUjQQAoApSCASASa0H/D3FBAnRqKgIAIYgBQQAgiAG8QYCAgPwHcQR9IIgBBUMAAAAACzgCmOUjQwAAAABDAAAAPyCHAZSTIYkBIIkBvEGAgID8B3EEfSCJAQVDAAAAAAshigFBACoCnOUjIIoBQwAAAD9BACoCqKUklJKSIYsBQaDlI0EAKAKUggFB/w9xQQJ0aiCLATgCAEGg5SNBACgClIIBIBNrQf8PcUECdGoqAgAhjAFBACCMAbxBgICA/AdxBH0gjAEFQwAAAAALOAKkpSRDAAAAAEMAAAA/IIsBlJMhjQEgjQG8QYCAgPwHcQR9II0BBUMAAAAACyGOAUEAKgKopSQgjgFDAAAAP0EAKgK05SSUkpIhjwFBrKUkQQAoApSCAUH/D3FBAnRqII8BOAIAQaylJEEAKAKUggEgFGtB/w9xQQJ0aioCACGQAUEAIJABvEGAgID8B3EEfSCQAQVDAAAAAAs4ArDlJEMAAAAAQwAAAD8gjwGUkyGRASCRAbxBgICA/AdxBH0gkQEFQwAAAAALIZIBIAUgFWogkgFBACoCtOUkkiA5IFKUkjgCAEEAQQAqApiAATgCnIABQQBBACgCtIABNgK4gAFBAEEAKALAgAE2AsSAAUEAQQAqAtiAATgC3IABQQBBACoC7IABOALwgAFBAEEAKgL4gAE4AvyAAUEAQQAqAoiBATgCjIEBQQBBACoClIEBOAKYgQFBAEEAKgKggQE4AqSBAUEAQQAqAqyBATgCsIEBQQBBACoCtIEBOAK4gQFBAEEAKgK8gQE4AsCBAUEAQQAqAsSBATgCyIEBQQshHANAAkBB0IEBIBxBAnRqQdCBASAcQQFrQQJ0aioCADgCACAcQQFrIRwgHEEASgRADAIMAQsLC0EAQQAqAoSCATgCiIIBQQBBACoCjIIBOAKQggFBAEEAKAKUggFBAWo2ApSCAUEAQQAqApyCAzgCoIIDQQBBACoCpIIDOAKoggNBAEEAKgKwggU4ArSCBUEAQQAqAriCBTgCvIIFQQBBACoCxIIHOALIggdBAEEAKgLMggc4AtCCB0EAQQAqAtiCCTgC3IIJQQBBACoC4IIJOALkgglBAEEAKgLsggs4AvCCC0EAQQAqAvSCCzgC+IILQQBBACoCgIMNOAKEgw1BAEEAKgKIgw04AoyDDUEAQQAqApSDDzgCmIMPQQBBACoCnIMPOAKggw9BAEEAKgKogxE4AqyDEUEAQQAqArTDETgCuMMRQQBBACoCwIMSOALEgxJBAEEAKgLMwxI4AtDDEkEAQQAqAtjjEjgC3OMSQQBBACoC4OMSOALk4xJBAEEAKgL04xQ4AvjjFEEAQQAqAvzjFDgCgOQUQQBBACoCiOQWOAKM5BZBAEEAKgKQ5BY4ApTkFkEAQQAqApzkGDgCoOQYQQBBACoCpOQYOAKo5BhBAEEAKgKw5Bo4ArTkGkEAQQAqArjkGjgCvOQaQQBBACoCxOQcOALI5BxBAEEAKgLM5Bw4AtDkHEEAQQAqAtjkHjgC3OQeQQBBACoC4OQeOALk5B5BAEEAKgLs5CA4AvDkIEEAQQAqAvTkIDgC+OQgQQBBACoCgOUiOAKE5SJBAEEAKgKMpSM4ApClI0EAQQAqApjlIzgCnOUjQQBBACoCpKUkOAKopSRBAEEAKgKw5SQ4ArTlJCAVQQRqIRUgFUEEIAFsSARADAIMAQsLCwuFgICAAABBAA8LhYCAgAAAQQIPC4uAgIAAACAAIAFqKgIADwuKgICAAABBACgCgIABDwuOgICAAAAgACABEAQgACABEA0L7J+AgAABT39BACEBQQAhAkEAIQNBACEEQQAhBUEAIQZBACEHQQAhCEEAIQlBACEKQQAhC0EAIQxBACENQQAhDkEAIQ9BACEQQQAhEUEAIRJBACETQQAhFEEAIRVBACEWQQAhF0EAIRhBACEZQQAhGkEAIRtBACEcQQAhHUEAIR5BACEfQQAhIEEAISFBACEiQQAhI0EAISRBACElQQAhJkEAISdBACEoQQAhKUEAISpBACErQQAhLEEAIS1BACEuQQAhL0EAITBBACExQQAhMkEAITNBACE0QQAhNUEAITZBACE3QQAhOEEAITlBACE6QQAhO0EAITxBACE9QQAhPkEAIT9BACFAQQAhQUEAIUJBACFDQQAhREEAIUVBACFGQQAhR0EAIUhBACFJQQAhSkEAIUtBACFMQQAhTUEAIU5BACFPQQAhAQNAAkBBmIABIAFBAnRqQwAAAAA4AgAgAUEBaiEBIAFBAkgEQAwCDAELCwtBACECA0ACQEG0gAEgAkECdGpBADYCACACQQFqIQIgAkECSARADAIMAQsLC0EAIQMDQAJAQcCAASADQQJ0akEANgIAIANBAWohAyADQQJIBEAMAgwBCwsLQQAhBANAAkBB2IABIARBAnRqQwAAAAA4AgAgBEEBaiEEIARBAkgEQAwCDAELCwtBACEFA0ACQEHsgAEgBUECdGpDAAAAADgCACAFQQFqIQUgBUECSARADAIMAQsLC0EAIQYDQAJAQfiAASAGQQJ0akMAAAAAOAIAIAZBAWohBiAGQQJIBEAMAgwBCwsLQQAhBwNAAkBBiIEBIAdBAnRqQwAAAAA4AgAgB0EBaiEHIAdBAkgEQAwCDAELCwtBACEIA0ACQEGUgQEgCEECdGpDAAAAADgCACAIQQFqIQggCEECSARADAIMAQsLC0EAIQkDQAJAQaCBASAJQQJ0akMAAAAAOAIAIAlBAWohCSAJQQJIBEAMAgwBCwsLQQAhCgNAAkBBrIEBIApBAnRqQwAAAAA4AgAgCkEBaiEKIApBAkgEQAwCDAELCwtBACELA0ACQEG0gQEgC0ECdGpDAAAAADgCACALQQFqIQsgC0ECSARADAIMAQsLC0EAIQwDQAJAQbyBASAMQQJ0akMAAAAAOAIAIAxBAWohDCAMQQJIBEAMAgwBCwsLQQAhDQNAAkBBxIEBIA1BAnRqQwAAAAA4AgAgDUEBaiENIA1BAkgEQAwCDAELCwtBACEOA0ACQEHQgQEgDkECdGpDAAAAADgCACAOQQFqIQ4gDkEMSARADAIMAQsLC0EAIQ8DQAJAQYSCASAPQQJ0akMAAAAAOAIAIA9BAWohDyAPQQJIBEAMAgwBCwsLQQAhEANAAkBBjIIBIBBBAnRqQwAAAAA4AgAgEEEBaiEQIBBBAkgEQAwCDAELCwtBAEEANgKUggFBACERA0ACQEGYggEgEUECdGpDAAAAADgCACARQQFqIREgEUGAwABIBEAMAgwBCwsLQQAhEgNAAkBBnIIDIBJBAnRqQwAAAAA4AgAgEkEBaiESIBJBAkgEQAwCDAELCwtBACETA0ACQEGkggMgE0ECdGpDAAAAADgCACATQQFqIRMgE0ECSARADAIMAQsLC0EAIRQDQAJAQayCAyAUQQJ0akMAAAAAOAIAIBRBAWohFCAUQYDAAEgEQAwCDAELCwtBACEVA0ACQEGwggUgFUECdGpDAAAAADgCACAVQQFqIRUgFUECSARADAIMAQsLC0EAIRYDQAJAQbiCBSAWQQJ0akMAAAAAOAIAIBZBAWohFiAWQQJIBEAMAgwBCwsLQQAhFwNAAkBBwIIFIBdBAnRqQwAAAAA4AgAgF0EBaiEXIBdBgMAASARADAIMAQsLC0EAIRgDQAJAQcSCByAYQQJ0akMAAAAAOAIAIBhBAWohGCAYQQJIBEAMAgwBCwsLQQAhGQNAAkBBzIIHIBlBAnRqQwAAAAA4AgAgGUEBaiEZIBlBAkgEQAwCDAELCwtBACEaA0ACQEHUggcgGkECdGpDAAAAADgCACAaQQFqIRogGkGAwABIBEAMAgwBCwsLQQAhGwNAAkBB2IIJIBtBAnRqQwAAAAA4AgAgG0EBaiEbIBtBAkgEQAwCDAELCwtBACEcA0ACQEHgggkgHEECdGpDAAAAADgCACAcQQFqIRwgHEECSARADAIMAQsLC0EAIR0DQAJAQeiCCSAdQQJ0akMAAAAAOAIAIB1BAWohHSAdQYDAAEgEQAwCDAELCwtBACEeA0ACQEHsggsgHkECdGpDAAAAADgCACAeQQFqIR4gHkECSARADAIMAQsLC0EAIR8DQAJAQfSCCyAfQQJ0akMAAAAAOAIAIB9BAWohHyAfQQJIBEAMAgwBCwsLQQAhIANAAkBB/IILICBBAnRqQwAAAAA4AgAgIEEBaiEgICBBgMAASARADAIMAQsLC0EAISEDQAJAQYCDDSAhQQJ0akMAAAAAOAIAICFBAWohISAhQQJIBEAMAgwBCwsLQQAhIgNAAkBBiIMNICJBAnRqQwAAAAA4AgAgIkEBaiEiICJBAkgEQAwCDAELCwtBACEjA0ACQEGQgw0gI0ECdGpDAAAAADgCACAjQQFqISMgI0GAwABIBEAMAgwBCwsLQQAhJANAAkBBlIMPICRBAnRqQwAAAAA4AgAgJEEBaiEkICRBAkgEQAwCDAELCwtBACElA0ACQEGcgw8gJUECdGpDAAAAADgCACAlQQFqISUgJUECSARADAIMAQsLC0EAISYDQAJAQaSDDyAmQQJ0akMAAAAAOAIAICZBAWohJiAmQYDAAEgEQAwCDAELCwtBACEnA0ACQEGogxEgJ0ECdGpDAAAAADgCACAnQQFqIScgJ0ECSARADAIMAQsLC0EAISgDQAJAQbCDESAoQQJ0akMAAAAAOAIAIChBAWohKCAoQYAQSARADAIMAQsLC0EAISkDQAJAQbTDESApQQJ0akMAAAAAOAIAIClBAWohKSApQQJIBEAMAgwBCwsLQQAhKgNAAkBBvMMRICpBAnRqQwAAAAA4AgAgKkEBaiEqICpBgBBIBEAMAgwBCwsLQQAhKwNAAkBBwIMSICtBAnRqQwAAAAA4AgAgK0EBaiErICtBAkgEQAwCDAELCwtBACEsA0ACQEHIgxIgLEECdGpDAAAAADgCACAsQQFqISwgLEGAEEgEQAwCDAELCwtBACEtA0ACQEHMwxIgLUECdGpDAAAAADgCACAtQQFqIS0gLUECSARADAIMAQsLC0EAIS4DQAJAQdTDEiAuQQJ0akMAAAAAOAIAIC5BAWohLiAuQYAISARADAIMAQsLC0EAIS8DQAJAQdjjEiAvQQJ0akMAAAAAOAIAIC9BAWohLyAvQQJIBEAMAgwBCwsLQQAhMANAAkBB4OMSIDBBAnRqQwAAAAA4AgAgMEEBaiEwIDBBAkgEQAwCDAELCwtBACExA0ACQEHo4xIgMUECdGpDAAAAADgCACAxQQFqITEgMUGAwABIBEAMAgwBCwsLQQAhMgNAAkBB9OMUIDJBAnRqQwAAAAA4AgAgMkEBaiEyIDJBAkgEQAwCDAELCwtBACEzA0ACQEH84xQgM0ECdGpDAAAAADgCACAzQQFqITMgM0ECSARADAIMAQsLC0EAITQDQAJAQYTkFCA0QQJ0akMAAAAAOAIAIDRBAWohNCA0QYDAAEgEQAwCDAELCwtBACE1A0ACQEGI5BYgNUECdGpDAAAAADgCACA1QQFqITUgNUECSARADAIMAQsLC0EAITYDQAJAQZDkFiA2QQJ0akMAAAAAOAIAIDZBAWohNiA2QQJIBEAMAgwBCwsLQQAhNwNAAkBBmOQWIDdBAnRqQwAAAAA4AgAgN0EBaiE3IDdBgMAASARADAIMAQsLC0EAITgDQAJAQZzkGCA4QQJ0akMAAAAAOAIAIDhBAWohOCA4QQJIBEAMAgwBCwsLQQAhOQNAAkBBpOQYIDlBAnRqQwAAAAA4AgAgOUEBaiE5IDlBAkgEQAwCDAELCwtBACE6A0ACQEGs5BggOkECdGpDAAAAADgCACA6QQFqITogOkGAwABIBEAMAgwBCwsLQQAhOwNAAkBBsOQaIDtBAnRqQwAAAAA4AgAgO0EBaiE7IDtBAkgEQAwCDAELCwtBACE8A0ACQEG45BogPEECdGpDAAAAADgCACA8QQFqITwgPEECSARADAIMAQsLC0EAIT0DQAJAQcDkGiA9QQJ0akMAAAAAOAIAID1BAWohPSA9QYDAAEgEQAwCDAELCwtBACE+A0ACQEHE5BwgPkECdGpDAAAAADgCACA+QQFqIT4gPkECSARADAIMAQsLC0EAIT8DQAJAQczkHCA/QQJ0akMAAAAAOAIAID9BAWohPyA/QQJIBEAMAgwBCwsLQQAhQANAAkBB1OQcIEBBAnRqQwAAAAA4AgAgQEEBaiFAIEBBgMAASARADAIMAQsLC0EAIUEDQAJAQdjkHiBBQQJ0akMAAAAAOAIAIEFBAWohQSBBQQJIBEAMAgwBCwsLQQAhQgNAAkBB4OQeIEJBAnRqQwAAAAA4AgAgQkEBaiFCIEJBAkgEQAwCDAELCwtBACFDA0ACQEHo5B4gQ0ECdGpDAAAAADgCACBDQQFqIUMgQ0GAwABIBEAMAgwBCwsLQQAhRANAAkBB7OQgIERBAnRqQwAAAAA4AgAgREEBaiFEIERBAkgEQAwCDAELCwtBACFFA0ACQEH05CAgRUECdGpDAAAAADgCACBFQQFqIUUgRUECSARADAIMAQsLC0EAIUYDQAJAQfzkICBGQQJ0akMAAAAAOAIAIEZBAWohRiBGQYDAAEgEQAwCDAELCwtBACFHA0ACQEGA5SIgR0ECdGpDAAAAADgCACBHQQFqIUcgR0ECSARADAIMAQsLC0EAIUgDQAJAQYjlIiBIQQJ0akMAAAAAOAIAIEhBAWohSCBIQYAQSARADAIMAQsLC0EAIUkDQAJAQYylIyBJQQJ0akMAAAAAOAIAIElBAWohSSBJQQJIBEAMAgwBCwsLQQAhSgNAAkBBlKUjIEpBAnRqQwAAAAA4AgAgSkEBaiFKIEpBgBBIBEAMAgwBCwsLQQAhSwNAAkBBmOUjIEtBAnRqQwAAAAA4AgAgS0EBaiFLIEtBAkgEQAwCDAELCwtBACFMA0ACQEGg5SMgTEECdGpDAAAAADgCACBMQQFqIUwgTEGAEEgEQAwCDAELCwtBACFNA0ACQEGkpSQgTUECdGpDAAAAADgCACBNQQFqIU0gTUECSARADAIMAQsLC0EAIU4DQAJAQaylJCBOQQJ0akMAAAAAOAIAIE5BAWohTiBOQYAQSARADAIMAQsLC0EAIU8DQAJAQbDlJCBPQQJ0akMAAAAAOAIAIE9BAWohTyBPQQJIBEAMAgwBCwsLC7eFgIAAAQR/Q5GQTjxBACoChIABlKghAkMK1yM8QQAqAoSAAZSoIQNDS2D9O0EAKgKEgAGUqCEEQwUvpztBACoChIABlKghBUEAIAE2AoCAAUEAQwCAO0hDAACAP0EAKAKAgAGyl5Y4AoSAAUEAQwDwQEZBACoChIABlTgCiIABQQBDANCJRkEAKgKEgAGVOAKQgAFBAEMAAIA/QQAqAoSAAZU4AtCAAUEAQ2ZmMEJBACoChIABlTgC4IABQQBDAACAP0EAKgLggAGTOALogAFBAEPJTs88QQAqAoSAAZSoNgKYggNBAEO1rtw8QQAqAoSAAZSoNgKsggVBAEMON+08QQAqAoSAAZSoNgLAggdBAEPb4/s8QQAqAoSAAZSoNgLUgglBAEM6EwQ9QQAqAoSAAZSoNgLoggtBAEPbewo9QQAqAoSAAZSoNgL8gg1BAEMnnRA9QQAqAoSAAZSoNgKQgw9BAEPJLxY9QQAqAoSAAZSoNgKkgxFDkZBOPEEAKgKEgAGUqCECQQBBgAhBACACQX9qEA8QEDYCsMMRQwrXIzxBACoChIABlKghA0EAQYAIQQAgA0F/ahAPEBA2AryDEkNLYP07QQAqAoSAAZSoIQRBAEGACEEAIARBf2oQDxAQNgLIwxJDBS+nO0EAKgKEgAGUqCEFQQBBACAFQX9qEA82AtTjEkEAQQAoApiCA7I4AujjFEEAQxa4iDpBACoChIABlDgC7OMUQQBBACgCrIIFsjgChOQWQQBBACgCwIIHsjgCmOQYQQBBACgC1IIJsjgCrOQaQQBBACgC6IILsjgCwOQcQQBBACgC/IINsjgC1OQeQQBBACgCkIMPsjgC6OQgQQBBACgCpIMRsjgC/OQiQQAgArI4AoilI0EAIAOyOAKU5SNBACAEsjgCoKUkQQAgBbI4AqzlJAuQgICAAAAgACABEAwgABAOIAAQCwv+gYCAAABBAEMzMzM/OAKMgAFBAEMAAAA/OAKUgAFBAEPNzMw+OAKggAFBAEMAAAA/OAKkgAFBAEMAAAA/OAKogAFBAEMAAIA/OAKsgAFBAEMAAAAAOAKwgAFBAEPNzEw/OAK8gAFBAEMK1yM8OALIgAFBAEOamRk/OALMgAFBAEPNzEw+OALUgAFBAEMAAAA/OALkgAFBAEOamZk+OAL0gAFBAEMAAAAAOAKAgQFBAEMAAAAAOAKEgQFBAEMAAAAAOAKQgQFBAEMAAAAAOAKcgQFBAEPNzMw9OAKogQFBAEMzMzM/OALMgQFBAEMAAIBAOAKAggFBAEOamRk/OALw4xQLkICAgAAAIAAgAUgEfyABBSAACw8LkICAgAAAIAAgAUgEfyAABSABCw8LjICAgAAAIAAgAWogAjgCAAsLpLOAgAABAEEAC50zeyJuYW1lIjogIndhdmVzaW4iLCJmaWxlbmFtZSI6ICJ3YXZlc2luLmRzcCIsInZlcnNpb24iOiAiMi40MC4yIiwiY29tcGlsZV9vcHRpb25zIjogIi1sYW5nIHdhc20taWIgLWNuIHdhdmVzaW4gLWVzIDEgLW1jZCAxNiAtc2luZ2xlIC1mdHogMiIsImxpYnJhcnlfbGlzdCI6IFsiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9zdGRmYXVzdC5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L3NpZ25hbHMubGliIiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9tYXRocy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L3BsYXRmb3JtLmxpYiIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QvZW52ZWxvcGVzLmxpYiIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QvYmFzaWNzLmxpYiIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3Qvb3NjaWxsYXRvcnMubGliIiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9taXNjZWZmZWN0cy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L2ZpbHRlcnMubGliIiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9waGFmbGFuZ2Vycy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L2RlbGF5cy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L3NwYXRzLmxpYiIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QvcmV2ZXJicy5saWIiXSwiaW5jbHVkZV9wYXRobmFtZXMiOiBbIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0IiwiL3Vzci9zaGFyZS9mYXVzdCIsIi4iLCIvaG9tZS9taXMvc3JjL19leHRlcm5zL3NlcnZldXItaW5qZWN0aW9uL3N0YXRpYy9yb29tcy85Sm9nOWJHL2ZhdXN0Il0sInNpemUiOiA2MDI4NDAsImlucHV0cyI6IDAsIm91dHB1dHMiOiAyLCJtZXRhIjogWyB7ICJiYXNpY3MubGliL25hbWUiOiAiRmF1c3QgQmFzaWMgRWxlbWVudCBMaWJyYXJ5IiB9LHsgImJhc2ljcy5saWIvdmVyc2lvbiI6ICIwLjUiIH0seyAiY29tcGlsZV9vcHRpb25zIjogIi1sYW5nIHdhc20taWIgLWNuIHdhdmVzaW4gLWVzIDEgLW1jZCAxNiAtc2luZ2xlIC1mdHogMiIgfSx7ICJkZWxheXMubGliL25hbWUiOiAiRmF1c3QgRGVsYXkgTGlicmFyeSIgfSx7ICJkZWxheXMubGliL3ZlcnNpb24iOiAiMC4xIiB9LHsgImVudmVsb3Blcy5saWIvYXV0aG9yIjogIkdSQU1FIiB9LHsgImVudmVsb3Blcy5saWIvY29weXJpZ2h0IjogIkdSQU1FIiB9LHsgImVudmVsb3Blcy5saWIvbGljZW5zZSI6ICJMR1BMIHdpdGggZXhjZXB0aW9uIiB9LHsgImVudmVsb3Blcy5saWIvbmFtZSI6ICJGYXVzdCBFbnZlbG9wZSBMaWJyYXJ5IiB9LHsgImVudmVsb3Blcy5saWIvdmVyc2lvbiI6ICIwLjEiIH0seyAiZmlsZW5hbWUiOiAid2F2ZXNpbi5kc3AiIH0seyAiZmlsdGVycy5saWIvYWxscGFzc19jb21iOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2FsbHBhc3NfY29tYjpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zQGNjcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvYWxscGFzc19jb21iOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9kY2Jsb2NrZXI6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvZGNibG9ja2VyOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NAY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9kY2Jsb2NrZXI6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3MwX2hpZ2hwYXNzMSI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NAY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9uYW1lIjogIkZhdXN0IEZpbHRlcnMgTGlicmFyeSIgfSx7ICJmaWx0ZXJzLmxpYi9wb2xlOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL3BvbGU6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc0BjY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL3BvbGU6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL3ZlcnNpb24iOiAiMC4zIiB9LHsgImZpbHRlcnMubGliL3plcm86YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvemVybzpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zQGNjcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvemVybzpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAibWF0aHMubGliL2F1dGhvciI6ICJHUkFNRSIgfSx7ICJtYXRocy5saWIvY29weXJpZ2h0IjogIkdSQU1FIiB9LHsgIm1hdGhzLmxpYi9saWNlbnNlIjogIkxHUEwgd2l0aCBleGNlcHRpb24iIH0seyAibWF0aHMubGliL25hbWUiOiAiRmF1c3QgTWF0aCBMaWJyYXJ5IiB9LHsgIm1hdGhzLmxpYi92ZXJzaW9uIjogIjIuNSIgfSx7ICJtaXNjZWZmZWN0cy5saWIvbmFtZSI6ICJNaXNjIEVmZmVjdHMgTGlicmFyeSIgfSx7ICJtaXNjZWZmZWN0cy5saWIvdmVyc2lvbiI6ICIyLjAiIH0seyAibmFtZSI6ICJ3YXZlc2luIiB9LHsgIm9zY2lsbGF0b3JzLmxpYi9uYW1lIjogIkZhdXN0IE9zY2lsbGF0b3IgTGlicmFyeSIgfSx7ICJvc2NpbGxhdG9ycy5saWIvdmVyc2lvbiI6ICIwLjMiIH0seyAicGhhZmxhbmdlcnMubGliL25hbWUiOiAiRmF1c3QgUGhhc2VyIGFuZCBGbGFuZ2VyIExpYnJhcnkiIH0seyAicGhhZmxhbmdlcnMubGliL3ZlcnNpb24iOiAiMC4xIiB9LHsgInBsYXRmb3JtLmxpYi9uYW1lIjogIkdlbmVyaWMgUGxhdGZvcm0gTGlicmFyeSIgfSx7ICJwbGF0Zm9ybS5saWIvdmVyc2lvbiI6ICIwLjIiIH0seyAicmV2ZXJicy5saWIvbmFtZSI6ICJGYXVzdCBSZXZlcmIgTGlicmFyeSIgfSx7ICJyZXZlcmJzLmxpYi92ZXJzaW9uIjogIjAuMiIgfSx7ICJzaWduYWxzLmxpYi9uYW1lIjogIkZhdXN0IFNpZ25hbCBSb3V0aW5nIExpYnJhcnkiIH0seyAic2lnbmFscy5saWIvdmVyc2lvbiI6ICIwLjEiIH0seyAic3BhdHMubGliL25hbWUiOiAiRmF1c3QgU3BhdGlhbGl6YXRpb24gTGlicmFyeSIgfSx7ICJzcGF0cy5saWIvdmVyc2lvbiI6ICIwLjAiIH1dLCJ1aSI6IFsgeyJ0eXBlIjogInZncm91cCIsImxhYmVsIjogIndhdmVzaW4iLCJpdGVtcyI6IFsgeyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJhIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9hIiwiaW5kZXgiOiAxNjQ1NiwibWV0YSI6IFt7ICJtaWRpIjogImN0cmwgNzMiIH1dLCJpbml0IjogMC4wMSwibWluIjogMC4wMSwibWF4IjogNCwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAiYmVuZCIsImFkZHJlc3MiOiAiL3dhdmVzaW4vYmVuZCIsImluZGV4IjogMTY1MTYsIm1ldGEiOiBbeyAibWlkaSI6ICJwaXRjaHdoZWVsIiB9XSwiaW5pdCI6IDAsIm1pbiI6IC0yLCJtYXgiOiAyLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJkIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9kIiwiaW5kZXgiOiAxNjQ2MCwibWV0YSI6IFt7ICJtaWRpIjogImN0cmwgNzYiIH1dLCJpbml0IjogMC42LCJtaW4iOiAwLjAxLCJtYXgiOiA4LCJzdGVwIjogMC4wMX0seyJ0eXBlIjogInZzbGlkZXIiLCJsYWJlbCI6ICJkYW1wIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9kYW1wIiwiaW5kZXgiOiAxNjQwNCwibWV0YSI6IFt7ICJtaWRpIjogImN0cmwgOTUiIH1dLCJpbml0IjogMC41LCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMjV9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAiZHJpdmUiLCJhZGRyZXNzIjogIi93YXZlc2luL2RyaXZlIiwiaW5kZXgiOiAxNjUwMCwibWV0YSI6IFt7ICJCRUxBIjogIkFOQUxPR180IiB9XSwiaW5pdCI6IDAuMywibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogImRyeXdldGZsYW5nIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9kcnl3ZXRmbGFuZyIsImluZGV4IjogMTY0MjQsIm1ldGEiOiBbeyAiQkVMQSI6ICJBTkFMT0dfNSIgfV0sImluaXQiOiAwLjUsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAwMX0seyJ0eXBlIjogInZzbGlkZXIiLCJsYWJlbCI6ICJkcnl3ZXRyZXZlcmIiLCJhZGRyZXNzIjogIi93YXZlc2luL2RyeXdldHJldmVyYiIsImluZGV4IjogMTY0MTYsIm1ldGEiOiBbeyAiQkVMQSI6ICJBTkFMT0dfNiIgfV0sImluaXQiOiAwLjQsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAwMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJmbGFuZ2RlbCIsImFkZHJlc3MiOiAiL3dhdmVzaW4vZmxhbmdkZWwiLCJpbmRleCI6IDE2NjQwLCJtZXRhIjogW3sgIm1pZGkiOiAiY3RybCAxMyIgfV0sImluaXQiOiA0LCJtaW4iOiAwLjAwMSwibWF4IjogMTAsInN0ZXAiOiAwLjAwMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJmbGFuZ2ZlZWRiYWNrIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9mbGFuZ2ZlZWRiYWNrIiwiaW5kZXgiOiAxNjU4OCwibWV0YSI6IFt7ICJtaWRpIjogImN0cmwgOTQiIH1dLCJpbml0IjogMC43LCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMDF9LHsidHlwZSI6ICJuZW50cnkiLCJsYWJlbCI6ICJmcmVxIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9mcmVxIiwiaW5kZXgiOiAxNjUxMiwibWV0YSI6IFt7ICJ1bml0IjogIkh6IiB9XSwiaW5pdCI6IDAsIm1pbiI6IDAsIm1heCI6IDIwMDAwLCJzdGVwIjogMX0seyJ0eXBlIjogIm5lbnRyeSIsImxhYmVsIjogImdhaW4iLCJhZGRyZXNzIjogIi93YXZlc2luL2dhaW4iLCJpbmRleCI6IDE2NDg0LCJpbml0IjogMC41LCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogImJ1dHRvbiIsImxhYmVsIjogImdhdGUiLCJhZGRyZXNzIjogIi93YXZlc2luL2dhdGUiLCJpbmRleCI6IDE2NDMyfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogImxmb2RlcHRoIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9sZm9kZXB0aCIsImluZGV4IjogMTY1NDAsIm1ldGEiOiBbeyAiQkVMQSI6ICJBTkFMT0dfMiIgfV0sImluaXQiOiAwLCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAibGZvZnJlcSIsImFkZHJlc3MiOiAiL3dhdmVzaW4vbGZvZnJlcSIsImluZGV4IjogMTY1NTIsIm1ldGEiOiBbeyAiQkVMQSI6ICJBTkFMT0dfMSIgfV0sImluaXQiOiAwLjEsIm1pbiI6IDAuMDEsIm1heCI6IDEwLCJzdGVwIjogMC4wMDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAicGFuIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9wYW4iLCJpbmRleCI6IDE2NDIwLCJtZXRhIjogW3sgIm1pZGkiOiAiY3RybCAxMCIgfV0sImluaXQiOiAwLjUsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAwMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJyIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi9yIiwiaW5kZXgiOiAxNjQ0NCwibWV0YSI6IFt7ICJCRUxBIjogIkFOQUxPR18zIiB9XSwiaW5pdCI6IDAuOCwibWluIjogMC4wMSwibWF4IjogOCwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJ2c2xpZGVyIiwibGFiZWwiOiAicm9vbXNpemUiLCJhZGRyZXNzIjogIi93YXZlc2luL3Jvb21zaXplIiwiaW5kZXgiOiAxNjM5NiwibWV0YSI6IFt7ICJCRUxBIjogIkFOQUxPR183IiB9XSwiaW5pdCI6IDAuNywibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDI1fSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogInMiLCJhZGRyZXNzIjogIi93YXZlc2luL3MiLCJpbmRleCI6IDE2NDY4LCJtZXRhIjogW3sgIm1pZGkiOiAiY3RybCA3NyIgfV0sImluaXQiOiAwLjIsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAxfSx7InR5cGUiOiAidnNsaWRlciIsImxhYmVsIjogInN0ZXJlbyIsImFkZHJlc3MiOiAiL3dhdmVzaW4vc3RlcmVvIiwiaW5kZXgiOiAzNDA0NjQsIm1ldGEiOiBbeyAibWlkaSI6ICJjdHJsIDkwIiB9XSwiaW5pdCI6IDAuNiwibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAidm9sdW1lIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi92b2x1bWUiLCJpbmRleCI6IDE2NDI4LCJtZXRhIjogW3sgIm1pZGkiOiAiY3RybCA3IiB9XSwiaW5pdCI6IDEsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAwMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJ3YXZldHJhdmVsIiwiYWRkcmVzcyI6ICIvd2F2ZXNpbi93YXZldHJhdmVsIiwiaW5kZXgiOiAxNjUyOCwibWV0YSI6IFt7ICJCRUxBIjogIkFOQUxPR18wIiB9XSwiaW5pdCI6IDAsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAxfV19XX0="; }

/*
 faust2wasm: GRAME 2017-2019
*/

'use strict';

// Monophonic Faust DSP
class wavesinProcessor extends AudioWorkletProcessor {

    // JSON parsing functions
    static parse_ui(ui, obj, callback) {
        for (var i = 0; i < ui.length; i++) {
            wavesinProcessor.parse_group(ui[i], obj, callback);
        }
    }

    static parse_group(group, obj, callback) {
        if (group.items) {
            wavesinProcessor.parse_items(group.items, obj, callback);
        }
    }

    static parse_items(items, obj, callback) {
        for (var i = 0; i < items.length; i++) {
            callback(items[i], obj, callback);
        }
    }

    static parse_item1(item, obj, callback) {
        if (item.type === "vgroup"
            || item.type === "hgroup"
            || item.type === "tgroup") {
            wavesinProcessor.parse_items(item.items, obj, callback);
        } else if (item.type === "hbargraph"
            || item.type === "vbargraph") {
            // Nothing
        } else if (item.type === "vslider"
            || item.type === "hslider"
            || item.type === "button"
            || item.type === "checkbox"
            || item.type === "nentry") {
            obj.push({
                name: item.address,
                defaultValue: item.init,
                minValue: item.min,
                maxValue: item.max
            });
        }
    }

    static parse_item2(item, obj, callback) {
        if (item.type === "vgroup"
            || item.type === "hgroup"
            || item.type === "tgroup") {
            wavesinProcessor.parse_items(item.items, obj, callback);
        } else if (item.type === "hbargraph"
            || item.type === "vbargraph") {
            // Keep bargraph adresses
            obj.outputs_items.push(item.address);
            obj.pathTable[item.address] = parseInt(item.index);
        } else if (item.type === "soundfile") {
            // Keep soundfile adresses
            obj.soundfile_items.push(item.address);
            obj.pathTable[item.address] = parseInt(item.index);
        } else if (item.type === "vslider"
            || item.type === "hslider"
            || item.type === "button"
            || item.type === "checkbox"
            || item.type === "nentry") {
            // Keep inputs adresses
            obj.inputs_items.push(item.address);
            obj.pathTable[item.address] = parseInt(item.index);
        }
    }

    static get parameterDescriptors() {
        // Analyse JSON to generate AudioParam parameters
        var params = [];
        wavesinProcessor.parse_ui(JSON.parse(getJSONwavesin()).ui, params, wavesinProcessor.parse_item1);
        return params;
    }

    constructor(options) {
        super(options);
        this.running = true;

        const importObject = {
            env: {
                memoryBase: 0,
                tableBase: 0,

                // Integer version
                _abs: Math.abs,

                // Float version
                _acosf: Math.acos,
                _asinf: Math.asin,
                _atanf: Math.atan,
                _atan2f: Math.atan2,
                _ceilf: Math.ceil,
                _cosf: Math.cos,
                _expf: Math.exp,
                _floorf: Math.floor,
                _fmodf: function (x, y) { return x % y; },
                _logf: Math.log,
                _log10f: Math.log10,
                _max_f: Math.max,
                _min_f: Math.min,
                _remainderf: function (x, y) { return x - Math.round(x / y) * y; },
                _powf: Math.pow,
                _roundf: Math.fround,
                _sinf: Math.sin,
                _sqrtf: Math.sqrt,
                _tanf: Math.tan,
                _acoshf: Math.acosh,
                _asinhf: Math.asinh,
                _atanhf: Math.atanh,
                _coshf: Math.cosh,
                _sinhf: Math.sinh,
                _tanhf: Math.tanh,
                _isnanf: Number.isNaN,
                _isinff: function (x) { return !isFinite(x); },
                _copysignf: function (x, y) { return Math.sign(x) === Math.sign(y) ? x : -x; },

                // Double version
                _acos: Math.acos,
                _asin: Math.asin,
                _atan: Math.atan,
                _atan2: Math.atan2,
                _ceil: Math.ceil,
                _cos: Math.cos,
                _exp: Math.exp,
                _floor: Math.floor,
                _fmod: function (x, y) { return x % y; },
                _log: Math.log,
                _log10: Math.log10,
                _max_: Math.max,
                _min_: Math.min,
                _remainder: function (x, y) { return x - Math.round(x / y) * y; },
                _pow: Math.pow,
                _round: Math.fround,
                _sin: Math.sin,
                _sqrt: Math.sqrt,
                _tan: Math.tan,
                _acosh: Math.acosh,
                _asinh: Math.asinh,
                _atanh: Math.atanh,
                _cosh: Math.cosh,
                _sinh: Math.sinh,
                _tanh: Math.tanh,
                _isnan: Number.isNaN,
                _isinf: function (x) { return !isFinite(x); },
                _copysign: function (x, y) { return Math.sign(x) === Math.sign(y) ? x : -x; },

                table: new WebAssembly.Table({ initial: 0, element: 'anyfunc' })
            }
        };

        this.wavesin_instance = new WebAssembly.Instance(options.processorOptions.wasm_module, importObject);
        this.json_object = JSON.parse(options.processorOptions.json);

        this.output_handler = function (path, value) { this.port.postMessage({ path: path, value: value }); };

        this.ins = null;
        this.outs = null;

        this.dspInChannnels = [];
        this.dspOutChannnels = [];

        this.numIn = parseInt(this.json_object.inputs);
        this.numOut = parseInt(this.json_object.outputs);

        // Memory allocator
        this.ptr_size = 4;
        this.sample_size = 4;
        this.integer_size = 4;

        this.factory = this.wavesin_instance.exports;
        this.HEAP = this.wavesin_instance.exports.memory.buffer;
        this.HEAP32 = new Int32Array(this.HEAP);
        this.HEAPF32 = new Float32Array(this.HEAP);

        // Warning: keeps a ref on HEAP in Chrome and prevent proper GC
        //console.log(this.HEAP);
        //console.log(this.HEAP32);
        //console.log(this.HEAPF32);

        // bargraph
        this.outputs_timer = 5;
        this.outputs_items = [];

        // input items
        this.inputs_items = [];

        // soundfile items
        this.soundfile_items = [];

        // Start of HEAP index

        // DSP is placed first with index 0. Audio buffer start at the end of DSP.
        this.audio_heap_ptr = parseInt(this.json_object.size);

        // Setup pointers offset
        this.audio_heap_ptr_inputs = this.audio_heap_ptr;
        this.audio_heap_ptr_outputs = this.audio_heap_ptr_inputs + (this.numIn * this.ptr_size);

        // Setup buffer offset
        this.audio_heap_inputs = this.audio_heap_ptr_outputs + (this.numOut * this.ptr_size);
        this.audio_heap_outputs = this.audio_heap_inputs + (this.numIn * NUM_FRAMES * this.sample_size);

        // Start of DSP memory : DSP is placed first with index 0
        this.dsp = 0;

        this.pathTable = [];

        // Send output values to the AudioNode
        this.update_outputs = function () {
            if (this.outputs_items.length > 0 && this.output_handler && this.outputs_timer-- === 0) {
                this.outputs_timer = 5;
                for (var i = 0; i < this.outputs_items.length; i++) {
                    this.output_handler(this.outputs_items[i], this.HEAPF32[this.pathTable[this.outputs_items[i]] >> 2]);
                }
            }
        }

        this.initAux = function () {
            var i;

            if (this.numIn > 0) {
                this.ins = this.audio_heap_ptr_inputs;
                for (i = 0; i < this.numIn; i++) {
                    this.HEAP32[(this.ins >> 2) + i] = this.audio_heap_inputs + ((NUM_FRAMES * this.sample_size) * i);
                }

                // Prepare Ins buffer tables
                var dspInChans = this.HEAP32.subarray(this.ins >> 2, (this.ins + this.numIn * this.ptr_size) >> 2);
                for (i = 0; i < this.numIn; i++) {
                    this.dspInChannnels[i] = this.HEAPF32.subarray(dspInChans[i] >> 2, (dspInChans[i] + NUM_FRAMES * this.sample_size) >> 2);
                }
            }

            if (this.numOut > 0) {
                this.outs = this.audio_heap_ptr_outputs;
                for (i = 0; i < this.numOut; i++) {
                    this.HEAP32[(this.outs >> 2) + i] = this.audio_heap_outputs + ((NUM_FRAMES * this.sample_size) * i);
                }

                // Prepare Out buffer tables
                var dspOutChans = this.HEAP32.subarray(this.outs >> 2, (this.outs + this.numOut * this.ptr_size) >> 2);
                for (i = 0; i < this.numOut; i++) {
                    this.dspOutChannnels[i] = this.HEAPF32.subarray(dspOutChans[i] >> 2, (dspOutChans[i] + NUM_FRAMES * this.sample_size) >> 2);
                }
            }

            // Parse UI
            wavesinProcessor.parse_ui(this.json_object.ui, this, wavesinProcessor.parse_item2);

            // Init DSP
            this.factory.init(this.dsp, sampleRate); // 'sampleRate' is defined in AudioWorkletGlobalScope  
        }

        this.setParamValue = function (path, val) {
            this.HEAPF32[this.pathTable[path] >> 2] = val;
        }

        this.getParamValue = function (path) {
            return this.HEAPF32[this.pathTable[path] >> 2];
        }

        // Init resulting DSP
        this.initAux();
        console.log(this);
    }

    handleMessage(event) {
        var msg = event.data;
        switch (msg.type) {
            case "destroy": this.running = false; break;
        }
    }

    process(inputs, outputs, parameters) {
        var input = inputs[0];
        var output = outputs[0];

        // Check inputs
        if (this.numIn > 0 && (!input || !input[0] || input[0].length === 0)) {
            //console.log("Process input error");
            return true;
        }
        // Check outputs
        if (this.numOut > 0 && (!output || !output[0] || output[0].length === 0)) {
            //console.log("Process output error");
            return true;
        }

        // Copy inputs
        if (input !== undefined) {
            for (var chan = 0; chan < Math.min(this.numIn, input.length); ++chan) {
                var dspInput = this.dspInChannnels[chan];
                dspInput.set(input[chan]);
            }
        }

        /*
        TODO: sample accurate control change is not yet handled
        When no automation occurs, params[i][1] has a length of 1,
        otherwise params[i][1] has a length of NUM_FRAMES with possible control change each sample
        */

        // Update controls
        for (const path in parameters) {
            const paramArray = parameters[path];
            this.setParamValue(path, paramArray[0]);
        }

        // Compute
        try {
            this.factory.compute(this.dsp, NUM_FRAMES, this.ins, this.outs);
        } catch (e) {
            console.log("ERROR in compute (" + e + ")");
        }

        // Update bargraph
        this.update_outputs();

        // Copy outputs
        if (output !== undefined) {
            for (var chan = 0; chan < Math.min(this.numOut, output.length); ++chan) {
                var dspOutput = this.dspOutChannnels[chan];
                output[chan].set(dspOutput);
            }
        }

        return this.running;
    }
}

// Globals
const NUM_FRAMES = 128;
try {
    registerProcessor('wavesin', wavesinProcessor);
} catch (error) {
    console.warn(error);
}
