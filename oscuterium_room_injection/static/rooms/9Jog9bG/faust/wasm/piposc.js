
/*
Code generated with Faust version 2.40.2
Compilation options: -lang wasm-ib -cn piposc -es 1 -mcd 16 -single -ftz 2 
*/

function getJSONpiposc() {
	return '{"name": "piposc","filename": "piposc.dsp","version": "2.40.2","compile_options": "-lang wasm-ib -cn piposc -es 1 -mcd 16 -single -ftz 2","library_list": ["/usr/local/share/faust/stdfaust.lib","/usr/local/share/faust/signals.lib","/usr/local/share/faust/maths.lib","/usr/local/share/faust/platform.lib","/usr/local/share/faust/reverbs.lib","/usr/local/share/faust/delays.lib","/usr/local/share/faust/basics.lib","/usr/local/share/faust/filters.lib","/usr/local/share/faust/routes.lib"],"include_pathnames": ["/usr/local/share/faust","/usr/local/share/faust","/usr/share/faust",".","/home/mis/src/_externs/serveur-injection/static/rooms/9Jog9bG/faust"],"size": 844300,"inputs": 0,"outputs": 2,"meta": [ { "basics.lib/name": "Faust Basic Element Library" },{ "basics.lib/version": "0.5" },{ "compile_options": "-lang wasm-ib -cn piposc -es 1 -mcd 16 -single -ftz 2" },{ "delays.lib/name": "Faust Delay Library" },{ "delays.lib/version": "0.1" },{ "filename": "piposc.dsp" },{ "filters.lib/allpass_comb:author": "Julius O. Smith III" },{ "filters.lib/allpass_comb:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/allpass_comb:license": "MIT-style STK-4.3 license" },{ "filters.lib/lowpass0_highpass1": "MIT-style STK-4.3 license" },{ "filters.lib/lowpass0_highpass1:author": "Julius O. Smith III" },{ "filters.lib/lowpass:author": "Julius O. Smith III" },{ "filters.lib/lowpass:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/lowpass:license": "MIT-style STK-4.3 license" },{ "filters.lib/name": "Faust Filters Library" },{ "filters.lib/tf1:author": "Julius O. Smith III" },{ "filters.lib/tf1:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/tf1:license": "MIT-style STK-4.3 license" },{ "filters.lib/tf1s:author": "Julius O. Smith III" },{ "filters.lib/tf1s:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },{ "filters.lib/tf1s:license": "MIT-style STK-4.3 license" },{ "filters.lib/version": "0.3" },{ "maths.lib/author": "GRAME" },{ "maths.lib/copyright": "GRAME" },{ "maths.lib/license": "LGPL with exception" },{ "maths.lib/name": "Faust Math Library" },{ "maths.lib/version": "2.5" },{ "name": "piposc" },{ "platform.lib/name": "Generic Platform Library" },{ "platform.lib/version": "0.2" },{ "reverbs.lib/name": "Faust Reverb Library" },{ "reverbs.lib/version": "0.2" },{ "routes.lib/hadamard:author": "Remy Muller, revised by RM" },{ "routes.lib/name": "Faust Signal Routing Library" },{ "routes.lib/version": "0.2" },{ "signals.lib/name": "Faust Signal Routing Library" },{ "signals.lib/version": "0.1" }],"ui": [ {"type": "vgroup","label": "piposc","items": [ {"type": "hslider","label": "freq","address": "/piposc/freq","index": 131168,"meta": [{ "unit": "Hz" }],"init": 440,"min": 20,"max": 20000,"step": 1},{"type": "hslider","label": "gain","address": "/piposc/gain","index": 131132,"init": 0.5,"min": 0,"max": 10,"step": 0.01},{"type": "button","label": "gate","address": "/piposc/gate","index": 131128},{"type": "hslider","label": "reverbgain","address": "/piposc/reverbgain","index": 131148,"init": 0.137,"min": 0,"max": 1,"step": 0.01},{"type": "hslider","label": "roomsize","address": "/piposc/roomsize","index": 8,"init": 0.72,"min": 0.01,"max": 2,"step": 0.01},{"type": "hslider","label": "volume","address": "/piposc/volume","index": 131124,"init": 0.5,"min": 0,"max": 1,"step": 0.01}]}]}';
}
function getBase64Codepiposc() { return "AGFzbQEAAAAB64CAgAAUYAJ/fwBgBH9/f38AYAF9AX1gAX0BfWACfX0BfWABfwF/YAF/AX9gAn9/AX1gAX8Bf2ACf38AYAF/AGACf38AYAJ/fwBgAX8AYAJ/fwF/YAJ/fwF/YAJ9fQF9YAN/f30AYAF9AX1gAX0BfQLKgICAAAYDZW52BV9jb3NmAAIDZW52BV9leHBmAAMDZW52Bl9mbW9kZgAEA2VudgVfcG93ZgAQA2VudgVfc2luZgASA2VudgVfdGFuZgATA4+AgIAADgABBQYHCAkKCwwNDg8RBYyAgIAAAQGQgICAAPiHgIAAB7qBgIAADAdjb21wdXRlAAcMZ2V0TnVtSW5wdXRzAAgNZ2V0TnVtT3V0cHV0cwAJDWdldFBhcmFtVmFsdWUACg1nZXRTYW1wbGVSYXRlAAsEaW5pdAAMDWluc3RhbmNlQ2xlYXIADRFpbnN0YW5jZUNvbnN0YW50cwAODGluc3RhbmNlSW5pdAAPGmluc3RhbmNlUmVzZXRVc2VySW50ZXJmYWNlABANc2V0UGFyYW1WYWx1ZQATBm1lbW9yeQIACqPdgIAADoKAgIAAAAuBuICAAAIDf5cBfUEAIQRBACEFQwAAAAAhB0MAAAAAIQhDAAAAACEJQwAAAAAhCkMAAAAAIQtDAAAAACEMQwAAAAAhDUMAAAAAIQ5DAAAAACEPQwAAAAAhEEMAAAAAIRFDAAAAACESQwAAAAAhE0MAAAAAIRRDAAAAACEVQwAAAAAhFkMAAAAAIRdDAAAAACEYQwAAAAAhGUMAAAAAIRpDAAAAACEbQwAAAAAhHEMAAAAAIR1DAAAAACEeQwAAAAAhH0MAAAAAISBDAAAAACEhQwAAAAAhIkMAAAAAISNDAAAAACEkQwAAAAAhJUMAAAAAISZDAAAAACEnQwAAAAAhKEMAAAAAISlDAAAAACEqQwAAAAAhK0MAAAAAISxDAAAAACEtQwAAAAAhLkMAAAAAIS9DAAAAACEwQwAAAAAhMUMAAAAAITJDAAAAACEzQwAAAAAhNEMAAAAAITVDAAAAACE2QwAAAAAhN0MAAAAAIThDAAAAACE5QwAAAAAhOkMAAAAAITtDAAAAACE8QwAAAAAhPUMAAAAAIT5DAAAAACE/QwAAAAAhQEMAAAAAIUFDAAAAACFCQwAAAAAhQ0MAAAAAIURDAAAAACFFQwAAAAAhRkMAAAAAIUdDAAAAACFIQwAAAAAhSUMAAAAAIUpDAAAAACFLQwAAAAAhTEMAAAAAIU1DAAAAACFOQwAAAAAhT0MAAAAAIVBDAAAAACFRQwAAAAAhUkMAAAAAIVNDAAAAACFUQwAAAAAhVUMAAAAAIVZBACEGQwAAAAAhV0MAAAAAIVhDAAAAACFZQwAAAAAhWkMAAAAAIVtDAAAAACFcQwAAAAAhXUMAAAAAIV5DAAAAACFfQwAAAAAhYEMAAAAAIWFDAAAAACFiQwAAAAAhY0MAAAAAIWRDAAAAACFlQwAAAAAhZkMAAAAAIWdDAAAAACFoQwAAAAAhaUMAAAAAIWpDAAAAACFrQwAAAAAhbEMAAAAAIW1DAAAAACFuQwAAAAAhb0MAAAAAIXBDAAAAACFxQwAAAAAhckMAAAAAIXNDAAAAACF0QwAAAAAhdUMAAAAAIXZDAAAAACF3QwAAAAAheEMAAAAAIXlDAAAAACF6QwAAAAAhe0MAAAAAIXxDAAAAACF9QwAAAAAhfkMAAAAAIX9DAAAAACGAAUMAAAAAIYEBQwAAAAAhggFDAAAAACGDAUMAAAAAIYQBQwAAAAAhhQFDAAAAACGGAUMAAAAAIYcBQwAAAAAhiAFDAAAAACGJAUMAAAAAIYoBQwAAAAAhiwFDAAAAACGMAUMAAAAAIY0BQwAAAAAhjgFDAAAAACGPAUMAAAAAIZABQwAAAAAhkQFDAAAAACGSAUMAAAAAIZMBQwAAAAAhlAFDAAAAACGVAUMAAAAAIZYBQwAAAAAhlwFDAAAAACGYAUMAAAAAIZkBQwAAAAAhmgFDAAAAACGbAUMAAAAAIZwBQwAAAAAhnQEgA0EAaigCACEEIANBBGooAgAhBUEAKgIIIQdBACoCBCAHlRABIQggCEMAAABAEAMhCUMAAIA/QQAqAgwgCZSTIQpDAACAPyAJkyELQwAAAAAgCkMAAABAEAMgC0MAAABAEAOVQwAAgL+Sl5EhDCAKIAuVIQ0gCCAMQwAAgD8gDZOSlCEOQQAqAhAgB5UQASAIlUMAAIC/kiEPIA0gDJMhEEEAKgK0gAghEUNvEgM6QQAqAriACEEAKgK8gAiUlCESQQAqAsiACEEAKgLMgAiUIRNBACoC4IAIIRRBACoC3IAIIBSUIRVBACoC7IAIIBSUIRZBACoC+IAIIBSUIRdBACoClMEJIAeVEAEhGCAYQwAAAEAQAyEZQwAAgD9BACoCDCAZlJMhGkMAAIA/IBmTIRtDAAAAACAaQwAAAEAQAyAbQwAAAEAQA5VDAACAv5KXkSEcIBogG5UhHSAYIBxDAACAPyAdk5KUIR5BACoCmMEJIAeVEAEgGJVDAACAv5IhHyAdIByTISBBACoCvMESIAeVEAEhISAhQwAAAEAQAyEiQwAAgD9BACoCDCAilJMhI0MAAIA/ICKTISRDAAAAACAjQwAAAEAQAyAkQwAAAEAQA5VDAACAv5KXkSElICMgJJUhJiAhICVDAACAPyAmk5KUISdBACoCwMESIAeVEAEgIZVDAACAv5IhKCAmICWTISlBACoC5MEXIAeVEAEhKiAqQwAAAEAQAyErQwAAgD9BACoCDCArlJMhLEMAAIA/ICuTIS1DAAAAACAsQwAAAEAQAyAtQwAAAEAQA5VDAACAv5KXkSEuICwgLZUhLyAqIC5DAACAPyAvk5KUITBBACoC6MEXIAeVEAEgKpVDAACAv5IhMSAvIC6TITJBACoCjMIgIAeVEAEhMyAzQwAAAEAQAyE0QwAAgD9BACoCDCA0lJMhNUMAAIA/IDSTITZDAAAAACA1QwAAAEAQAyA2QwAAAEAQA5VDAACAv5KXkSE3IDUgNpUhOCAzIDdDAACAPyA4k5KUITlBACoCkMIgIAeVEAEgM5VDAACAv5IhOiA4IDeTITtBACoCtIIlIAeVEAEhPCA8QwAAAEAQAyE9QwAAgD9BACoCDCA9lJMhPkMAAIA/ID2TIT9DAAAAACA+QwAAAEAQAyA/QwAAAEAQA5VDAACAv5KXkSFAID4gP5UhQSA8IEBDAACAPyBBk5KUIUJBACoCuIIlIAeVEAEgPJVDAACAv5IhQyBBIECTIURBACoC3IIqIAeVEAEhRSBFQwAAAEAQAyFGQwAAgD9BACoCDCBGlJMhR0MAAIA/IEaTIUhDAAAAACBHQwAAAEAQAyBIQwAAAEAQA5VDAACAv5KXkSFJIEcgSJUhSiBFIElDAACAPyBKk5KUIUtBACoC4IIqIAeVEAEgRZVDAACAv5IhTCBKIEmTIU1BACoChIMvIAeVEAEhTiBOQwAAAEAQAyFPQwAAgD9BACoCDCBPlJMhUEMAAIA/IE+TIVFDAAAAACBQQwAAAEAQAyBRQwAAAEAQA5VDAACAv5KXkSFSIFAgUZUhUyBOIFJDAACAPyBTk5KUIVRBACoCiIMvIAeVEAEgTpVDAACAv5IhVSBTIFKTIVZBACEGA0ACQEMAAAAAQQAqAhRBACoCGEEAKgIglEEAKgKExDNBACoCiMQzkpOUkyFXQQAgV7xBgICA/AdxBH0gVwVDAAAAAAs4AhwgDkEAKgKExDMgD0EAKgIclJKUIBBBACoCKJSSIVhBACBYvEGAgID8B3EEfSBYBUMAAAAACzgCJEEwQQAoAixB//8BcUECdGpD8wS1PkEAKgIklEMI5TwekjgCACASQzvffz9BACoCxIAIlJIhWUEAIFm8QYCAgPwHcQR9IFkFQwAAAAALOALAgAggE0EAKgLQgAhBACoC2IAIlJIhWkEAIFq8QYCAgPwHcQR9IFoFQwAAAAALOALUgAggFUEAKgLogAiSQwAAgD8QAiFbQQAgW7xBgICA/AdxBH0gWwVDAAAAAAs4AuSACCAWQQAqAvSACJJDAACAPxACIVxBACBcvEGAgID8B3EEfSBcBUMAAAAACzgC8IAIIBdBACoCgIEIkkMAAIA/EAIhXUEAIF28QYCAgPwHcQR9IF0FQwAAAAALOAL8gAhD2w/JQEEAKgLkgAiUEARDAAAAP0PbD8lAQQAqAvCACJQQBJSSQwAAgD5D2w/JQEEAKgL8gAiUEASUkiFeQYSBCEEAKAIsQf8fcUECdGogEUEAKgLAgAhBACoC1IAIlCBelJQ4AgBDmpmZPkGEgQhBACgCLEEAKAKEgQlrQf8fcUECdGoqAgCUIV9DmpkZP0EAKgKQwQmUQTBBACgCLEEAKAKwgAhrQf//AXFBAnRqKgIAkiBfkyFgQYiBCUEAKAIsQf8PcUECdGogYDgCAEGIgQlBACgCLEEAKAKIwQlrQf8PcUECdGoqAgAhYUEAIGG8QYCAgPwHcQR9IGEFQwAAAAALOAKMwQlDAAAAAEOamRk/IGCUkyFiIGK8QYCAgPwHcQR9IGIFQwAAAAALIWNDAAAAAEEAKgIUQQAqAhhBACoCoMEJlEEAKgLUwzNBACoC2MMzkpOUkyFkQQAgZLxBgICA/AdxBH0gZAVDAAAAAAs4ApzBCSAeQQAqAtTDMyAfQQAqApzBCZSSlCAgQQAqAqjBCZSSIWVBACBlvEGAgID8B3EEfSBlBUMAAAAACzgCpMEJQazBCUEAKAIsQf//AXFBAnRqQ/MEtT5BACoCpMEJlEMI5TwekjgCAEOamRk/QQAqArjBEpRBrMEJQQAoAixBACgCrMERa0H//wFxQQJ0aioCAJIgX5MhZkGwwRFBACgCLEH/H3FBAnRqIGY4AgBBsMERQQAoAixBACgCsMESa0H/H3FBAnRqKgIAIWdBACBnvEGAgID8B3EEfSBnBUMAAAAACzgCtMESQwAAAABDmpkZPyBmlJMhaCBovEGAgID8B3EEfSBoBUMAAAAACyFpQwAAAABBACoCFEEAKgIYQQAqAsjBEpRBACoC7MMzQQAqAvDDM5KTlJMhakEAIGq8QYCAgPwHcQR9IGoFQwAAAAALOALEwRIgJ0EAKgLswzMgKEEAKgLEwRKUkpQgKUEAKgLQwRKUkiFrQQAga7xBgICA/AdxBH0gawVDAAAAAAs4AszBEkHUwRJBACgCLEH//wBxQQJ0akPzBLU+QQAqAszBEpRDCOU8HpI4AgBB1MESQQAoAixBACgC1MEWa0H//wBxQQJ0aioCACBfQ5qZGT9BACoC4MEXlJKSIWxB2MEWQQAoAixB/x9xQQJ0aiBsOAIAQdjBFkEAKAIsQQAoAtjBF2tB/x9xQQJ0aioCACFtQQAgbbxBgICA/AdxBH0gbQVDAAAAAAs4AtzBF0MAAAAAQ5qZGT8gbJSTIW4gbrxBgICA/AdxBH0gbgVDAAAAAAshb0MAAAAAQQAqAhRBACoCGEEAKgLwwReUQQAqArzDM0EAKgLAwzOSk5STIXBBACBwvEGAgID8B3EEfSBwBUMAAAAACzgC7MEXIDBBACoCvMMzIDFBACoC7MEXlJKUIDJBACoC+MEXlJIhcUEAIHG8QYCAgPwHcQR9IHEFQwAAAAALOAL0wRdB/MEXQQAoAixB//8BcUECdGpD8wS1PkEAKgL0wReUQwjlPB6SOAIAQfzBF0EAKAIsQQAoAvzBH2tB//8BcUECdGoqAgAgX0OamRk/QQAqAojCIJSSkiFyQYDCH0EAKAIsQf8fcUECdGogcjgCAEGAwh9BACgCLEEAKAKAwiBrQf8fcUECdGoqAgAhc0EAIHO8QYCAgPwHcQR9IHMFQwAAAAALOAKEwiBDAAAAAEOamRk/IHKUkyF0IHS8QYCAgPwHcQR9IHQFQwAAAAALIXVDAAAAAEEAKgIUQQAqAhhBACoCmMIglEEAKgL4wzNBACoC/MMzkpOUkyF2QQAgdrxBgICA/AdxBH0gdgVDAAAAAAs4ApTCICA5QQAqAvjDMyA6QQAqApTCIJSSlCA7QQAqAqDCIJSSIXdBACB3vEGAgID8B3EEfSB3BUMAAAAACzgCnMIgQaTCIEEAKAIsQf//AHFBAnRqQ/MEtT5BACoCnMIglEMI5TwekjgCAEGkwiBBACgCLEEAKAKkwiRrQf//AHFBAnRqKgIAIF9DmpkZP0EAKgKwgiWUkpMheEGowiRBACgCLEH/D3FBAnRqIHg4AgBBqMIkQQAoAixBACgCqIIla0H/D3FBAnRqKgIAIXlBACB5vEGAgID8B3EEfSB5BUMAAAAACzgCrIIlQ5qZGT8geJQheiB6vEGAgID8B3EEfSB6BUMAAAAACyF7QwAAAABBACoCFEEAKgIYQQAqAsCCJZRBACoCyMMzQQAqAszDM5KTlJMhfEEAIHy8QYCAgPwHcQR9IHwFQwAAAAALOAK8giUgQkEAKgLIwzMgQ0EAKgK8giWUkpQgREEAKgLIgiWUkiF9QQAgfbxBgICA/AdxBH0gfQVDAAAAAAs4AsSCJUHMgiVBACgCLEH//wBxQQJ0akPzBLU+QQAqAsSCJZRDCOU8HpI4AgBBzIIlQQAoAixBACgCzIIpa0H//wBxQQJ0aioCACBfQ5qZGT9BACoC2IIqlJKTIX5B0IIpQQAoAixB/x9xQQJ0aiB+OAIAQdCCKUEAKAIsQQAoAtCCKmtB/x9xQQJ0aioCACF/QQAgf7xBgICA/AdxBH0gfwVDAAAAAAs4AtSCKkOamRk/IH6UIYABIIABvEGAgID8B3EEfSCAAQVDAAAAAAshgQFDAAAAAEEAKgIUQQAqAhhBACoC6IIqlEEAKgLgwzNBACoC5MMzkpOUkyGCAUEAIIIBvEGAgID8B3EEfSCCAQVDAAAAAAs4AuSCKiBLQQAqAuDDMyBMQQAqAuSCKpSSlCBNQQAqAvCCKpSSIYMBQQAggwG8QYCAgPwHcQR9IIMBBUMAAAAACzgC7IIqQfSCKkEAKAIsQf//AHFBAnRqQ/MEtT5BACoC7IIqlEMI5TwekjgCACBfQfSCKkEAKAIsQQAoAvSCLmtB//8AcUECdGoqAgCSQ5qZGT9BACoCgIMvlJMhhAFB+IIuQQAoAixB/x9xQQJ0aiCEATgCAEH4gi5BACgCLEEAKAL4gi9rQf8fcUECdGoqAgAhhQFBACCFAbxBgICA/AdxBH0ghQEFQwAAAAALOAL8gi9DmpkZPyCEAZQhhgEghgG8QYCAgPwHcQR9IIYBBUMAAAAACyGHAUMAAAAAQQAqAhRBACoCGEEAKgKQgy+UQQAqArDDM0EAKgK0wzOSk5STIYgBQQAgiAG8QYCAgPwHcQR9IIgBBUMAAAAACzgCjIMvIFRBACoCsMMzIFVBACoCjIMvlJKUIFZBACoCmIMvlJIhiQFBACCJAbxBgICA/AdxBH0giQEFQwAAAAALOAKUgy9BnIMvQQAoAixB//8AcUECdGpD8wS1PkEAKgKUgy+UQwjlPB6SOAIAQZyDL0EAKAIsQQAoApyDM2tB//8AcUECdGoqAgAgX5JDmpkZP0EAKgKowzOUkyGKAUGggzNBACgCLEH/D3FBAnRqIIoBOAIAQaCDM0EAKAIsQQAoAqDDM2tB/w9xQQJ0aioCACGLAUEAIIsBvEGAgID8B3EEfSCLAQVDAAAAAAs4AqTDM0OamRk/IIoBlCGMASCMAbxBgICA/AdxBH0gjAEFQwAAAAALIY0BII0BIIcBkiGOASB7IIEBII4BkpIhjwFBACoCkMEJQQAqArjBEkEAKgLgwRdBACoCiMIgQQAqArCCJUEAKgLYgipBACoCgIMvQQAqAqjDMyBjIGkgbyB1II8BkpKSkpKSkpKSkpKSIZABQQAgkAG8QYCAgPwHcQR9IJABBUMAAAAACzgCrMMzQQAqArCCJUEAKgLYgipBACoCgIMvQQAqAqjDMyCPAZKSkpJBACoCkMEJQQAqArjBEkEAKgLgwRdBACoCiMIgIGMgaSB1IG+SkpKSkpKSkyGRAUEAIJEBvEGAgID8B3EEfSCRAQVDAAAAAAs4ArjDMyCBASB7kiGSAUEAKgLgwRdBACoCiMIgQQAqAoCDL0EAKgKowzMgbyB1II4BkpKSkpKSQQAqApDBCUEAKgK4wRJBACoCsIIlQQAqAtiCKiBjIGkgkgGSkpKSkpKTIZMBQQAgkwG8QYCAgPwHcQR9IJMBBUMAAAAACzgCxMMzQQAqApDBCUEAKgK4wRJBACoCgIMvQQAqAqjDMyBjIGkgjgGSkpKSkpJBACoC4MEXQQAqAojCIEEAKgKwgiVBACoC2IIqIG8gdSCSAZKSkpKSkpMhlAFBACCUAbxBgICA/AdxBH0glAEFQwAAAAALOALQwzMgjQEggQGSIZUBIIcBIHuSIZYBQQAqArjBEkEAKgKIwiBBACoC2IIqQQAqAqjDMyBpIHUglQGSkpKSkpJBACoCkMEJQQAqAuDBF0EAKgKwgiVBACoCgIMvIGMgbyCWAZKSkpKSkpMhlwFBACCXAbxBgICA/AdxBH0glwEFQwAAAAALOALcwzNBACoCkMEJQQAqAuDBF0EAKgLYgipBACoCqMMzIGMgbyCVAZKSkpKSkkEAKgK4wRJBACoCiMIgQQAqArCCJUEAKgKAgy8gaSB1IJYBkpKSkpKSkyGYAUEAIJgBvEGAgID8B3EEfSCYAQVDAAAAAAs4AujDMyCNASB7kiGZASCHASCBAZIhmgFBACoCkMEJQQAqAojCIEEAKgKwgiVBACoCqMMzIGMgdSCZAZKSkpKSkkEAKgK4wRJBACoC4MEXQQAqAtiCKkEAKgKAgy8gaSBvIJoBkpKSkpKSkyGbAUEAIJsBvEGAgID8B3EEfSCbAQVDAAAAAAs4AvTDM0EAKgK4wRJBACoC4MEXQQAqArCCJUEAKgKowzMgaSBvIJkBkpKSkpKSQQAqApDBCUEAKgKIwiBBACoC2IIqQQAqAoCDLyBjIHUgmgGSkpKSkpKTIZwBQQAgnAG8QYCAgPwHcQR9IJwBBUMAAAAACzgCgMQzIBFBACoCwIAIQwAAgD9BACoC1IAIk5QgXpSUIZ0BIAQgBmpDpHC9PkEAKgK4wzNBACoCxMMzkpQgnQGSOAIAIAUgBmognQFDpHC9PkEAKgK4wzNBACoCxMMzk5SSOAIAQQBBACoCHDgCIEEAQQAqAiQ4AihBAEEAKAIsQQFqNgIsQQBBACoCwIAIOALEgAhBAEEAKgLUgAg4AtiACEEAQQAqAuSACDgC6IAIQQBBACoC8IAIOAL0gAhBAEEAKgL8gAg4AoCBCEEAQQAqAozBCTgCkMEJQQBBACoCnMEJOAKgwQlBAEEAKgKkwQk4AqjBCUEAQQAqArTBEjgCuMESQQBBACoCxMESOALIwRJBAEEAKgLMwRI4AtDBEkEAQQAqAtzBFzgC4MEXQQBBACoC7MEXOALwwRdBAEEAKgL0wRc4AvjBF0EAQQAqAoTCIDgCiMIgQQBBACoClMIgOAKYwiBBAEEAKgKcwiA4AqDCIEEAQQAqAqyCJTgCsIIlQQBBACoCvIIlOALAgiVBAEEAKgLEgiU4AsiCJUEAQQAqAtSCKjgC2IIqQQBBACoC5IIqOALogipBAEEAKgLsgio4AvCCKkEAQQAqAvyCLzgCgIMvQQBBACoCjIMvOAKQgy9BAEEAKgKUgy84ApiDL0EAQQAqAqTDMzgCqMMzQQBBACoCsMMzOAK0wzNBAEEAKgKswzM4ArDDM0EAQQAqArzDMzgCwMMzQQBBACoCuMMzOAK8wzNBAEEAKgLIwzM4AszDM0EAQQAqAsTDMzgCyMMzQQBBACoC1MMzOALYwzNBAEEAKgLQwzM4AtTDM0EAQQAqAuDDMzgC5MMzQQBBACoC3MMzOALgwzNBAEEAKgLswzM4AvDDM0EAQQAqAujDMzgC7MMzQQBBACoC+MMzOAL8wzNBAEEAKgL0wzM4AvjDM0EAQQAqAoTEMzgCiMQzQQBBACoCgMQzOAKExDMgBkEEaiEGIAZBBCABbEgEQAwCDAELCwsLhYCAgAAAQQAPC4WAgIAAAEECDwuLgICAAAAgACABaioCAA8LiICAgAAAQQAoAgAPC46AgIAAACAAIAEQBiAAIAEQDwvglYCAAAE2f0EAIQFBACECQQAhA0EAIQRBACEFQQAhBkEAIQdBACEIQQAhCUEAIQpBACELQQAhDEEAIQ1BACEOQQAhD0EAIRBBACERQQAhEkEAIRNBACEUQQAhFUEAIRZBACEXQQAhGEEAIRlBACEaQQAhG0EAIRxBACEdQQAhHkEAIR9BACEgQQAhIUEAISJBACEjQQAhJEEAISVBACEmQQAhJ0EAIShBACEpQQAhKkEAIStBACEsQQAhLUEAIS5BACEvQQAhMEEAITFBACEyQQAhM0EAITRBACE1QQAhNkEAIQEDQAJAQRwgAUECdGpDAAAAADgCACABQQFqIQEgAUECSARADAIMAQsLC0EAIQIDQAJAQSQgAkECdGpDAAAAADgCACACQQFqIQIgAkECSARADAIMAQsLC0EAQQA2AixBACEDA0ACQEEwIANBAnRqQwAAAAA4AgAgA0EBaiEDIANBgIACSARADAIMAQsLC0EAIQQDQAJAQcCACCAEQQJ0akMAAAAAOAIAIARBAWohBCAEQQJIBEAMAgwBCwsLQQAhBQNAAkBB1IAIIAVBAnRqQwAAAAA4AgAgBUEBaiEFIAVBAkgEQAwCDAELCwtBACEGA0ACQEHkgAggBkECdGpDAAAAADgCACAGQQFqIQYgBkECSARADAIMAQsLC0EAIQcDQAJAQfCACCAHQQJ0akMAAAAAOAIAIAdBAWohByAHQQJIBEAMAgwBCwsLQQAhCANAAkBB/IAIIAhBAnRqQwAAAAA4AgAgCEEBaiEIIAhBAkgEQAwCDAELCwtBACEJA0ACQEGEgQggCUECdGpDAAAAADgCACAJQQFqIQkgCUGAIEgEQAwCDAELCwtBACEKA0ACQEGIgQkgCkECdGpDAAAAADgCACAKQQFqIQogCkGAEEgEQAwCDAELCwtBACELA0ACQEGMwQkgC0ECdGpDAAAAADgCACALQQFqIQsgC0ECSARADAIMAQsLC0EAIQwDQAJAQZzBCSAMQQJ0akMAAAAAOAIAIAxBAWohDCAMQQJIBEAMAgwBCwsLQQAhDQNAAkBBpMEJIA1BAnRqQwAAAAA4AgAgDUEBaiENIA1BAkgEQAwCDAELCwtBACEOA0ACQEGswQkgDkECdGpDAAAAADgCACAOQQFqIQ4gDkGAgAJIBEAMAgwBCwsLQQAhDwNAAkBBsMERIA9BAnRqQwAAAAA4AgAgD0EBaiEPIA9BgCBIBEAMAgwBCwsLQQAhEANAAkBBtMESIBBBAnRqQwAAAAA4AgAgEEEBaiEQIBBBAkgEQAwCDAELCwtBACERA0ACQEHEwRIgEUECdGpDAAAAADgCACARQQFqIREgEUECSARADAIMAQsLC0EAIRIDQAJAQczBEiASQQJ0akMAAAAAOAIAIBJBAWohEiASQQJIBEAMAgwBCwsLQQAhEwNAAkBB1MESIBNBAnRqQwAAAAA4AgAgE0EBaiETIBNBgIABSARADAIMAQsLC0EAIRQDQAJAQdjBFiAUQQJ0akMAAAAAOAIAIBRBAWohFCAUQYAgSARADAIMAQsLC0EAIRUDQAJAQdzBFyAVQQJ0akMAAAAAOAIAIBVBAWohFSAVQQJIBEAMAgwBCwsLQQAhFgNAAkBB7MEXIBZBAnRqQwAAAAA4AgAgFkEBaiEWIBZBAkgEQAwCDAELCwtBACEXA0ACQEH0wRcgF0ECdGpDAAAAADgCACAXQQFqIRcgF0ECSARADAIMAQsLC0EAIRgDQAJAQfzBFyAYQQJ0akMAAAAAOAIAIBhBAWohGCAYQYCAAkgEQAwCDAELCwtBACEZA0ACQEGAwh8gGUECdGpDAAAAADgCACAZQQFqIRkgGUGAIEgEQAwCDAELCwtBACEaA0ACQEGEwiAgGkECdGpDAAAAADgCACAaQQFqIRogGkECSARADAIMAQsLC0EAIRsDQAJAQZTCICAbQQJ0akMAAAAAOAIAIBtBAWohGyAbQQJIBEAMAgwBCwsLQQAhHANAAkBBnMIgIBxBAnRqQwAAAAA4AgAgHEEBaiEcIBxBAkgEQAwCDAELCwtBACEdA0ACQEGkwiAgHUECdGpDAAAAADgCACAdQQFqIR0gHUGAgAFIBEAMAgwBCwsLQQAhHgNAAkBBqMIkIB5BAnRqQwAAAAA4AgAgHkEBaiEeIB5BgBBIBEAMAgwBCwsLQQAhHwNAAkBBrIIlIB9BAnRqQwAAAAA4AgAgH0EBaiEfIB9BAkgEQAwCDAELCwtBACEgA0ACQEG8giUgIEECdGpDAAAAADgCACAgQQFqISAgIEECSARADAIMAQsLC0EAISEDQAJAQcSCJSAhQQJ0akMAAAAAOAIAICFBAWohISAhQQJIBEAMAgwBCwsLQQAhIgNAAkBBzIIlICJBAnRqQwAAAAA4AgAgIkEBaiEiICJBgIABSARADAIMAQsLC0EAISMDQAJAQdCCKSAjQQJ0akMAAAAAOAIAICNBAWohIyAjQYAgSARADAIMAQsLC0EAISQDQAJAQdSCKiAkQQJ0akMAAAAAOAIAICRBAWohJCAkQQJIBEAMAgwBCwsLQQAhJQNAAkBB5IIqICVBAnRqQwAAAAA4AgAgJUEBaiElICVBAkgEQAwCDAELCwtBACEmA0ACQEHsgiogJkECdGpDAAAAADgCACAmQQFqISYgJkECSARADAIMAQsLC0EAIScDQAJAQfSCKiAnQQJ0akMAAAAAOAIAICdBAWohJyAnQYCAAUgEQAwCDAELCwtBACEoA0ACQEH4gi4gKEECdGpDAAAAADgCACAoQQFqISggKEGAIEgEQAwCDAELCwtBACEpA0ACQEH8gi8gKUECdGpDAAAAADgCACApQQFqISkgKUECSARADAIMAQsLC0EAISoDQAJAQYyDLyAqQQJ0akMAAAAAOAIAICpBAWohKiAqQQJIBEAMAgwBCwsLQQAhKwNAAkBBlIMvICtBAnRqQwAAAAA4AgAgK0EBaiErICtBAkgEQAwCDAELCwtBACEsA0ACQEGcgy8gLEECdGpDAAAAADgCACAsQQFqISwgLEGAgAFIBEAMAgwBCwsLQQAhLQNAAkBBoIMzIC1BAnRqQwAAAAA4AgAgLUEBaiEtIC1BgBBIBEAMAgwBCwsLQQAhLgNAAkBBpMMzIC5BAnRqQwAAAAA4AgAgLkEBaiEuIC5BAkgEQAwCDAELCwtBACEvA0ACQEGswzMgL0ECdGpDAAAAADgCACAvQQFqIS8gL0EDSARADAIMAQsLC0EAITADQAJAQbjDMyAwQQJ0akMAAAAAOAIAIDBBAWohMCAwQQNIBEAMAgwBCwsLQQAhMQNAAkBBxMMzIDFBAnRqQwAAAAA4AgAgMUEBaiExIDFBA0gEQAwCDAELCwtBACEyA0ACQEHQwzMgMkECdGpDAAAAADgCACAyQQFqITIgMkEDSARADAIMAQsLC0EAITMDQAJAQdzDMyAzQQJ0akMAAAAAOAIAIDNBAWohMyAzQQNIBEAMAgwBCwsLQQAhNANAAkBB6MMzIDRBAnRqQwAAAAA4AgAgNEEBaiE0IDRBA0gEQAwCDAELCwtBACE1A0ACQEH0wzMgNUECdGpDAAAAADgCACA1QQFqITUgNUEDSARADAIMAQsLC0EAITYDQAJAQYDEMyA2QQJ0akMAAAAAOAIAIDZBAWohNiA2QQNIBEAMAgwBCwsLC8qNgIAAARp9QwCAO0hDAACAP0EAKAIAspeWIQJDUkVhPiAClEMAAAA/ko4hA0MAAAAAQ1UM3UAgA5STIAKVIQRDAACAP0NjFB1EIAKVEAWVIQVD1qecPCAClEMAAAA/ko4hBkM4h4M+IAKUQwAAAD+SjiEHQwAAAABDVQzdQCAHlJMgApUhCEN16d88IAKUQwAAAD+SjiEJQxTrRD4gApRDAAAAP5KOIQpDAAAAAENVDN1AIAqUkyAClSELQ67z7zwgApRDAAAAP5KOIQxDN3BXPiAClEMAAAA/ko4hDUMAAAAAQ1UM3UAgDZSTIAKVIQ5DjQ7IPCAClEMAAAA/ko4hD0MAAAA+IAKUQwAAAD+SjiEQQwAAAABDVQzdQCAQlJMgApUhEUPxflw8IAKUQwAAAD+SjiESQ7TnAj4gApRDAAAAP5KOIRNDAAAAAENVDN1AIBOUkyAClSEUQzJzAT0gApRDAAAAP5KOIRVD9+cyPiAClEMAAAA/ko4hFkMAAAAAQ1UM3UAgFpSTIAKVIRdDK6G7PCAClEMAAAA/ko4hGEPZzRw+IAKUQwAAAD+SjiEZQwAAAABDVQzdQCAZlJMgApUhGkOorKY8IAKUQwAAAD+SjiEbQQAgATYCAEMAgDtIQwAAgD9BACgCALKXliECQ1JFYT4gApRDAAAAP5KOIQNDAAAAAENVDN1AIAOUkyAClSEEQQBDAAAAPyAElDgCBEEAQx1DE0cgApUQADgCDEEAQ6uqqj4gBJQ4AhBDAACAP0NjFB1EIAKVEAWVIQVBAEMAAIA/IAVDAACAP5KVOAIUQQBDAACAPyAFkzgCGEPWp5w8IAKUQwAAAD+SjiEGQQBDAACARkMAAAAAIAMgBpOXlqg2ArCACEEAQ2ZmMEIgApU4AsiACEEAQwAAgD9BACoCyIAIkzgC0IAIQQBDAACAPyAClTgC3IAIQQBDAAAAQCAClTgC7IAIQQBDAABAQCAClTgC+IAIQQBDCtejPCAClKg2AoSBCUEAQwAAgERDAAAAACAGQwAAgL+Sl5aoNgKIwQlDOIeDPiAClEMAAAA/ko4hB0MAAAAAQ1UM3UAgB5STIAKVIQhBAEMAAAA/IAiUOAKUwQlBAEOrqqo+IAiUOAKYwQlDdenfPCAClEMAAAA/ko4hCUEAQwAAgEZDAAAAACAHIAmTl5aoNgKswRFBAEMAAABFQwAAAAAgCUMAAIC/kpeWqDYCsMESQxTrRD4gApRDAAAAP5KOIQpDAAAAAENVDN1AIAqUkyAClSELQQBDAAAAPyALlDgCvMESQQBDq6qqPiALlDgCwMESQ67z7zwgApRDAAAAP5KOIQxBAEMAAABGQwAAAAAgCiAMk5eWqDYC1MEWQQBDAAAARUMAAAAAIAxDAACAv5KXlqg2AtjBF0M3cFc+IAKUQwAAAD+SjiENQwAAAABDVQzdQCANlJMgApUhDkEAQwAAAD8gDpQ4AuTBF0EAQ6uqqj4gDpQ4AujBF0ONDsg8IAKUQwAAAD+SjiEPQQBDAACARkMAAAAAIA0gD5OXlqg2AvzBH0EAQwAAAEVDAAAAACAPQwAAgL+Sl5aoNgKAwiBDAAAAPiAClEMAAAA/ko4hEEMAAAAAQ1UM3UAgEJSTIAKVIRFBAEMAAAA/IBGUOAKMwiBBAEOrqqo+IBGUOAKQwiBD8X5cPCAClEMAAAA/ko4hEkEAQwAAAEZDAAAAACAQIBKTl5aoNgKkwiRBAEMAAIBEQwAAAAAgEkMAAIC/kpeWqDYCqIIlQ7TnAj4gApRDAAAAP5KOIRNDAAAAAENVDN1AIBOUkyAClSEUQQBDAAAAPyAUlDgCtIIlQQBDq6qqPiAUlDgCuIIlQzJzAT0gApRDAAAAP5KOIRVBAEMAAABGQwAAAAAgEyAVk5eWqDYCzIIpQQBDAAAARUMAAAAAIBVDAACAv5KXlqg2AtCCKkP35zI+IAKUQwAAAD+SjiEWQwAAAABDVQzdQCAWlJMgApUhF0EAQwAAAD8gF5Q4AtyCKkEAQ6uqqj4gF5Q4AuCCKkMrobs8IAKUQwAAAD+SjiEYQQBDAAAARkMAAAAAIBYgGJOXlqg2AvSCLkEAQwAAAEVDAAAAACAYQwAAgL+Sl5aoNgL4gi9D2c0cPiAClEMAAAA/ko4hGUMAAAAAQ1UM3UAgGZSTIAKVIRpBAEMAAAA/IBqUOAKEgy9BAEOrqqo+IBqUOAKIgy9DqKymPCAClEMAAAA/ko4hG0EAQwAAAEZDAAAAACAZIBuTl5aoNgKcgzNBAEMAAIBEQwAAAAAgG0MAAIC/kpeWqDYCoMMzC5CAgIAAACAAIAEQDiAAEBAgABANC8iAgIAAAEEAQ+xROD84AghBAEMAAAA/OAK0gAhBAEMAAAAAOAK4gAhBAEMAAAA/OAK8gAhBAEO6SQw+OALMgAhBAEMAANxDOALggAgLkICAgAAAIAAgAUgEfyABBSAACw8LkICAgAAAIAAgAUgEfyAABSABCw8LjICAgAAAIAAgAWogAjgCAAsLu5uAgAABAEEAC7QbeyJuYW1lIjogInBpcG9zYyIsImZpbGVuYW1lIjogInBpcG9zYy5kc3AiLCJ2ZXJzaW9uIjogIjIuNDAuMiIsImNvbXBpbGVfb3B0aW9ucyI6ICItbGFuZyB3YXNtLWliIC1jbiBwaXBvc2MgLWVzIDEgLW1jZCAxNiAtc2luZ2xlIC1mdHogMiIsImxpYnJhcnlfbGlzdCI6IFsiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9zdGRmYXVzdC5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L3NpZ25hbHMubGliIiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9tYXRocy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L3BsYXRmb3JtLmxpYiIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QvcmV2ZXJicy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L2RlbGF5cy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L2Jhc2ljcy5saWIiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0L2ZpbHRlcnMubGliIiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdC9yb3V0ZXMubGliIl0sImluY2x1ZGVfcGF0aG5hbWVzIjogWyIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0IiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdCIsIi91c3Ivc2hhcmUvZmF1c3QiLCIuIiwiL2hvbWUvbWlzL3NyYy9fZXh0ZXJucy9zZXJ2ZXVyLWluamVjdGlvbi9zdGF0aWMvcm9vbXMvOUpvZzliRy9mYXVzdCJdLCJzaXplIjogODQ0MzAwLCJpbnB1dHMiOiAwLCJvdXRwdXRzIjogMiwibWV0YSI6IFsgeyAiYmFzaWNzLmxpYi9uYW1lIjogIkZhdXN0IEJhc2ljIEVsZW1lbnQgTGlicmFyeSIgfSx7ICJiYXNpY3MubGliL3ZlcnNpb24iOiAiMC41IiB9LHsgImNvbXBpbGVfb3B0aW9ucyI6ICItbGFuZyB3YXNtLWliIC1jbiBwaXBvc2MgLWVzIDEgLW1jZCAxNiAtc2luZ2xlIC1mdHogMiIgfSx7ICJkZWxheXMubGliL25hbWUiOiAiRmF1c3QgRGVsYXkgTGlicmFyeSIgfSx7ICJkZWxheXMubGliL3ZlcnNpb24iOiAiMC4xIiB9LHsgImZpbGVuYW1lIjogInBpcG9zYy5kc3AiIH0seyAiZmlsdGVycy5saWIvYWxscGFzc19jb21iOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2FsbHBhc3NfY29tYjpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zQGNjcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvYWxscGFzc19jb21iOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzMF9oaWdocGFzczEiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzMF9oaWdocGFzczE6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvbG93cGFzczphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NAY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9uYW1lIjogIkZhdXN0IEZpbHRlcnMgTGlicmFyeSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjE6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvdGYxOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NAY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi90ZjE6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL3RmMXM6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvdGYxczpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zQGNjcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvdGYxczpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvdmVyc2lvbiI6ICIwLjMiIH0seyAibWF0aHMubGliL2F1dGhvciI6ICJHUkFNRSIgfSx7ICJtYXRocy5saWIvY29weXJpZ2h0IjogIkdSQU1FIiB9LHsgIm1hdGhzLmxpYi9saWNlbnNlIjogIkxHUEwgd2l0aCBleGNlcHRpb24iIH0seyAibWF0aHMubGliL25hbWUiOiAiRmF1c3QgTWF0aCBMaWJyYXJ5IiB9LHsgIm1hdGhzLmxpYi92ZXJzaW9uIjogIjIuNSIgfSx7ICJuYW1lIjogInBpcG9zYyIgfSx7ICJwbGF0Zm9ybS5saWIvbmFtZSI6ICJHZW5lcmljIFBsYXRmb3JtIExpYnJhcnkiIH0seyAicGxhdGZvcm0ubGliL3ZlcnNpb24iOiAiMC4yIiB9LHsgInJldmVyYnMubGliL25hbWUiOiAiRmF1c3QgUmV2ZXJiIExpYnJhcnkiIH0seyAicmV2ZXJicy5saWIvdmVyc2lvbiI6ICIwLjIiIH0seyAicm91dGVzLmxpYi9oYWRhbWFyZDphdXRob3IiOiAiUmVteSBNdWxsZXIsIHJldmlzZWQgYnkgUk0iIH0seyAicm91dGVzLmxpYi9uYW1lIjogIkZhdXN0IFNpZ25hbCBSb3V0aW5nIExpYnJhcnkiIH0seyAicm91dGVzLmxpYi92ZXJzaW9uIjogIjAuMiIgfSx7ICJzaWduYWxzLmxpYi9uYW1lIjogIkZhdXN0IFNpZ25hbCBSb3V0aW5nIExpYnJhcnkiIH0seyAic2lnbmFscy5saWIvdmVyc2lvbiI6ICIwLjEiIH1dLCJ1aSI6IFsgeyJ0eXBlIjogInZncm91cCIsImxhYmVsIjogInBpcG9zYyIsIml0ZW1zIjogWyB7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogImZyZXEiLCJhZGRyZXNzIjogIi9waXBvc2MvZnJlcSIsImluZGV4IjogMTMxMTY4LCJtZXRhIjogW3sgInVuaXQiOiAiSHoiIH1dLCJpbml0IjogNDQwLCJtaW4iOiAyMCwibWF4IjogMjAwMDAsInN0ZXAiOiAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogImdhaW4iLCJhZGRyZXNzIjogIi9waXBvc2MvZ2FpbiIsImluZGV4IjogMTMxMTMyLCJpbml0IjogMC41LCJtaW4iOiAwLCJtYXgiOiAxMCwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJidXR0b24iLCJsYWJlbCI6ICJnYXRlIiwiYWRkcmVzcyI6ICIvcGlwb3NjL2dhdGUiLCJpbmRleCI6IDEzMTEyOH0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJyZXZlcmJnYWluIiwiYWRkcmVzcyI6ICIvcGlwb3NjL3JldmVyYmdhaW4iLCJpbmRleCI6IDEzMTE0OCwiaW5pdCI6IDAuMTM3LCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJyb29tc2l6ZSIsImFkZHJlc3MiOiAiL3BpcG9zYy9yb29tc2l6ZSIsImluZGV4IjogOCwiaW5pdCI6IDAuNzIsIm1pbiI6IDAuMDEsIm1heCI6IDIsInN0ZXAiOiAwLjAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogInZvbHVtZSIsImFkZHJlc3MiOiAiL3BpcG9zYy92b2x1bWUiLCJpbmRleCI6IDEzMTEyNCwiaW5pdCI6IDAuNSwibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDF9XX1dfQ=="; }

/*
 faust2wasm: GRAME 2017-2019
*/

'use strict';

if (typeof (AudioWorkletNode) === "undefined") {
    alert("AudioWorklet is not supported in this browser !")
}

class piposcNode extends AudioWorkletNode {

    constructor(context, baseURL, options) {
        super(context, 'piposc', options);

        this.baseURL = baseURL;
        this.json = options.processorOptions.json;
        this.json_object = JSON.parse(this.json);

        // JSON parsing functions
        this.parse_ui = function (ui, obj) {
            for (var i = 0; i < ui.length; i++) {
                this.parse_group(ui[i], obj);
            }
        }

        this.parse_group = function (group, obj) {
            if (group.items) {
                this.parse_items(group.items, obj);
            }
        }

        this.parse_items = function (items, obj) {
            for (var i = 0; i < items.length; i++) {
                this.parse_item(items[i], obj);
            }
        }

        this.parse_item = function (item, obj) {
            if (item.type === "vgroup"
                || item.type === "hgroup"
                || item.type === "tgroup") {
                this.parse_items(item.items, obj);
            } else if (item.type === "hbargraph"
                || item.type === "vbargraph") {
                // Keep bargraph adresses
                obj.outputs_items.push(item.address);
            } else if (item.type === "vslider"
                || item.type === "hslider"
                || item.type === "button"
                || item.type === "checkbox"
                || item.type === "nentry") {
                // Keep inputs adresses
                obj.inputs_items.push(item.address);
                obj.descriptor.push(item);
                // Decode MIDI
                if (item.meta !== undefined) {
                    for (var i = 0; i < item.meta.length; i++) {
                        if (item.meta[i].midi !== undefined) {
                            if (item.meta[i].midi.trim() === "pitchwheel") {
                                obj.fPitchwheelLabel.push({
                                    path: item.address,
                                    min: parseFloat(item.min),
                                    max: parseFloat(item.max)
                                });
                            } else if (item.meta[i].midi.trim().split(" ")[0] === "ctrl") {
                                obj.fCtrlLabel[parseInt(item.meta[i].midi.trim().split(" ")[1])]
                                    .push({
                                        path: item.address,
                                        min: parseFloat(item.min),
                                        max: parseFloat(item.max)
                                    });
                            }
                        }
                    }
                }
                // Define setXXX/getXXX, replacing '/c' with 'C' everywhere in the string
                var set_name = "set" + item.address;
                var get_name = "get" + item.address;
                set_name = set_name.replace(/\/./g, (x) => { return x.substr(1, 1).toUpperCase(); });
                get_name = get_name.replace(/\/./g, (x) => { return x.substr(1, 1).toUpperCase(); });
                obj[set_name] = (val) => { obj.setParamValue(item.address, val); };
                obj[get_name] = () => { return obj.getParamValue(item.address); };
                //console.log(set_name);
                //console.log(get_name);
            }
        }

        this.output_handler = null;

        // input/output items
        this.inputs_items = [];
        this.outputs_items = [];
        this.descriptor = [];

        // MIDI
        this.fPitchwheelLabel = [];
        this.fCtrlLabel = new Array(128);
        for (var i = 0; i < this.fCtrlLabel.length; i++) { this.fCtrlLabel[i] = []; }

        // Parse UI
        this.parse_ui(this.json_object.ui, this);

        // Set message handler
        this.port.onmessage = this.handleMessage.bind(this);
        try {
            if (this.parameters) this.parameters.forEach(p => p.automationRate = "k-rate");
        } catch (e) { }
    }

    // To be called by the message port with messages coming from the processor
    handleMessage(event) {
        var msg = event.data;
        if (this.output_handler) {
            this.output_handler(msg.path, msg.value);
        }
    }

    // Public API

    /**
     * Destroy the node, deallocate resources.
     */
    destroy() {
        this.port.postMessage({ type: "destroy" });
        this.port.close();
    }

    /**
     *  Returns a full JSON description of the DSP.
     */
    getJSON() {
        return this.json;
    }

    // For WAP
    async getMetadata() {
        return new Promise(resolve => {
            let real_url = (this.baseURL === "") ? "main.json" : (this.baseURL + "/main.json");
            fetch(real_url).then(responseJSON => {
                return responseJSON.json();
            }).then(json => {
                resolve(json);
            })
        });
    }

    /**
     *  Set the control value at a given path.
     *
     * @param path - a path to the control
     * @param val - the value to be set
     */
    setParamValue(path, val) {
        // Needed for sample accurate control
        this.parameters.get(path).setValueAtTime(val, 0);
    }

    // For WAP
    setParam(path, val) {
        // Needed for sample accurate control
        this.parameters.get(path).setValueAtTime(val, 0);
    }

    /**
     *  Get the control value at a given path.
     *
     * @return the current control value
     */
    getParamValue(path) {
        return this.parameters.get(path).value;
    }

    // For WAP
    getParam(path) {
        return this.parameters.get(path).value;
    }

    /**
     * Setup a control output handler with a function of type (path, value)
     * to be used on each generated output value. This handler will be called
     * each audio cycle at the end of the 'compute' method.
     *
     * @param handler - a function of type function(path, value)
     */
    setOutputParamHandler(handler) {
        this.output_handler = handler;
    }

    /**
     * Get the current output handler.
     */
    getOutputParamHandler() {
        return this.output_handler;
    }

    getNumInputs() {
        return parseInt(this.json_object.inputs);
    }

    getNumOutputs() {
        return parseInt(this.json_object.outputs);
    }

    // For WAP
    inputChannelCount() {
        return parseInt(this.json_object.inputs);
    }

    outputChannelCount() {
        return parseInt(this.json_object.outputs);
    }

    /**
     * Returns an array of all input paths (to be used with setParamValue/getParamValue)
     */
    getParams() {
        return this.inputs_items;
    }

    // For WAP
    getDescriptor() {
        var desc = {};
        for (const item in this.descriptor) {
            if (this.descriptor.hasOwnProperty(item)) {
                if (this.descriptor[item].label != "bypass") {
                    desc = Object.assign({ [this.descriptor[item].label]: { minValue: this.descriptor[item].min, maxValue: this.descriptor[item].max, defaultValue: this.descriptor[item].init } }, desc);
                }
            }
        }
        return desc;
    }

    /**
     * Control change
     *
     * @param channel - the MIDI channel (0..15, not used for now)
     * @param ctrl - the MIDI controller number (0..127)
     * @param value - the MIDI controller value (0..127)
     */
    ctrlChange(channel, ctrl, value) {
        if (this.fCtrlLabel[ctrl] !== []) {
            for (var i = 0; i < this.fCtrlLabel[ctrl].length; i++) {
                var path = this.fCtrlLabel[ctrl][i].path;
                this.setParamValue(path, piposcNode.remap(value, 0, 127, this.fCtrlLabel[ctrl][i].min, this.fCtrlLabel[ctrl][i].max));
                if (this.output_handler) {
                    this.output_handler(path, this.getParamValue(path));
                }
            }
        }
    }

    /**
     * PitchWeel
     *
     * @param channel - the MIDI channel (0..15, not used for now)
     * @param value - the MIDI controller value (0..16383)
     */
    pitchWheel(channel, wheel) {
        for (var i = 0; i < this.fPitchwheelLabel.length; i++) {
            var pw = this.fPitchwheelLabel[i];
            this.setParamValue(pw.path, piposcNode.remap(wheel, 0, 16383, pw.min, pw.max));
            if (this.output_handler) {
                this.output_handler(pw.path, this.getParamValue(pw.path));
            }
        }
    }

    /**
     * Generic MIDI message handler.
     */
    midiMessage(data) {
        var cmd = data[0] >> 4;
        var channel = data[0] & 0xf;
        var data1 = data[1];
        var data2 = data[2];

        if (channel === 9) {
            return;
        } else if (cmd === 11) {
            this.ctrlChange(channel, data1, data2);
        } else if (cmd === 14) {
            this.pitchWheel(channel, (data2 * 128.0 + data1));
        }
    }

    // For WAP
    onMidi(data) {
        midiMessage(data);
    }

    /**
     * @returns {Object} describes the path for each available param and its current value
     */
    async getState() {
        var params = new Object();
        for (let i = 0; i < this.getParams().length; i++) {
            Object.assign(params, { [this.getParams()[i]]: `${this.getParam(this.getParams()[i])}` });
        }
        return new Promise(resolve => { resolve(params) });
    }

    /**
     * Sets each params with the value indicated in the state object
     * @param {Object} state 
     */
    async setState(state) {
        return new Promise(resolve => {
            for (const param in state) {
                if (state.hasOwnProperty(param)) this.setParam(param, state[param]);
            }
            try {
                this.gui.setAttribute('state', JSON.stringify(state));
            } catch (error) {
                console.warn("Plugin without gui or GUI not defined", error);
            }
            resolve(state);
        })
    }

    /**
     * A different call closer to the preset management
     * @param {Object} patch to assign as a preset to the node
     */
    setPatch(patch) {
        this.setState(this.presets[patch])
    }

    static remap(v, mn0, mx0, mn1, mx1) {
        return (1.0 * (v - mn0) / (mx0 - mn0)) * (mx1 - mn1) + mn1;
    }

}

// Factory class
class piposc {

    static fWorkletProcessors;

    /**
     * Factory constructor.
     *
     * @param context - the audio context
     * @param baseURL - the baseURL of the plugin folder
     */
    constructor(context, baseURL = "") {
        console.log("baseLatency " + context.baseLatency);
        console.log("outputLatency " + context.outputLatency);
        console.log("sampleRate " + context.sampleRate);

        this.context = context;
        this.baseURL = baseURL;
        this.pathTable = [];

        this.fWorkletProcessors = this.fWorkletProcessors || [];
    }

    heap2Str(buf) {
        let str = "";
        let i = 0;
        while (buf[i] !== 0) {
            str += String.fromCharCode(buf[i++]);
        }
        return str;
    }

    /**
     * Load additionnal resources to prepare the custom AudioWorkletNode. Returns a promise to be used with the created node.
     */
    async load() {
        try {
            const importObject = {
                env: {
                    memoryBase: 0,
                    tableBase: 0,
                    _abs: Math.abs,

                    // Float version
                    _acosf: Math.acos,
                    _asinf: Math.asin,
                    _atanf: Math.atan,
                    _atan2f: Math.atan2,
                    _ceilf: Math.ceil,
                    _cosf: Math.cos,
                    _expf: Math.exp,
                    _floorf: Math.floor,
                    _fmodf: (x, y) => x % y,
                    _logf: Math.log,
                    _log10f: Math.log10,
                    _max_f: Math.max,
                    _min_f: Math.min,
                    _remainderf: (x, y) => x - Math.round(x / y) * y,
                    _powf: Math.pow,
                    _roundf: Math.fround,
                    _sinf: Math.sin,
                    _sqrtf: Math.sqrt,
                    _tanf: Math.tan,
                    _acoshf: Math.acosh,
                    _asinhf: Math.asinh,
                    _atanhf: Math.atanh,
                    _coshf: Math.cosh,
                    _sinhf: Math.sinh,
                    _tanhf: Math.tanh,
                    _isnanf: Number.isNaN,
                    _isinff: function (x) { return !isFinite(x); },
                    _copysignf: function (x, y) { return Math.sign(x) === Math.sign(y) ? x : -x; },

                    // Double version
                    _acos: Math.acos,
                    _asin: Math.asin,
                    _atan: Math.atan,
                    _atan2: Math.atan2,
                    _ceil: Math.ceil,
                    _cos: Math.cos,
                    _exp: Math.exp,
                    _floor: Math.floor,
                    _fmod: (x, y) => x % y,
                    _log: Math.log,
                    _log10: Math.log10,
                    _max_: Math.max,
                    _min_: Math.min,
                    _remainder: (x, y) => x - Math.round(x / y) * y,
                    _pow: Math.pow,
                    _round: Math.fround,
                    _sin: Math.sin,
                    _sqrt: Math.sqrt,
                    _tan: Math.tan,
                    _acosh: Math.acosh,
                    _asinh: Math.asinh,
                    _atanh: Math.atanh,
                    _cosh: Math.cosh,
                    _sinh: Math.sinh,
                    _tanh: Math.tanh,
                    _isnan: Number.isNaN,
                    _isinf: function (x) { return !isFinite(x); },
                    _copysign: function (x, y) { return Math.sign(x) === Math.sign(y) ? x : -x; },

                    table: new WebAssembly.Table({ initial: 0, element: "anyfunc" })
                }
            };

            let real_url = (this.baseURL === "") ? "piposc.wasm" : (this.baseURL + "/piposc.wasm");
            const dspFile = await fetch(real_url);
            const dspBuffer = await dspFile.arrayBuffer();
            const dspModule = await WebAssembly.compile(dspBuffer);
            const dspInstance = await WebAssembly.instantiate(dspModule, importObject);

            let HEAPU8 = new Uint8Array(dspInstance.exports.memory.buffer);
            let json = this.heap2Str(HEAPU8);
            let json_object = JSON.parse(json);
            let options = { wasm_module: dspModule, json: json };

            if (this.fWorkletProcessors.indexOf(name) === -1) {
                try {
                    let re = /JSON_STR/g;
                    let piposcProcessorString1 = piposcProcessorString.replace(re, json);
                    let real_url = window.URL.createObjectURL(new Blob([piposcProcessorString1], { type: 'text/javascript' }));
                    await this.context.audioWorklet.addModule(real_url);
                    // Keep the DSP name
                    console.log("Keep the DSP name");
                    this.fWorkletProcessors.push(name);
                } catch (e) {
                    console.error(e);
                    console.error("Faust " + this.name + " cannot be loaded or compiled");
                    return null;
                }
            }
            this.node = new piposcNode(this.context, this.baseURL,
                {
                    numberOfInputs: (parseInt(json_object.inputs) > 0) ? 1 : 0,
                    numberOfOutputs: (parseInt(json_object.outputs) > 0) ? 1 : 0,
                    channelCount: Math.max(1, parseInt(json_object.inputs)),
                    outputChannelCount: [parseInt(json_object.outputs)],
                    channelCountMode: "explicit",
                    channelInterpretation: "speakers",
                    processorOptions: options
                });
            this.node.onprocessorerror = () => { console.log('An error from piposc-processor was detected.'); }
            return (this.node);
        } catch (e) {
            console.error(e);
            console.error("Faust " + this.name + " cannot be loaded or compiled");
            return null;
        }
    }

    async loadGui() {
        return new Promise((resolve, reject) => {
            try {
                // DO THIS ONLY ONCE. If another instance has already been added, do not add the html file again
                let real_url = (this.baseURL === "") ? "main.html" : (this.baseURL + "/main.html");
                if (!this.linkExists(real_url)) {
                    // LINK DOES NOT EXIST, let's add it to the document
                    var link = document.createElement('link');
                    link.rel = 'import';
                    link.href = real_url;
                    document.head.appendChild(link);
                    link.onload = (e) => {
                        // the file has been loaded, instanciate GUI
                        // and get back the HTML elem
                        // HERE WE COULD REMOVE THE HARD CODED NAME
                        var element = createpiposcGUI(this.node);
                        resolve(element);
                    }
                } else {
                    // LINK EXIST, WE AT LEAST CREATED ONE INSTANCE PREVIOUSLY
                    // so we can create another instance
                    var element = createpiposcGUI(this.node);
                    resolve(element);
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });
    };

    linkExists(url) {
        return document.querySelectorAll(`link[href="${url}"]`).length > 0;
    }
}

// Template string for AudioWorkletProcessor

let piposcProcessorString = `

    'use strict';

    // Monophonic Faust DSP
    class piposcProcessor extends AudioWorkletProcessor {
        
        // JSON parsing functions
        static parse_ui(ui, obj, callback)
        {
            for (var i = 0; i < ui.length; i++) {
                piposcProcessor.parse_group(ui[i], obj, callback);
            }
        }
        
        static parse_group(group, obj, callback)
        {
            if (group.items) {
                piposcProcessor.parse_items(group.items, obj, callback);
            }
        }
        
        static parse_items(items, obj, callback)
        {
            for (var i = 0; i < items.length; i++) {
                callback(items[i], obj, callback);
            }
        }
        
        static parse_item1(item, obj, callback)
        {
            if (item.type === "vgroup"
                || item.type === "hgroup"
                || item.type === "tgroup") {
                piposcProcessor.parse_items(item.items, obj, callback);
            } else if (item.type === "hbargraph"
                       || item.type === "vbargraph") {
                // Nothing
            } else if (item.type === "vslider"
                       || item.type === "hslider"
                       || item.type === "button"
                       || item.type === "checkbox"
                       || item.type === "nentry") {
                obj.push({ name: item.address,
                         defaultValue: item.init,
                         minValue: item.min,
                         maxValue: item.max });
            }
        }
        
        static parse_item2(item, obj, callback)
        {
            if (item.type === "vgroup"
                || item.type === "hgroup"
                || item.type === "tgroup") {
                piposcProcessor.parse_items(item.items, obj, callback);
            } else if (item.type === "hbargraph"
                       || item.type === "vbargraph") {
                // Keep bargraph adresses
                obj.outputs_items.push(item.address);
                obj.pathTable[item.address] = parseInt(item.index);
            } else if (item.type === "vslider"
                       || item.type === "hslider"
                       || item.type === "button"
                       || item.type === "checkbox"
                       || item.type === "nentry") {
                // Keep inputs adresses
                obj.inputs_items.push(item.address);
                obj.pathTable[item.address] = parseInt(item.index);
            }
        }
     
        static get parameterDescriptors() 
        {
            // Analyse JSON to generate AudioParam parameters
            var params = [];
            piposcProcessor.parse_ui(JSON.parse(\`JSON_STR\`).ui, params, piposcProcessor.parse_item1);
            return params;
        }
       
        constructor(options)
        {
            super(options);
            this.running = true;
            
            const importObject = {
                    env: {
                        memoryBase: 0,
                        tableBase: 0,

                        // Integer version
                        _abs: Math.abs,

                        // Float version
                        _acosf: Math.acos,
                        _asinf: Math.asin,
                        _atanf: Math.atan,
                        _atan2f: Math.atan2,
                        _ceilf: Math.ceil,
                        _cosf: Math.cos,
                        _expf: Math.exp,
                        _floorf: Math.floor,
                        _fmodf: function(x, y) { return x % y; },
                        _logf: Math.log,
                        _log10f: Math.log10,
                        _max_f: Math.max,
                        _min_f: Math.min,
                        _remainderf: function(x, y) { return x - Math.round(x/y) * y; },
                        _powf: Math.pow,
                        _roundf: Math.fround,
                        _sinf: Math.sin,
                        _sqrtf: Math.sqrt,
                        _tanf: Math.tan,
                        _acoshf: Math.acosh,
                        _asinhf: Math.asinh,
                        _atanhf: Math.atanh,
                        _coshf: Math.cosh,
                        _sinhf: Math.sinh,
                        _tanhf: Math.tanh,

                        // Double version
                        _acos: Math.acos,
                        _asin: Math.asin,
                        _atan: Math.atan,
                        _atan2: Math.atan2,
                        _ceil: Math.ceil,
                        _cos: Math.cos,
                        _exp: Math.exp,
                        _floor: Math.floor,
                        _fmod: function(x, y) { return x % y; },
                        _log: Math.log,
                        _log10: Math.log10,
                        _max_: Math.max,
                        _min_: Math.min,
                        _remainder:function(x, y) { return x - Math.round(x/y) * y; },
                        _pow: Math.pow,
                        _round: Math.fround,
                        _sin: Math.sin,
                        _sqrt: Math.sqrt,
                        _tan: Math.tan,
                        _acosh: Math.acosh,
                        _asinh: Math.asinh,
                        _atanh: Math.atanh,
                        _cosh: Math.cosh,
                        _sinh: Math.sinh,
                        _tanh: Math.tanh,

                        table: new WebAssembly.Table({ initial: 0, element: 'anyfunc' })
                    }
            };
            
            this.piposc_instance = new WebAssembly.Instance(options.processorOptions.wasm_module, importObject);
            this.json_object = JSON.parse(options.processorOptions.json);
         
            this.output_handler = function(path, value) { this.port.postMessage({ path: path, value: value }); };
            
            this.ins = null;
            this.outs = null;

            this.dspInChannnels = [];
            this.dspOutChannnels = [];

            this.numIn = parseInt(this.json_object.inputs);
            this.numOut = parseInt(this.json_object.outputs);

            // Memory allocator
            this.ptr_size = 4;
            this.sample_size = 4;
            this.integer_size = 4;
            
            this.factory = this.piposc_instance.exports;
            this.HEAP = this.piposc_instance.exports.memory.buffer;
            this.HEAP32 = new Int32Array(this.HEAP);
            this.HEAPF32 = new Float32Array(this.HEAP);

            // Warning: keeps a ref on HEAP in Chrome and prevent proper GC
            //console.log(this.HEAP);
            //console.log(this.HEAP32);
            //console.log(this.HEAPF32);

            // bargraph
            this.outputs_timer = 5;
            this.outputs_items = [];

            // input items
            this.inputs_items = [];
        
            // Start of HEAP index

            // DSP is placed first with index 0. Audio buffer start at the end of DSP.
            this.audio_heap_ptr = parseInt(this.json_object.size);

            // Setup pointers offset
            this.audio_heap_ptr_inputs = this.audio_heap_ptr;
            this.audio_heap_ptr_outputs = this.audio_heap_ptr_inputs + (this.numIn * this.ptr_size);

            // Setup buffer offset
            this.audio_heap_inputs = this.audio_heap_ptr_outputs + (this.numOut * this.ptr_size);
            this.audio_heap_outputs = this.audio_heap_inputs + (this.numIn * NUM_FRAMES * this.sample_size);
            
            // Start of DSP memory : DSP is placed first with index 0
            this.dsp = 0;

            this.pathTable = [];
         
            // Send output values to the AudioNode
            this.update_outputs = function ()
            {
                if (this.outputs_items.length > 0 && this.output_handler && this.outputs_timer-- === 0) {
                    this.outputs_timer = 5;
                    for (var i = 0; i < this.outputs_items.length; i++) {
                        this.output_handler(this.outputs_items[i], this.HEAPF32[this.pathTable[this.outputs_items[i]] >> 2]);
                    }
                }
            }
            
            this.initAux = function ()
            {
                var i;
                
                if (this.numIn > 0) {
                    this.ins = this.audio_heap_ptr_inputs;
                    for (i = 0; i < this.numIn; i++) {
                        this.HEAP32[(this.ins >> 2) + i] = this.audio_heap_inputs + ((NUM_FRAMES * this.sample_size) * i);
                    }
                    
                    // Prepare Ins buffer tables
                    var dspInChans = this.HEAP32.subarray(this.ins >> 2, (this.ins + this.numIn * this.ptr_size) >> 2);
                    for (i = 0; i < this.numIn; i++) {
                        this.dspInChannnels[i] = this.HEAPF32.subarray(dspInChans[i] >> 2, (dspInChans[i] + NUM_FRAMES * this.sample_size) >> 2);
                    }
                }
                
                if (this.numOut > 0) {
                    this.outs = this.audio_heap_ptr_outputs;
                    for (i = 0; i < this.numOut; i++) {
                        this.HEAP32[(this.outs >> 2) + i] = this.audio_heap_outputs + ((NUM_FRAMES * this.sample_size) * i);
                    }
                    
                    // Prepare Out buffer tables
                    var dspOutChans = this.HEAP32.subarray(this.outs >> 2, (this.outs + this.numOut * this.ptr_size) >> 2);
                    for (i = 0; i < this.numOut; i++) {
                        this.dspOutChannnels[i] = this.HEAPF32.subarray(dspOutChans[i] >> 2, (dspOutChans[i] + NUM_FRAMES * this.sample_size) >> 2);
                    }
                }
                
                // Parse UI
                piposcProcessor.parse_ui(this.json_object.ui, this, piposcProcessor.parse_item2);
                
                // Init DSP
                this.factory.init(this.dsp, sampleRate); // 'sampleRate' is defined in AudioWorkletGlobalScope  
            }

            this.setParamValue = function (path, val)
            {
                this.HEAPF32[this.pathTable[path] >> 2] = val;
            }

            this.getParamValue = function (path)
            {
                return this.HEAPF32[this.pathTable[path] >> 2];
            }

            // Init resulting DSP
            this.initAux();
        }
        
        process(inputs, outputs, parameters) 
        {
            var input = inputs[0];
            var output = outputs[0];
            
            // Check inputs
            if (this.numIn > 0 && (!input || !input[0] || input[0].length === 0)) {
                //console.log("Process input error");
                return true;
            }
            // Check outputs
            if (this.numOut > 0 && (!output || !output[0] || output[0].length === 0)) {
                //console.log("Process output error");
                return true;
            }
            
            // Copy inputs
            if (input !== undefined) {
                for (var chan = 0; chan < Math.min(this.numIn, input.length); ++chan) {
                    var dspInput = this.dspInChannnels[chan];
                    dspInput.set(input[chan]);
                }
            }
            
            /*
            TODO: sample accurate control change is not yet handled
            When no automation occurs, params[i][1] has a length of 1,
            otherwise params[i][1] has a length of NUM_FRAMES with possible control change each sample
            */
            
            // Update controls
            for (const path in parameters) {
                const paramArray = parameters[path];
                this.setParamValue(path, paramArray[0]);
            }
        
          	// Compute
            try {
                this.factory.compute(this.dsp, NUM_FRAMES, this.ins, this.outs);
            } catch(e) {
                console.log("ERROR in compute (" + e + ")");
            }
            
            // Update bargraph
            this.update_outputs();
            
            // Copy outputs
            if (output !== undefined) {
                for (var chan = 0; chan < Math.min(this.numOut, output.length); ++chan) {
                    var dspOutput = this.dspOutChannnels[chan];
                    output[chan].set(dspOutput);
                }
            }
            
            return this.running;
    	}
        
        handleMessage(event)
        {
            var msg = event.data;
            switch (msg.type) {
                case "destroy": this.running = false; break;
            }
        }
    }

    // Globals
    const NUM_FRAMES = 128;
    try {
        registerProcessor('piposc', piposcProcessor);
    } catch (error) {
        console.warn(error);
    }
`;

SAT.Utils.faust.classes['piposc'] = piposc;
