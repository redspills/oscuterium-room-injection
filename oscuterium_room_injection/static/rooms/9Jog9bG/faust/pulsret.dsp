declare name "pulsret";
declare version "0.1";
declare author "djiamnot";
declare license "GPL v3";

import("stdfaust.lib");
import("limiter.dsp");

gate = button("gate");
attack = hslider("attack", 0.1, 0.0015, 20, 0.001):si.smoo;
decay = hslider("decay", 0.1, 0.0001, 20, 0.001):si.smoo;
release = hslider("release", 0.1, 0.001, 20, 0.0001):si.smoo;
sustain = hslider("sustain", 0.5, 0, 1, 0.001):si.smoo;

freq = hslider("freq", 8, 0.0015, 500, 0.001):si.smoo;
grainfreq = hslider("grainfreq", 200, 1, 5000, 0.001):si.smoo;
duty = hslider("duty", 0.01, 0, 1, 0.0001):si.smoo;
cf = hslider("filter_freq", 100, 18, 10000, 0.001):si.smoo;
q = hslider("Q", 0.1, 0.001, 100, 0.001):si.smoo;
fgain = hslider("filter_gain", 0.3, 0, 1, 0.001):si.smoo;
rev_time = hslider("reverb_time", 2, 0.1, 60, 0.01):si.smoo;
rev_damp = hslider("damp", 0.7, 0, 1, 0.001):si.smoo;
rev_size = hslider("rev_size", 0.7, 0.5, 5, 0.01):si.smoo;
early = hslider("rev_early", 0.2, 0, 1, 0.001):si.smoo;
feedback = hslider("feedback", 0.7, 0, 1, 0.001):si.smoo;
mod_depth = hslider("mod_depth", 0.3, 0, 1, 0.001):si.smoo;
mod_freq = hslider("mod_freq", 2, 0, 10, 0.01):si.smoo;
gain = hslider("gain", 0.5, 0, 5, 0.001):si.smoo;
lfo_freq = hslider("lfo_freq", 10,0.1,20,0.01);
lfo_depth = hslider("lfo_depth",500,1,1000,0.01);

grain = os.osc(grainfreq) * en.ar(0.01, 0.01, pa);
pa = os.lf_pulsetrain(freq, duty);
reson = grain *2.0 : fi.resonlp(cf + os.osc(lfo_freq)*lfo_depth, q, fgain);
envelope = en.adsr(attack, decay, sustain, release, gate);
process = ((reson * gain) * envelope <: re.greyhole(rev_time, rev_damp, rev_size, early, feedback, mod_depth, mod_freq)):limiter(2);
