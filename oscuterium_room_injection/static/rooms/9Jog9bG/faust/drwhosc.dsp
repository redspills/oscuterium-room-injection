// Declaration
declare options "[osc:on]";

// STDs
import("stdfaust.lib");

// Variables
gate = button("gate [1][tooltip:noteOn = 1, noteOff = 0]"):si.smoo;
x0 = hslider("tap_rate",0.15,0,400,0.01) : si.smoo;
y0 = hslider("pitch",0.3,0,1,0.01) : si.smoo;
y1 = hslider("level",0.2,0,1,0.01) : si.smoo;
q = hslider("resonance",18,10,50,0.01) : si.smoo;
del = hslider("delay",0.15,0.01,1,0.01) : si.smoo;
fb = hslider("feedback",0.5,0,1,0.01) : si.smoo;
modFq = hslider("modfrequency",0,0,100,0.01) : si.smoo;
modAmp = hslider("modamp",0,0,5,0.01) : si.smoo;
gain = hslider("gain",0.7,0,1,0.01) : si.smoo;

// Mapping
impFreq = 2 + x0*20;
resFreq = y0*3000+300;

// Echo effect
echo = +~(de.delay(65536,del*ma.SR)*fb);

// DSP
process = ((((os.osc(modFq) * modAmp) + 1 * 0.5) * ((os.lf_imptrain(impFreq) : fi.resonlp(resFreq,q,gain) : echo : ef.cubicnl(y1,0)*0.95))):ma.tanh * gate) <: _,_ : instrReverb3;

instrReverb3 = _,_ <: *(reverbGain),*(reverbGain),*(1 - reverbGain),*(1 - reverbGain) :
re.zita_rev1_stereo(rdel,f1,f2,t60dc,t60m,fsmax),_,_ <: _,!,_,!,!,_,!,_ : +,+
    with {
        reverbGain = hslider("reverbgain",0.137,0,1,0.01) : si.smoo;
        roomSize = hslider("roomsize",0.72,0.01,2,0.01);
        rdel = 20;
        f1 = 200;
        f2 = 6000;
        t60dc = roomSize*3;
        t60m = roomSize*2;
        fsmax = 48000;
    };
