// Simple Organ
import("stdfaust.lib");

// MIDI keyon-keyoff
midigate = button ("gate");    
// MIDI keyon key                        	
midifreq = hslider("freq[unit:Hz]", 440, 20, 20000, 1);	
// MIDI keyon velocity
midigain = hslider("gain", 0.5, 0, 10, 0.01);	       	

// Implementation
phasor(f) = f/ma.SR : (+,1.0:fmod) ~ _ ;
osc(f) = phasor(f) * 6.28318530718 : sin;
timbre(freq) = osc(freq) + 0.5 * osc(2.0*freq) + 0.25 * osc(3.0*freq);
envelop(gate, gain) = gate * gain : smooth(0.9995)
with { 
	smooth(c) = * (1-c) : + ~ * (c); 
};
voice(gate, gain, freq) = envelop(gate, gain) * timbre(freq);

// Main
process = voice(midigate, midigain, midifreq) * hslider("volume", 0.5, 0, 1, 0.01) <: _,_ : instrReverb3;

instrReverb3 = _,_ <: *(reverbGain),*(reverbGain),*(1 - reverbGain),*(1 - reverbGain) :
re.zita_rev1_stereo(rdel,f1,f2,t60dc,t60m,fsmax),_,_ <: _,!,_,!,!,_,!,_ : +,+
    with {
        reverbGain = hslider("reverbgain",0.137,0,1,0.01) : si.smoo;
        roomSize = hslider("roomsize",0.72,0.01,2,0.01);
        rdel = 20;
        f1 = 200;
        f2 = 6000;
        t60dc = roomSize*3;
        t60m = roomSize*2;
        fsmax = 48000;
    };
