declare name "tunedBar";
declare description "Nonlinear Banded Waveguide Models";
declare author "Romain Michon";
declare copyright "Romain Michon (rmichon@ccrma.stanford.edu)";
declare version "1.0";
declare licence "STK-4.3"; // Synthesis Tool Kit 4.3 (MIT style license);
declare description "This instrument uses banded waveguide. For more information, see Essl, G. and Cook, P. Banded Waveguides: Towards Physical Modelling of Bar Percussion Instruments, Proceedings of the 1999 International Computer Music Conference.";
declare options "[midi:on]";
declare options "[osc:on]";

import("instruments.lib");

//==================== GUI SPECIFICATION ================

freq = nentry("freq [1][unit:Hz] [tooltip:Tone frequency]",440,20,20000,1);
gain = nentry("gain [1][tooltip:Gain (value between 0 and 1)]",0.8,0,1,0.01);
gate = button("gate [1][tooltip:noteOn = 1, noteOff = 0]");

select = nentry("excitation_selector
[2][tooltip:0=Bow; 1=Strike]",0,0,1,1);
integrationConstant = hslider("integration_constant
[2][tooltip:A value between 0 and 1]",0,0,1,0.01);
baseGain = hslider("base_gain
[2][tooltip:A value between 0 and 1]",1,0,1,0.01);
bowPressure = hslider("bow_pressure
[2][tooltip:Bow pressure on the instrument (Value between 0 and 1)]",0.2,0,1,0.01);
bowPosition = hslider("bow_position
[2][tooltip:Bow position on the instrument (Value between 0 and 1)]",0,0,1,0.01);

typeModulation = nentry("modulation_type[midi:ctrl 1]
[3][tooltip: 0=theta is modulated by the incoming signal; 1=theta is modulated by the averaged incoming signal;
2=theta is modulated by the squared incoming signal; 3=theta is modulated by a sine wave of frequency freqMod;
4=theta is modulated by a sine wave of frequency freq;",0,0,4,1);
nonLinearity = hslider("nonlinearity[midi:ctrl 2]
[3][tooltip:Nonlinearity factor (value between 0 and 1)]",0,0,1,0.01);
frequencyMod = hslider("modulation_frequency[midi:ctrl 3]
[3][unit:Hz][tooltip:Frequency of the sine wave for the modulation of theta (works if Modulation Type=3)]",220,20,1000,0.1);
nonLinAttack = hslider("nonlinearity_attack
[3][unit:s][Attack duration of the nonlinearity]",0.1,0,2,0.01);

//==================== MODAL PARAMETERS ================

preset = 2;

nMode(2) = 4;

modes(2,0) = 1;
basegains(2,0) = pow(0.999,1);
excitation(2,0) = 1*gain*gate/nMode(2);

modes(2,1) = 4.0198391420;
basegains(2,1) = pow(0.999,2);
excitation(2,1) = 1*gain*gate/nMode(2);

modes(2,2) = 10.7184986595;
basegains(2,2) = pow(0.999,3);
excitation(2,2) = 1*gain*gate/nMode(2);

modes(2,3) = 18.0697050938;
basegains(2,3) = pow(0.999,4);
excitation(2,3) = 1*gain*gate/nMode(2);

//==================== SIGNAL PROCESSING ================

//----------------------- Nonlinear filter ----------------------------
//nonlinearities are created by the nonlinear passive allpass ladder filter declared in miscfilter.lib

//nonlinear filter order
nlfOrder = 6;

//nonLinearModultor is declared in instruments.lib, it adapts allpassnn from miscfilter.lib
//for using it with waveguide instruments
NLFM =  nonLinearModulator((nonLinearity : si.smoo),1,freq,
typeModulation,(frequencyMod : si.smoo),nlfOrder);

//----------------------- Synthesis parameters computing and functions declaration ----------------------------

//the number of modes depends on the preset being used
nModes = nMode(preset);

//bow table parameters
tableOffset = 0;
tableSlope = 10 - (9*bowPressure);

delayLengthBase = ma.SR/freq;

//delay lengths in number of samples
delayLength(x) = delayLengthBase/modes(preset,x);

//delay lines
delayLine(x) = de.delay(4096,delayLength(x));

//Filter bank: bandpass filters (declared in instruments.lib)
radius = 1 - ma.PI*32/ma.SR;
bandPassFilter(x) = bandPass(freq*modes(preset,x),radius);

//Delay lines feedback for bow table lookup control
baseGainApp = 0.8999999999999999 + (0.1*baseGain);
velocityInputApp = integrationConstant;
velocityInput = velocityInputApp + _*baseGainApp,par(i,(nModes-1),(_*baseGainApp)) :> +;

//Bow velocity is controlled by an ADSR envelope
maxVelocity = 0.03 + 0.1*gain;
bowVelocity = maxVelocity*en.adsr(0.02,0.005,0.9,0.01,gate);

//stereoizer is declared in instruments.lib and implement a stereo spacialisation in function of
//the frequency period in number of samples
stereo = stereoizer2(delayLengthBase);

//----------------------- Algorithm implementation ----------------------------

//Bow table lookup (bow is decalred in instruments.lib)
bowing = bowVelocity - velocityInput <: *(bow(tableOffset,tableSlope)) : /(nModes);

//One resonance
resonance(x) = + : + (excitation(preset,x)*select) : delayLine(x) : *(basegains(preset,x)) : bandPassFilter(x);

process =
		//Bowed Excitation
		(bowing*((select-1)*-1) <:
		//nModes resonances with nModes feedbacks for bow table look-up
		par(i,nModes,(resonance(i)~_)))~par(i,nModes,_) :> + :
		//Signal Scaling and stereo
		*(4) : NLFM : stereo : instrReverb4;

instrReverb4 = _,_ <: *(reverbGain),*(reverbGain),*(1 - reverbGain),*(1 - reverbGain) :
re.zita_rev1_stereo(rdel,f1,f2,t60dc,t60m,fsmax),_,_ <: _,!,_,!,!,_,!,_ : +,+
    with {
        reverbGain = hslider("reverbgain",0.137,0,1,0.01) : si.smoo;
        roomSize = hslider("roomsize",0.72,0.01,2,0.01);
        rdel = 20;
        f1 = 200;
        f2 = 6000;
        t60dc = roomSize*3;
        t60m = roomSize*2;
        fsmax = 48000;
    };

stereoizer2(periodDuration) = _ <: _,widthdelay : stereopanner
    with {
        W = hslider("spatial_width", 0.5, 0, 1, 0.01);
        A = hslider("pan_angle", 0.6, 0, 1, 0.01);
        widthdelay = de.delay(4096,W*periodDuration/2);
        stereopanner = _,_ : *(1.0-A), *(A);
    };
