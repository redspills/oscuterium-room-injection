window.APP_CONFIG.features.enable_lobby_ghosts = true;

if (SAT.Utils.mainPane) {
    SAT.Utils.mainPane.hidden = true;
}

SAT.rs = {};

document.addEventListener('keydown', function (event) {
    if (SAT.Utils.mainPane && SAT.Utils.isSatAdmin) {
        if (event.code === "Digit0" || event.key === "0" || event.keyCode === 48) {
            if (event.ctrlKey) {
                if (SAT.Utils.mainPane.hidden) {
                    SAT.Utils.mainPane.hidden = false;
                } else {
                    SAT.Utils.mainPane.hidden = true;
                }
            }
        }
    }
});


function setUVscrollSpeed(entity, x, y) {
    if (entity) {
        entity.setAttribute("uv-scroll", `speed: ${x} ${y};`);
    }

};

function setUVscrollIncrement(entity, x, y) {
    if (entity) {
        entity.setAttribute("uv-scroll", `increment: ${x} ${y};`);
    }
};

document.querySelector('body').addEventListener("entered", () => {
    setTimeout(() => {
        let p1 = document.querySelector("a-entity[class='planey1']");
        let p2 = document.querySelector("a-entity[class='planey2']");
        let p3 = document.querySelector("a-entity[class='planey3']");
        // set up some variables needed by the synth function
        // let wavesin_panner = null;
        // let organ_panner = null;
        // let modalbar_panner = null;
        // let saxophony_panner = null;
        // let drwhosc_panner = null;

        console.log("****************** calling synths **************************");
        startorgan();
        startmodalbar();
        // starttunedBar();
        startsaxophony();
        startdrwhosc();
        startwavesin();
        // startpulsret();
        // startkick();
        // startpiposc();
        // startgraindosc();

        console.log("****************** done synths *****************************");
        let p1x = 0.001;
        let p1y = 0.001;
        let p2x = 0.001;
        let p2y = 0.001;
        let p3x = 0.001;
        let p3y = 0.001;
        setUVscrollIncrement(p1, 0.0001, 0.0001);
        setUVscrollIncrement(p2, 0.0001, 0.0001);
        setUVscrollIncrement(p3, 0.0001, 0.0001);

        SAT.Utils.io.room.on('babyHands', (data) => {
            if (data.payload.osc.user == "djiamnot") {
                if (data.payload.osc.address.split("/")[1] == "scroll") {
                    setUVscrollSpeed(p1, data.payload.osc.tag[0]*0.001, data.payload.osc.tag[1]*0.001);
                }
                // if (data.payload.osc.address.split("/")[1] == "drwhosc") {
                //     handleDrwhosc(data.payload.osc.address, data.payload.osc.tag);
                // };
            };
            if (data.payload.osc.address.split("/")[1] == "gridblocks3") {
                if (data.payload.osc.address.split("/")[2] == "move") {
                    if (data.payload.osc.tag.length == 3){
                        SAT.Utils.gridblocks3.move(`${data.payload.osc.tag[0]} ${data.payload.osc.tag[1]} ${data.payload.osc.tag[2]}`)
                    }
                }
                if (data.payload.osc.address.split("/")[2] == "rotate") {
                    if (data.payload.osc.tag.length == 3){
                        SAT.Utils.gridblocks3.rotate(`${data.payload.osc.tag[0]} ${data.payload.osc.tag[1]} ${data.payload.osc.tag[2]}`)
                    }
                }
            }

            if (data.payload.osc.address.split("/")[1] == "drwhosc") {
                handleDrwhosc(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "bm-kick") {
                handleKick(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "graindosc") {
                handleModalbar(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "piposc") {
                handleModalbar(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "tunedBar") {
                handleModalbar(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "modalbar") {
                handleModalbar(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "organ") {
                handleOrgan(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "pipeosc") {
                handleOrgan(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "saxophony") {
                handlesaxophony(data.payload.osc.address, data.payload.osc.tag);
            };
            if (data.payload.osc.address.split("/")[1] == "wavesin") {
                handleWavesin(data.payload.osc.address, data.payload.osc.tag);
            };

            if (data.payload.osc.user == "dirk") {
                if (data.payload.osc.address.split("/")[1] == "scroll") {
                    setUVscrollSpeed(p3, data.payload.osc.tag[0]*0.001, data.payload.osc.tag[1]*0.001);
                }
            }

            if (data.payload.osc.user == "dndrew") {
                if (data.payload.osc.address.split("/")[1] == "scroll") {
                    setUVscrollSpeed(p2, data.payload.osc.tag[0]*0.001, data.payload.osc.tag[1]*0.001);
                }
            }
        });

        function handleWavesin(address, data) {
            if (SAT.Utils.faust.dsps.wavesin) {
                if (SAT.Utils.faust.dsps.wavesin.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.wavesin.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.wavesin_panner.pan.value = pan;
                }
            }
        };

        function handleDrwhosc(address, data) {
            if (SAT.Utils.faust.dsps.drwhosc) {
                if (SAT.Utils.faust.dsps.drwhosc.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.drwhosc.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.drwhosc_panner.pan.value = pan;
                }
            }
        };

        function handleGrainosc(address, data) {
            if (SAT.Utils.faust.dsps.drwhosc) {
                if (SAT.Utils.faust.dsps.grainosc.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.grainosc.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.grainosc_panner.pan.value = pan;
                }
            }
        };

        function handleModalbar(address, data) {
            if (SAT.Utils.faust.dsps.modalbar) {
                if (SAT.Utils.faust.dsps.modalbar.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.modalbar.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.modalbar_panner.pan.value = pan;
                }
            }
        }

        function handleKick(address, data) {
            if (SAT.Utils.faust.dsps.modalbar) {
                if (SAT.Utils.faust.dsps.kick.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.kick.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.kick_panner.pan.value = pan;
                }
            }
        }


        function handleTunedBar(address, data) {
            if (SAT.Utils.faust.dsps.modalbar) {
                if (SAT.Utils.faust.dsps.tunedBar.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.tunedBar.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.tunedBar_panner.pan.value = pan;
                }
            }
        }

        function handlesaxophony(address, data) {
            if (SAT.Utils.faust.dsps.saxophony) {
                if (SAT.Utils.faust.dsps.saxophony.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.saxophony.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.saxophony_panner.pan.value = pan;
                }
            }
        };

        function handleOrgan(address, data) {
            if (SAT.Utils.faust.dsps.organ) {
                if (SAT.Utils.faust.dsps.organ.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.organ.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.organ_panner.pan.value = pan;
                }
            }
        };

        function handlePiposc(address, data) {
            if (SAT.Utils.faust.dsps.organ) {
                if (SAT.Utils.faust.dsps.piposc.getParams().includes(address)) {
                    SAT.Utils.faust.dsps.piposc.setParam(address, data[0]);
                }
                if (address.split("/")[2] == "pan") {
                    let pan = data[0];
                    SAT.Utils.faust.piposc_panner.pan.value = pan;
                }
            }
        };


        SAT.Utils.circlesGLB = new SAT.Utils.NetworkedGLBComponent("circlesglb", true, { position: { x: 2, y: 0, z: -14 }, rotation: { x: 90, y: 0, z: 0 } });
        SAT.Utils.gridblocks3 = new SAT.Utils.NetworkedGLBComponent("gridblocks3", true, { position: { x: 2, y: 0, z: -14 }, rotation: { x: 90, y: 0, z: 0 } });

        if (SAT.Utils.mainPane && SAT.Utils.isSatAdmin) {
            // do main redspills folder
            SAT.rs.folder = SAT.Utils.mainPane.addFolder({ "title": "REDSPILLS", "expanded": false });
            SAT.rs.dsps = {};
            // Do DSPS folders
            // First, organ folder
            //
            const organValue2 = { value2: 0 };
            const organValue3 = { value3: 0 };

            SAT.rs.dsps.organ = { "folder": null, "value1": null, "value2": null, "value3": null, "keyOnBtn": null, "keyOffBtn": null };
            let _organ = SAT.rs.dsps.organ;

            // _organ.folder = SAT.rs.folder.addFolder({ "title": "Organ", "expanded": false });
            //_organ.value2 = _organ.folder.addInput(organValue2, 'value2', { min: 0, max: 127, step: 1, label: "note" });
            //_organ.value3 = _organ.folder.addInput(organValue3, 'value3', { min: 0, max: 127, step: 1, label: "amplitude" });
            //_organ.keyOnBtn = _organ.folder.addButton({ title: 'KeyOn', label: '' });
            //_organ.keyOffBtn = _organ.folder.addButton({ title: 'KeyOff', label: '' });

            /*
            _organ.keyOnBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "organ", dspCommand: "keyOn", dspData: [0, organValue2.value2, organValue3.value3] } });
            })

            _organ.keyOffBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "organ", dspCommand: "keyOff", dspData: [0, organValue2.value2, organValue3.value3] } });
            })
            */

            // modal bar

            const modalBarValue2 = { value2: 0 };
            const modalBarValue3 = { value3: 0 };

            SAT.rs.dsps.modalbar = { "folder": null, "value1": null, "value2": null, "value3": null, "keyOnBtn": null, "keyOffBtn": null };
            let _mb = SAT.rs.dsps.modalbar;

            // _mb.folder = SAT.rs.folder.addFolder({ "title": "Modalbar", "expanded": false });
            //_mb.value2 = _mb.folder.addInput(modalBarValue2, 'value2', { min: 0, max: 127, step: 1, label: "midi note" });
            //_mb.value3 = _mb.folder.addInput(modalBarValue3, 'value3', { min: 0, max: 127, step: 1, label: "amplitude" });
            // _mb.keyOnBtn = _mb.folder.addButton({ title: 'KeyOn', label: '' });
            // _mb.keyOffBtn = _mb.folder.addButton({ title: 'KeyOff', label: '' });

            /*
            _mb.keyOnBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "modalbar", dspCommand: "keyOn", dspData: [0, modalBarValue2.value2, modalBarValue3.value3] } });
            })

            _mb.keyOffBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "modalbar", dspCommand: "keyOff", dspData: [0, modalBarValue2.value2, modalBarValue3.value3] } });
            })
            */

            // feedback
            const feedbackValue2 = { value2: 0 };
            const feedbackValue3 = { value3: 0 };

            SAT.rs.dsps.feedback = { "folder": null, "value1": null, "value2": null, "value3": null, "keyOnBtn": null, "keyOffBtn": null };
            let _feed = SAT.rs.dsps.feedback;

            // _feed.folder = SAT.rs.folder.addFolder({ "title": "saxophony", "expanded": false });
            // _feed.value2 = _feed.folder.addInput(feedbackValue2, 'value2', { min: 0, max: 127, step: 1, label: "note" });
            // _feed.value3 = _feed.folder.addInput(feedbackValue3, 'value3', { min: 0, max: 127, step: 1, label: "amplitude" });
            // _feed.keyOnBtn = _feed.folder.addButton({ title: 'KeyOn', label: '' });
            // _feed.keyOffBtn = _feed.folder.addButton({ title: 'KeyOff', label: '' });

            /*
            _feed.keyOnBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "saxophony", dspCommand: "keyOn", dspData: [0, feedbackValue2.value2, feedbackValue3.value3] } });
            })

            _feed.keyOffBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "saxophony", dspCommand: "keyOff", dspData: [0, feedbackValue2.value2, feedbackValue3.value3] } });
            })
            */

            // drwhosc



            const drwhoscValue0 = { value0: 0 };
            const drwhoscValue1 = { value1: 0 };
            const drwhoscValue2 = { value2: 0 };
            const drwhoscValue3 = { value3: 0 };
            const drwhoscValue4 = { value4: 0 };
            const drwhoscValue5 = { value5: 0 };
            const drwhoscValue6 = { value6: 0 };
            const drwhoscValue7 = { value7: 0 };
            const drwhoscValue8 = { value8: 0 };
            const drwhoscValue9 = { value9: 0 };
            const drwhoscValue10 = { value10: 0 };
            const drwhoscValue11 = { value11: 0 };

            SAT.rs.dsps.drwhosc = {
                "folder": null,
                "value1": null,
                "value2": null,
                "value3": null,
                "value4": null,
                "value5": null,
                "value6": null,
                "value7": null,
                "value8": null,
                "value9": null,
                "value10": null,
                "value11": null,
                "keyOnBtn": null,
                "keyOffBtn": null
            };
            let _drwhosc = SAT.rs.dsps.drwhosc;

           // _drwhosc.folder = SAT.rs.folder.addFolder({ "title": "Drwhosc", "expanded": false });
           // _drwhosc.value10 = _drwhosc.folder.addInput(drwhoscValue10, 'value10', { min: 0, max: 127, step: 1, label: "note" });
           // _drwhosc.value11 = _drwhosc.folder.addInput(drwhoscValue11, 'value11', { min: 0, max: 127, step: 1, label: "amplitude" });
           // _drwhosc.keyOnBtn = _drwhosc.folder.addButton({ title: 'KeyOn', label: '' });
           //  _drwhosc.keyOffBtn = _drwhosc.folder.addButton({ title: 'KeyOff', label: '' });

        /*
            _drwhosc.keyOnBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "keyOn", dspData: [0, drwhoscValue10.value2, drwhoscValue11.value3] } });
            })

            _drwhosc.keyOffBtn.on("click", () => {
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "keyOff", dspData: [0, drwhoscValue10.value2, drwhoscValue11.value3] } });
            })

            _drwhosc.value1 = _drwhosc.folder.addInput(drwhoscValue1, 'value1', { min: 0, max: 1, step: 0.001, label: "saxophony" });
            _drwhosc.value1.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "saxophony", dspData: [drwhoscValue1.value1] } });
            })

            _drwhosc.value2 = _drwhosc.folder.addInput(drwhoscValue2, 'value2', { min: 0, max: 1, step: 0.001, label: "Level" });
            _drwhosc.value2.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "Level", dspData: [drwhoscValue2.value2] } });
            })

            _drwhosc.value3 = _drwhosc.folder.addInput(drwhoscValue3, 'value3', { min: 0, max: 1, step: 0.001, label: "ModAmpl" });
            _drwhosc.value3.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "ModAmpl", dspData: [drwhoscValue3.value3] } });
            })

            _drwhosc.value4 = _drwhosc.folder.addInput(drwhoscValue4, 'value4', { min: 0, max: 1, step: 0.001, label: "ModFreq" });
            _drwhosc.value4.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "ModFreq", dspData: [drwhoscValue4.value4] } });
            })

            _drwhosc.value5 = _drwhosc.folder.addInput(drwhoscValue5, 'value5', { min: 0, max: 1, step: 0.001, label: "Pitch" });
            _drwhosc.value5.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "Pitch", dspData: [drwhoscValue5.value5] } });
            })

            _drwhosc.value6 = _drwhosc.folder.addInput(drwhoscValue6, 'value6', { min: 0, max: 1, step: 0.001, label: "Resonance" });
            _drwhosc.value6.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "Resonance", dspData: [drwhoscValue6.value6] } });
            })

            _drwhosc.value7 = _drwhosc.folder.addInput(drwhoscValue7, 'value7', { min: 0, max: 1, step: 0.001, label: "Tap rate" });
            _drwhosc.value7.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "TapRate", dspData: [drwhoscValue7.value7] } });
            })

            _drwhosc.value8 = _drwhosc.folder.addInput(drwhoscValue8, 'value8', { min: 0, max: 1, step: 0.001, label: "Z/Dry/WetM" });
            _drwhosc.value8.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "Z/Dry/WetM", dspData: [drwhoscValue8.value8] } });
            })

            _drwhosc.value9 = _drwhosc.folder.addInput(drwhoscValue9, 'value9', { min: 0, max: 1, step: 0.001, label: "Zita/Level" });
            _drwhosc.value9.on('change', (evt) => {
                console.log(evt.value);
                SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "drwhosc", dspCommand: "Zita/Level", dspData: [drwhoscValue9.value9] } });
            })
            */
        }
    }, 100);
});

/*
document.addEventListener('keydown', (event) => {
    if (event.code === "Digit3" || event.key === "3") {
        SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "organ", dspCommand: "keyOn", dspData: [0, 60, 60] } });
    }

});

document.addEventListener('keydown', (event) => {
    if (event.code === "Digit4" || event.key === "4") {
        SAT.Utils.io.room.emit({ msgType: "faustMsg", msg: { msgType: "faustMsg", dsp: "organ", dspCommand: "keyOff", dspData: [0, 60, 60] } });
    }
});
*/

class NetworkedGLBComponent {
    constructor(id, autoSave = true, orinalAttr = { position: { x: 0, y: 0, z: 0 }, rotation: { x: 0, y: 0, z: 0 } }) {
        if (id) {
            this.componentID = id;
            this.glbElement = document.querySelector(`a-entity[class*='${id}']`);
            this.autoSave = autoSave;
            this.originalAttributes = orinalAttr;
            if (this.glbElement) {

                SAT.Utils.io.room.on(`${this.componentID}Position`, (data) => {
                    console.log(`${this.componentID}Position`, data);
                    this.glbElement.setAttribute("position", data.position);
                });

                SAT.Utils.io.room.on(`${this.componentID}Rotation`, (data) => {
                    console.log(`${this.componentID}Rosition`, data);
                    this.glbElement.setAttribute("rotation", data.rotation);
                });
            } else {
                console.warn(`NO glb element found with query ${this.componentID}`);
            }

        } else {
            console.warn("Components without ids are a NO NO");
        }


        const fetchLatestValues = async () => {
            const attributes = await SAT.Utils.store.find("component", this.componentID);

            if (attributes && attributes.configs) {
                if (attributes.configs.position) {
                    this.glbElement.setAttribute("position", attributes.configs.position);
                }
                if (attributes.configs.rotation) {
                    this.glbElement.setAttribute("rotation", attributes.configs.rotation);
                }
            }
        }
        if (this.glbElement) {
            fetchLatestValues();
        }

    }

    saveAttributes() {
        let saveAttributes = async () => {

            const pos = this.glbElement.getAttribute("position");
            const rot = this.glbElement.getAttribute("rotation");
            const attributes = {
                "position": `${pos.x} ${pos.y} ${pos.z}`,
                "rotation": `${rot.x} ${rot.y} ${rot.z}`
            }

            await SAT.Utils.store.save("component", this.componentID, attributes);
        }
        if (this.glbElement) {
            saveAttributes();
        }
    }

    move(position) {
        /* position could be :
           this.glbElement.setAttribute("position", "x", 12);
           this.glbElement.setAttribute("position", "12, 12, 12"); x_y_z format
           this.glbElement.setAttribute("position", {"x":12, "y":12, "z":12); {x: y: z:} format
        */
        if (this.glbElement) {
            this.glbElement.setAttribute("position", position);
            if (this.autoSave) { this.saveAttributes(); }
            SAT.Utils.io.room.emit({ msgType: `${this.componentID}Position`, msg: { msgType: `${this.componentID}Position`, "position": position } });
        }
    }

    rotate(rotation) {
        if (this.glbElement) {
            this.glbElement.setAttribute("rotation", rotation);
            if (this.autoSave) { this.saveAttributes(); }
            SAT.Utils.io.room.emit({ msgType: `${this.componentID}Rotation`, msg: { msgType: `${this.componentID}Rotation`, "rotation": rotation } });
        }
    }

    resetAttributes() {
        if (this.glbElement) {
            this.move(this.originalAttributes.position);
            this.rotate(this.originalAttributes.rotation);
        }
    }
}

SAT.Utils.NetworkedGLBComponent = NetworkedGLBComponent;
