function AllhandsSocket() {
  if ("WebSocket" in window) {
    var ws = new WebSocket("ws://localhost:9090");
    ws.addEventListener('open', function(event) {ws.send("hello server")});
    console.log("********************** websocket waiting for allhands ************")
    ws.addEventListener('message', function(evt) {
      var rcvd_msg = evt.data;
      console.log("ws message: " + rcvd_msg);
    })
    ws.onmessage = function (event) {
      const oscObj = JSON.parse(event.data);
      console.log("address pattern: " + oscObj.addressPattern);
      if (oscObj.addressPattern == "/djiamnot/says") {
        console.log("djiamnot says: " + rcvd_msg);
      }

    }
  } else {
    console.log("-=-=-=-=-=-=-=-=-=-=-=-=- No websocket availabel in " + window);
  }
}
setTimeout(()=> {
  AllhandsSocket();
  console.log(`******** test websocket should be running ****** from hubId: ${SAT.Utils.getHubID()}`);
}, 500)
