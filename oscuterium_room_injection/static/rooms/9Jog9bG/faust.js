


var isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
// var audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
var Feedback_dsp = null;
var ModalBar_dsp = null;
var organ_dsp = null;

const organjs = document.createElement('script');
organjs.src = "/rooms/rdN3prg/organ.js";
Document.head.append(organjs);

function startOrgan() {
  console.log("************************* ORGAN STARTING in room");
  console.log(organjs.src);
  var audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
  // Create the Faust generated node
  var pluginURL = "/rooms/rdN3prg";
  var plugin = new FaustorganPoly(audio_context, 16, pluginURL);
  plugin.load().then(node => {
    organ_dsp = node;
    console.log(organ_dsp.getJSON());
    // Print paths to be used with 'setParamValue'
    console.log(organ_dsp.getParams());
    // Connect it to output as a regular WebAudio node
    organ_dsp.connect(audio_context.destination);
  });
}


setTimeout(()=> {
  startOrgan();
  console.log(`******** faust should be running ****** from hubId: ${SAT.Utils.getHubID()}`);
}, 15000);
