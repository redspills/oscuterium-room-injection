
SAT.Utils.DynamicCustomEntry = {
    buttonText:{fr:"Rejoindre la Salle", en:"Join Room"},
    vrButtonText:{fr:"Rejoindre en VR", en:"Enter on device"},
    spectateButtonText:{fr:"Observer", en:"Spectate"},
    titleText:{fr:"OSCUTERIUM", en:"OSCUTERIUM"},
    subtitleText:{fr:"PAR MICHAL SETA, ANDREW STEWART & DIRK STROMBERG", en:"BY MICHAL SETA, ANDREW STEWART & DIRK STROMBERG"},
    descText:{
        fr:"Oscuterium est le dernier projet Web-XR de RedSpills. Le terme “Oscuterium” est un néologisme qui combine auditorium et l’acronyme OSC (Open Sound Control), un protocole réseau pour la transmission de sons.<br><br>RedSpills performe sur un ensemble de trois instruments musicaux et digitaux (connus sous DMIs), envoyant ainsi des données OSC à distance dans une session HUBS équipée de synthétiseurs VR. Jouant télémétriquement ces synthétiseurs, les valeurs OSC transforment également l’environnement HUBS; l’ensemble de mouvements tangibles requis pour maœuvrer les instruments sont transduits en signaux de données qui, à leur tour, deviennent gestes sonores et transformation de l’environnement VR, réifiant ainsi la gestuelle corporelle des performeurs. ", 
        en:"Oscuterium is RedSpills' latest Web XR project. Oscuterium is a neologism the combines auditorium and a reference to the Open Sound Control (OSC) networking protocol.<br><br>RedSpills (Seta, Stewart, and Stromberg) perform on three Digital Musical Instruments (DMIs), sending OSC control data remotely into a HUBS scene that is equipped with embedded VR synthesizers.<br><br>In Oscuterium, RedSpills plays these VR synthesizers, while using the OSC data from their instruments to simultaneously transform the HUBS scene; the tangible physical gestures required to maneouvre and operate the DMIs are \"transduced\" into data signals that produce high-quality sound and manipulate VR objects."
    },
    scriptsLoaded:false,
    stylesLoaded:false,
    doEntry:()=> {
        if (!SAT.Utils.DynamicCustomEntry.scriptsLoaded) {
            setTimeout(() => {
                const options = SAT.Utils.DynamicCustomEntry.options;
                if (document.getElementById("dynamicCustomEntry")) {

                    if (options.showJoinRoom) {
                        let button = document.getElementById("joinRoomButton");
                        if (button) {
                            button.style.opacity = "0";
                            button.addEventListener("click", ()=> { 
                                SAT.Utils.DynamicCustomEntry.scriptsLoaded = false; 
                                options.onJoinRoom(); 
                            }, {once : true});
                        } else {
                            SAT.Utils.DynamicCustomEntry.doEntry();
                        }
                    }

                    if (options.showEnterOnDevice) {
                        let vrButton = document.getElementById("enterOnDeviceButton");
                        if (vrButton) {
                            vrButton.style.opacity = "0";
                            
                            vrButton.addEventListener("click", ()=> {
                                SAT.Utils.DynamicCustomEntry.scriptsLoaded = false; 
                                options.onEnterOnDevice(); 
                            }, {once : true});
                        }
                    }

                    if (options.showSpectate) {
                        let spectateButton = document.getElementById("enterSpectateButton");
                        if (spectateButton) {
                            spectateButton.style.opacity = "0";
                            
                            spectateButton.addEventListener("click", ()=> {
                                SAT.Utils.DynamicCustomEntry.scriptsLoaded = false; 
                                options.onSpectate(); 
                            }, {once : true});
                        }
                    }
                        
                    console.log("set opacities to 0");
                    document.getElementById("topTitle").style.opacity = "0";
                    document.getElementById("title").style.opacity = "0";
                    document.getElementById("subtitle").style.opacity = "0";
                    document.getElementById("bodyText").style.opacity = "0";
                    document.getElementById("smallLogo").style.opacity = "0";
                    SAT.Utils.DynamicCustomEntry.fadeInElements();
                    SAT.Utils.DynamicCustomEntry.scriptsLoaded = true;
                } else {
                    SAT.Utils.DynamicCustomEntry.doEntry();
                }
            }, 5);
        }
    },
    options:{},
    renderHTML:(options)=> {
        SAT.Utils.DynamicCustomEntry.options = options;
        SAT.Utils.DynamicCustomEntry.options.showSpectate = true;
        return `
            ${SAT.Utils.DynamicCustomEntry.getHTMLString()}
        `;
    },
    fadeInElements:()=> {
        TweenMax.to("#overlay", 2, {autoAlpha:1});
        TweenMax.to("#topTitle", 1, {autoAlpha:1, delay:.2});
        TweenMax.to("#title", 1, {autoAlpha:1, delay:.2});
        TweenMax.to("#subtitle", 1, {autoAlpha:1, delay:.2});
        TweenMax.to("#bodyText", 1, {autoAlpha:1, delay:.7});

        if (SAT.Utils.DynamicCustomEntry.options.showJoinRoom) { 
            TweenMax.to("#joinRoomButton", 3, {autoAlpha:1, delay:1});
        }
        if (SAT.Utils.DynamicCustomEntry.options.showEnterOnDevice) {
            TweenMax.to("#enterOnDeviceButton", 3, {autoAlpha:1, delay:1.4});
        }
        if (SAT.Utils.DynamicCustomEntry.options.showSpectate) {
            TweenMax.to("#enterSpectateButton", 3, {autoAlpha:1, delay:1.6});
        }

        TweenMax.to("#smallLogo", 3, {autoAlpha:1, delay:1.8});
    },
    renderButton:()=>{
        if (SAT.Utils.DynamicCustomEntry.options.showJoinRoom) {
            return `<Button size="small" id="joinRoomButton" class="button customentrybutton">
            <img class="buttonicon" src="${SAT.baseURI}/assets/flecheIcon.png">
            <span class="buttontext">${SAT.Utils.DynamicCustomEntry.localizedText}</span>
            </Button>`
        } else {
            return "";
        }
    },
    renderVRButton:()=>{
        if (SAT.Utils.DynamicCustomEntry.options.showEnterOnDevice) {
            return `<Button size="small" id="enterOnDeviceButton" class="button customentrybutton">
            <img class="buttonicon" src="${SAT.baseURI}/assets/lunetteIcon.png">
            <span>${SAT.Utils.DynamicCustomEntry.localizedVRText}</span>
            </Button>`
        } else {
            return "";
        }
    },
    renderSpectateButton:()=>{
        if (SAT.Utils.DynamicCustomEntry.options.showSpectate) {
            return `<Button size="small" id="enterSpectateButton" class="button customentrybutton">
            <img class="buttonicon" src="${SAT.baseURI}/assets/eyeIcon.png">
            <span>${SAT.Utils.DynamicCustomEntry.localizedSpectateText}</span>
            </Button>`
        } else {
            return "";
        }
    },
    getHTMLString:()=> {
        SAT.Utils.DynamicCustomEntry.doEntry();  
        return `
        <html>
            <head>
            <style>

                @font-face {
                    font-family: 'barlowblack';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-black-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-black-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'barlowbold';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-bold-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-bold-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'barlowextrabold';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-extrabold-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-extrabold-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'barlowlight';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-light-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-light-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;

                }

                @font-face {
                    font-family: 'barlowmedium';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-medium-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-medium-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;

                }

                @font-face {
                    font-family: 'barlowregular';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-regular-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-regular-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'barlowsemibold';
                    src: url('${SAT.baseURI}/assets/fonts/Barlow/barlow-semibold-webfont.woff2') format('woff2'),
                        url('${SAT.baseURI}/assets/fonts/Barlow/barlow-semibold-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'terminal';
                    src: url('${SAT.baseURI}/assets/fonts/termina/termina-webfont.woff2') format('woff2'),
                         url('${SAT.baseURI}/assets/fonts/termina/termina-webfont.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                }

                .satellite {
                    display: flex;
                    align-self: center;
                    flex-direction: column;
                    align-items: center;
                    align-self: center;
                    max-width:100%;
                    padding: 0;
                    margin: 0;
                }

                .disableselection {
                    padding: 0;
                    margin: 0;
                    align-self: center;
                    -moz-user-select: -moz-none;
                    -khtml-user-select: none;
                    -webkit-user-select: none;
                    -o-user-select: none;
                    user-select: none;
                }

                .toptitle {
                    opacity: 0;
                    background-color: #CD00FF;
                    color: #FFFFFF;
                    display: flex;
                    align-self: center;
                    flex-direction: row;
                    font-family: barlowlight;
                    font-size: 20px;
                    padding-top: 10px;
                    padding-bottom: 10px;
                    padding-left: 22px;
                    padding-right: 22px;
                    margin-bottom: 15px;
                    z-index: 2;
                }

                .title {
                    opacity: 0;
                }

                @media only screen and (max-width: 640px) {
                    .title {
                        color: #FFFFFF;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        marging-top: 25px;
                        marging-bottom: 0px;
                        font-family: terminal;
                        font-size: 30px;
                        z-index: 3;
                        text-align: center;
                    }
                }

                @media only screen and (min-width: 641px) {
                    .title {
                        color: #FFFFFF;
                        display: flex;
                        text-align: center;
                        flex-direction: column;
                        justify-content: center;
                        marging-top: 25px;
                        marging-bottom: 0px;
                        font-family: terminal;
                        font-size: 45px;
                        z-index: 3;
                    }
                }

                .subtitle {
                    opacity: 0;
                }

                @media only screen and (max-width: 640px) {
                    .subtitle {
                        color: #FFFFFF;
                        display: flex;
                        align-self: center;
                        flex-direction: row;
                        marging-top: 3px;
                        font-family: barlowregular;
                        font-size: 15px;
                        z-index: 4;
                    }
                }

                @media only screen and (min-width: 641px) {
                    .subtitle {
                        color: #FFFFFF;
                        display: flex;
                        align-self: center;
                        flex-direction: row;
                        marging-top: 3px;
                        font-family: barlowregular;
                        font-size: 17px;
                        z-index: 4;
                    }
                }
                
                .text {
                    opacity: 0;
                }

                @media only screen and (max-width: 640px) {
                    .text {
                        margin-top:50px;
                        color: #FFFFFF;
                        display: flex;
                        align-self: center;
                        flex-direction: row;
                        marging-top: 20px;
                        width: 60%;
                        font-family: barlowregular;
                        font-size: 12px;
                        z-index: 5;
                        text-align: center;
                    }
                }

                @media only screen and (min-width: 641px) {
                    .text {
                        margin-top:50px;
                        color: #FFFFFF;
                        display: flex;
                        align-self: center;
                        flex-direction: row;
                        marging-top: 20px;
                        width: 40%;
                        font-family: barlowregular;
                        font-size: 14px;
                        z-index: 5;
                        text-align: center;
                    }
                }

                .buttons {
                    display: flex;  
                    align-items: center;
                    flex-direction: column;
                    justify-content: center;
                    margin-top: 50px;
                    z-index:6
                }

                .customentrybutton {
                    display: flex;  
                    align-items: center;
                    justify-content: center;
                    height: 48px;
                    min-height: 48px;
                    width: 100%
                    font-weight: 700; 
                    font-size: 12px;
                    border-radius: 8px;
                    border-width: 1;
                    transition: background-color 0.1s ease-in-out;
                    white-space: nowrap;
                    padding: 0 24px;
                    margin-top: 15px;
                    opacity: 0;
                    color:#ffffff;
                    background-color: #4B5562CC;
                    border: 1px;
                    border-color: #FFFFFF;
                    size:small !important;
                }

                .customentrybutton:hover {
                    background-color: #4B5562;
                    border: 1px;
                    border-color: #000000;
                }

                .buttonicon {
                    margin-right: auto;
                    padding-right:10px;
                  }
                  
                .buttontext {
                    margin-right: auto;
                }

                .overlay {
                    position: absolute;
                    top: 0px;
                    left: 0px;
                    height: 100vh;
                    width: 100%;
                    pointer-events: none;
                    background: rgb(0, 0, 0);
                    background: radial-gradient(circle, rgba(0,0,0,1) 0%, rgba(0,0,0,0.5) 55%, rgba(0,0,0,0) 90%);
                    z-index: 1;
                    opacity: 0;
                }

                .smalllogo {
                    margin-top: 50px;
                    width: 60px;
                    height: auto;
                    z-index: 9;
                    opacity: 0;
                }

            </style>
            </head>
            <div id="dynamicCustomEntry" class="satellite disableselection">
                <div id="topTitle" class="toptitle disableselection">
                    PERFORMANCE
                </div> 
                <div id="title" class="title disableselection">
                    ${SAT.Utils.DynamicCustomEntry.localizedTitleText}
                </div> 
                <div id="subtitle" class="subtitle disableselection">
                    ${SAT.Utils.DynamicCustomEntry.localizedSubTitleText}
                </div> 
                <div id="bodyText" class="text disableselection">
                ${SAT.Utils.DynamicCustomEntry.localizedDescText}
                </div>
                <div id="buttons" class="buttons">
                    ${SAT.Utils.DynamicCustomEntry.options.showJoinRoom ? SAT.Utils.DynamicCustomEntry.renderButton() : ""}
                    ${SAT.Utils.DynamicCustomEntry.options.showEnterOnDevice ? SAT.Utils.DynamicCustomEntry.renderVRButton() : ""}
                    ${SAT.Utils.DynamicCustomEntry.options.showSpectate ? SAT.Utils.DynamicCustomEntry.renderSpectateButton() : ""}
                </div>
               
                <div id="smallLogo" class="smalllogo">
                    <img draggable="false" src="${SAT.baseURI}/rooms/9Jog9bG/assets/whiteLogo.png" class="disableselection" />
                </div>
                <div id="overlay" class="overlay"/>
            </div>
            <script>
                document.getElementById("topTitle").style.opacity = "0";
                document.getElementById("title").style.opacity = "0";
                document.getElementById("subtitle").style.opacity = "0";
                document.getElementById("bodyText").style.opacity = "0";
                document.getElementById("joinRoomButton").style.opacity = "0";
                document.getElementById("enterOnDeviceButton").style.opacity = "0";
                document.getElementById("enterSpectateButton").style.opacity = "0";
                document.getElementById("smallLogo").style.opacity = "0";
            </script>`
    }
};

SAT.Utils.DynamicCustomEntry.localizedText = SAT.Utils.DynamicCustomEntry.buttonText[SAT.Utils.locale];
SAT.Utils.DynamicCustomEntry.localizedVRText = SAT.Utils.DynamicCustomEntry.vrButtonText[SAT.Utils.locale];
SAT.Utils.DynamicCustomEntry.localizedSpectateText = SAT.Utils.DynamicCustomEntry.spectateButtonText[SAT.Utils.locale];
SAT.Utils.DynamicCustomEntry.localizedTitleText = SAT.Utils.DynamicCustomEntry.titleText[SAT.Utils.locale];
SAT.Utils.DynamicCustomEntry.localizedSubTitleText = SAT.Utils.DynamicCustomEntry.subtitleText[SAT.Utils.locale];
SAT.Utils.DynamicCustomEntry.localizedDescText = SAT.Utils.DynamicCustomEntry.descText[SAT.Utils.locale];

